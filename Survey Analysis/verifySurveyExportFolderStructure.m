function verifySurveyExportFolderStructure( parentDir )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%% Find Run-folders in parentDir
runFolderNames = findFoldersWithName( parentDir, 'Run', 'sortByEndNumber');
NRunFolders = length(runFolderNames);

%% Find All folders in parentDir
content = dir(parentDir);
dirs = [content.isdir];
content = content(dirs);
allFolderNames = {content.name};

%% Check if non-Run folders contains csv-files
otherFolder = false;
if NRunFolders ~= length(allFolderNames)
    diffNames = setdiff(allFolderNames, runFolderNames);
    NDiff = length(diffNames);
    foundCSV = zeros(NDiff,1);
    
    for k = 1:NDiff
        foundCSV(k) = ~isempty(findAllCsvFiles(fullfile(parentDir, diffNames{k})));
    end
    
    if any(foundCSV)
        otherFolder = true;
    end
end

%% Check if Run-folders contain both OK and REMOVED folders
missingOKREM = {};
for k = 1:NRunFolders
    NOKFolders = length(findFoldersWithName( fullfile(parentDir, runFolderNames{k}), 'OK'));
    NREMFolders = length(findFoldersWithName( fullfile(parentDir, runFolderNames{k}), 'REMOVED'));
    
    if NOKFolders ~= 1 || NREMFolders ~= 1
        missingOKREM = [missingOKREM {[runFolderNames{k} ' ']}]; %#ok<AGROW>
    end
end

%% Check if numbers of csv-files in OK and REMOVED matches that in Run-folder
missmatchNCSV = {};
for k = 1:NRunFolders
    NTot = length(findAllCsvFiles(fullfile(parentDir, runFolderNames{k})));
    NOK = length(findAllCsvFiles(fullfile(parentDir, runFolderNames{k}, 'OK')));
    NREM = length(findAllCsvFiles(fullfile(parentDir, runFolderNames{k}, 'REMOVED')));
    
    if NREM + NOK ~= NTot
        missmatchNCSV = [missmatchNCSV runFolderNames(k)]; %#ok<AGROW>
    end
end

%% Summarise

DisplayCell = {};

if ~isempty(missingOKREM)
    DisplayCell = [ {'The following Run-folder(s) does not contain folders named OK or REMOVED:'}...
        missingOKREM {'Please create these folders!'} {' '} ];
end

if ~isempty(missmatchNCSV)
    DisplayCell = [DisplayCell {'Number of csv-files in the following Run-folder(s) does not add up:'}...
        missmatchNCSV {'Make sure the files were copied correctly into the OK & REMOVED folders!'} {' '}];
end

if otherFolder
    DisplayCell = [ DisplayCell {'Parent directory contains unrecognised folders with csv-files.'}...
        {'They will not be included in the calculations.'} ];
end
 
if ~isempty(missingOKREM) || ~isempty(missmatchNCSV)
    errordlg([ {['Error(s) in parent directory: ' parentDir]} {' '} DisplayCell ], ...
        'Survey export folder structure report')
    error(['Error(s) in parent directory: ' parentDir ' ' cell2mat(DisplayCell)])
    
elseif otherFolder
    warndlg([ {['Warning in parent directory: ' parentDir]} {' '} DisplayCell ], ...
        'Survey export folder structure report')
    
else
    disp(' ')
    disp('(Survey export folder structure has been verified)')
    disp(' ')
end

end

