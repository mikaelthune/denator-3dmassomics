function displaySurveyFeedback( SurveyStruct )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Nresp = SurveyStruct.numberOfResponses;

agreementQ1 = zeros(5,1);
agreementQ2 = zeros(5,1);
agreementQ3 = zeros(5,1);
agreementQ4 = zeros(5,1);

durationQ = zeros(5,1);
numPairsQ = zeros(5,1);

for k = 1:Nresp
    tmp = str2double(SurveyStruct.User(k).Eval.AgreementStatements{1}(2));
    agreementQ1(tmp) = agreementQ1(tmp) + 1;
    
    tmp = str2double(SurveyStruct.User(k).Eval.AgreementStatements{2}(2));
    agreementQ2(tmp) = agreementQ2(tmp) + 1;
    
    tmp = str2double(SurveyStruct.User(k).Eval.AgreementStatements{3}(2));
    agreementQ3(tmp) = agreementQ3(tmp) + 1;
    
    tmp = str2double(SurveyStruct.User(k).Eval.AgreementStatements{4}(2));
    agreementQ4(tmp) = agreementQ4(tmp) + 1;
    
    tmp = str2double(SurveyStruct.User(k).Eval.Duration(2));
    durationQ(tmp) = durationQ(tmp) + 1;
    
    tmp = str2double(SurveyStruct.User(k).Eval.NumberOfPairs(2));
    numPairsQ(tmp) = numPairsQ(tmp) + 1;
end

figure
bar(agreementQ1)
title('The instructions given at the start of the survey were clear')
xlabel('Strongly Disagree - Strongly Agree')

figure
bar(agreementQ2)
title('It was easy to understand how to use the slider in the questions')
xlabel('Strongly Disagree - Strongly Agree')

figure
bar(agreementQ3)
title('It was easy to decide which of the images in a pair was of higher quality')
xlabel('Strongly Disagree - Strongly Agree')

figure
bar(agreementQ4)
title('Determination of image quality is an import part of an MALDI imaging experiment')
xlabel('Strongly Disagree - Strongly Agree')

figure
bar(durationQ)
title('What do you think of the time it took to complete the survey?')
xlabel('Too short - Too long')

figure
bar(numPairsQ)
title('What do you think of the number of questions that were presented?')
xlabel('Too few - Too many')

end

