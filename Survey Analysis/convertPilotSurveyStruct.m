function FullSurveyStruct = convertPilotSurveyStruct( Pilot_SurveyStruct, Pilot_ImPairInfo )
%UNTITLED Summary of this function goes here
%   Convert SurveyStruct from Pilot-format to FullSurvey-format
%   Also discards inconsistent users

NResp = Pilot_SurveyStruct.numberOfResponses;
consistentCount = 0;

for k = 1:NResp
    tmpStruct = Pilot_SurveyStruct.User(k);
    
    if userIsConsistent(tmpStruct, 'std', 'NoDisp')
        consistentCount = consistentCount + 1;
        pair = tmpStruct.pairNumber;
        
        tmpStruct = rmfield(tmpStruct, {'id', 'pairNumber'});
        tmpStruct.SurveyID = nan;
        tmpStruct.ImPairs = Pilot_ImPairInfo(pair,:);
        
        FullSurveyStruct.Response(consistentCount) = tmpStruct;
    end
end

if consistentCount < NResp
    disp(' ')
    warning([num2str(NResp-consistentCount) ' users were removed because of inconsistency'])
end

FullSurveyStruct.AllImPairs = Pilot_ImPairInfo;

end

