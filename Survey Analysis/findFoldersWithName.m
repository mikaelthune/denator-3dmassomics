function folderNames = findFoldersWithName( parentDir, nameToMatch, varargin )
%UNTITLED3 Summary of this function goes here
%   Find all folders in parentDir that has a name containing the input 
%   string nameTomatch.
%   folderNames = findFoldersWithName( parentDir, nameToMatch, 'sortByEndNumber' )

content = dir(parentDir);
dirs = [content.isdir];
content = content(dirs);
dirNames = {content.name};

matchInd = strfind(dirNames, nameToMatch);
matchDirs = ~cellfun(@isempty, matchInd);
folderNames = dirNames(matchDirs);

% Sort folderNames by numbers.
% Only works if numbers in folder name comes directly after the input 
% string nameToMatch
if nargin > 2 && strcmpi(varargin{1}, 'sortByEndNumber')
    tmp = matchInd(matchDirs);
    numStartInd = tmp{1} + length(nameToMatch);
    
    NFolders = length(folderNames);
    nums = zeros(NFolders,1);
    for k = 1:NFolders
        nums(k) = str2double(folderNames{k}(numStartInd:end));
    end
    
    [~,ind] = sort(nums);
    folderNames = folderNames(ind);
    
end

end

