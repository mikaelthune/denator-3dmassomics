function SurveyIDs = extractSurveyIDFromFilenames( FileNames )
%UNTITLED Summary of this function goes here
%   NOTE: Only works when the survey ID is in the range 10001 - 10099

NFiles = length(FileNames);
SurveyIDs = zeros(NFiles,1);
 
for s = 1:NFiles
    beginInd = strfind(FileNames{s}, '100');
    SurveyIDs(s) = str2double(FileNames{s}(beginInd: beginInd + 4));
end

end

