function displaySurveyImPairsSTDInterval( FullSurveyStruct, ImCube, NIntervals )
%UNTITLED5 Summary of this function goes here
%   Display Image pairs from survey in even intervals of increasing
%   standard deviation in slider value.

ComparisonTable = createPairSliderValueTable( FullSurveyStruct );
sliderVals = ComparisonTable(:,3:end);

sliderSTD = std(sliderVals,0,2);
[sliderSTD_sort, idx] = sort(sliderSTD);
pairListRearranged = FullSurveyStruct.AllImPairs_plan(idx,:);

stdToDisplay = linspace(min(sliderSTD), max(sliderSTD), NIntervals);

% Plot sorted list of standard deviation in slider value for survey
figure
plot(sliderSTD_sort)
axis tight
a = axis;
hold on

% Calculate the image pairs to display and plot overlayed lines that 
% represents the intervals being used.
indToDisplay = nan(NIntervals,1);
for k = 1:NIntervals
    [~,indToDisplay(k)] = min(abs(sliderSTD_sort - stdToDisplay(k)));
    line([1;length(sliderSTD)],[stdToDisplay(k);stdToDisplay(k)], 'Color', 'k')
end
axis(a)

displayImPairs(ImCube,pairListRearranged(indToDisplay,:))

end

