function evaluateChangeOfConsitency( dirWithRuns, newStd )
%UNTITLED Summary of this function goes here
%   Compares new std critreia to defualt value of std = 0.2

warndlg('Assumes you have pre-sorted csv-files in folders called OK and REMOVED')

content = dir(dirWithRuns);
dirs = [content.isdir];
content = content(dirs);
dirNames = {content.name};

runDirs = ~cellfun(@isempty,strfind(dirNames,'Run'));
runDirNames = dirNames(runDirs);

NRunDirs = length(runDirNames);

NInconsistentDefault = 0;
NInconsistentNew = 0;
for k = 1:NRunDirs
        inconsistentFilesDefault = checkDirForInconsitency( fullfile(dirWithRuns,['Run' num2str(k)]), ...
            6, 'QEval_CommentTime', 'std', 'NoDisp' );
        NInconsistentDefault = NInconsistentDefault + length(inconsistentFilesDefault);
        
        inConsistentFilesAfterChange = checkDirForInconsitency( fullfile(dirWithRuns,['Run' num2str(k)]), ...
            6, 'QEval_CommentTime', 'std' , newStd, 'NoDisp');
        NInconsistentNew = NInconsistentNew + length(inConsistentFilesAfterChange);
end

disp(' ')
disp(['Inconsistent responses with Default criteria: ' num2str(NInconsistentDefault)])
disp(['Inconsistent responses with Changed criteria: ' num2str(NInconsistentNew)])

end

