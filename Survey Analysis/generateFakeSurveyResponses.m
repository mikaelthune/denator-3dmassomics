function FullSurveyStruct_fakesAdded = generateFakeSurveyResponses( FullSurveyStruct_consistent, SurveyPairList, FakeSurveyIDs )
%UNTITLED3 Summary of this function goes here
%   Append fake responses to a FullSurveyStruct.
%   These responses are based on Response(1) inthe input FullSurveyStruct.
%   But they've had the following values altered:
%       ImPairs_plan, sliderValue_planScaled, SurveyID, Mail
%

NFake = length(FakeSurveyIDs);
NConsistent = length(FullSurveyStruct_consistent.Response);


PairSliderValue_consistent = createPairSliderValueTable( FullSurveyStruct_consistent );
SliderTable_consistent = PairSliderValue_consistent(:,3:end);
AllImPairs = PairSliderValue_consistent(:,1:2)
NRespPair = size(SliderTable_consistent,2);

FullSurveyStruct_fakesAdded = FullSurveyStruct_consistent;

% Loop through the surveys that shall be faked
for k = 1:NFake
    % pairs to fake slider values for in current survey
    pairsToFake = SurveyPairList{FakeSurveyIDs(k)};
    FakeStruct = FullSurveyStruct_consistent.Response(1);
    NPairsInSurvey = size(pairsToFake,1);
    
    FakeStruct.SurveyID = FakeSurveyIDs(k);
    FakeStruct.Mail = '------- FAKE Response! -------';
    FakeStruct.ImPairs_plan = pairsToFake;
    
    fakeSliderValues = nan(NPairsInSurvey,1);
    for p = 1:NPairsInSurvey
        row = find(and( AllImPairs(:,1) == pairsToFake(p,1), AllImPairs(:,2) == pairsToFake(p,2) ));
        
        existent = SliderTable_consistent(row,:);
        % Set to 1 if no existing responses for this pair
        % Otherwise set to mean of existing values
        if sum(isnan(existent)) == NRespPair
            fakeSliderValues(p) = 1;
        else
            fakeSliderValues(p) = nanmean(existent);
        end
    end
    
    FakeStruct.sliderValue_planScaled = fakeSliderValues;
    
    FullSurveyStruct_fakesAdded.Response(NConsistent+k) = FakeStruct;
    
end

end


