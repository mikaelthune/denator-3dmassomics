function [SurveyIDs_sorted_final, varargout] = getSortedInconsistentInParentDir( parentDir, std_thresh, varargin )
%   Gets information on the inconsistent responses in directory 
%   parentDir. Will ONLY return information of inconsistent responses that 
%   do NOT have a consistent counterpart somewhere else in parentDir.
%
%   NOTE: Assumes that parentDir has a defined folder structure with 
%   folders named 'Run'.
%
%   Uses:
%   SurveyIDs_sorted = getSortedInconsistentInParentDir( parentDir, std_thresh )
%   SurveyIDs_sorted = getSortedInconsistentInParentDir( parentDir, std_thresh, NInfoStrings, endString )
%   [SurveyIDs_sorted, repPairStd, fileNames, containingFolder, LRIsTheSame, repValues] = getSortedInconsistentInParentDir( parentDir, std_thresh )
%
%   Input:
%   -   parentDir is the directory with 'Run' folders
%   -   std_thresh is the standard deviation threshold used to determine
%       which survey responses in parentDir are consistent. If a response
%       has a standard deviation <= std_thresh then it is consistent.
%   -   NInfoString is a survey parameter that says how many initial 
%       fields the csv-files have before the custom fields start appearing.
%       DEFAULT value is set to 6 (3DMassomics survey)
%   -   endString is a survey specific paramter that says what the field
%       name is in the csv-files.
%       DEFAULT value is set to 'QEval_CommentTime' (3DMassomics survey)
%
%   Output:
%   All output variables are sorted so that the most inconsistent response
%   is in the first index and the most consistent one is in the last index.
%   -   SurveyIDs_sorted is the survey IDs of the inconsistent responses
%   -   repPairStd is the standard deviation of the repeated pairs
%   -   fileNames is the name of the files
%   -   containingFolder is the name of the containing 'Run' folder
%   -   LRIsTheSame is a boolean that says if respondant has chosen the 
%       same of the repeated pairs all times.
%   -   repValues are the scaled slider values of the repeated pairs, in
%       the planning "space" of reference.

%% Input check
if nargin == 2
    NInfoStrings = 6;
    endString = 'QEval_CommentTime';
elseif nargin == 3
    error('Must be either 2 or 4 input paramters')
else
    if isnumeric(varargin{1}) && ischar(varargin{2})
        NInfoStrings = varargin{1};
        endString = varargin{2};
    else
        error('Input ''NInfoStrings'' must be numeric and ''endString'' must be a string')
    end
end

%% Calculate

% Check that the folder structure is ok
verifySurveyExportFolderStructure( parentDir );

folderNames = findFoldersWithName( parentDir, 'Run', 'sortByEndNumber');
NFolders = length(folderNames);

% One cell index for each Run-folder
FileNameCell = cell(NFolders,1);            % File names of responses
RepPairStdCell = cell(NFolders,1);          % Standard deviation for responses in folders
ContainingFolderCell = cell(NFolders,1);    % Name of containing folder for every File
LRIsTheSameCell = cell(NFolders,1);         % bool for describing if responses in folders favoured the same repeated pair all 3 times
repValuesCell = cell(NFolders,1);           % slider values for the repeated pairs

% Initiate growing list
fileNames_all = {};
fileID_all = [];
repPairStd_all = [];
containingFolder_all = {};
LRIsTheSame_all = [];
repValues_all = [];

for k = 1:NFolders
    [RepPairStdCell{k}, FileNameCell{k}] = ...
        getRepSTDInDir( fullfile(parentDir, folderNames{k}), NInfoStrings, endString);
    [LRIsTheSameCell{k}, ~, repValuesCell{k}] = ...
        getRepLRInDir( fullfile(parentDir, folderNames{k}), NInfoStrings,  endString);
    
    NFiles = length(FileNameCell{k});
    ContainingFolderCell{k} = repmat(folderNames(k), NFiles, 1);
    
    fileNames_all = [fileNames_all; FileNameCell{k}];
    containingFolder_all = [containingFolder_all; ContainingFolderCell{k}];
    repPairStd_all = [repPairStd_all; RepPairStdCell{k}];
    fileID_all = [fileID_all; extractSurveyIDFromFilenames(FileNameCell{k})];
    LRIsTheSame_all = [LRIsTheSame_all; LRIsTheSameCell{k}];
    repValues_all = [repValues_all; repValuesCell{k}];
    
end

% Sort responses by ascending STD. This is done so the
% unique-function can remove the worst response of a duplicate.
[repPairStd_sort, sortInd_asc] = sort(repPairStd_all);
containingFolder_sort = containingFolder_all(sortInd_asc);
fileNames_sort = fileNames_all(sortInd_asc);
fileID_sort = fileID_all(sortInd_asc);
LRIsTheSame_sort = LRIsTheSame_all(sortInd_asc);
repValues_sort = repValues_all(sortInd_asc,:);

% Remove most inconsistent response in eventual duplicates
[~, uniqueInd, ~] = unique(fileID_sort, 'stable');
repPairStd_unique = repPairStd_sort(uniqueInd);
containingFolder_unique = containingFolder_sort(uniqueInd);
fileNames_unique = fileNames_sort(uniqueInd);
fileID_unique = fileID_sort(uniqueInd);
LRIsTheSame_unique = LRIsTheSame_sort(uniqueInd);
repValues_unique = repValues_sort(uniqueInd,:);

% Remove all consistent responses from calculations
inconsistent = repPairStd_unique >= std_thresh;
repPairStd_inc = repPairStd_unique(inconsistent);
containingFolder_inc = containingFolder_unique(inconsistent);
fileNames_inc = fileNames_unique(inconsistent);
fileID_inc = fileID_unique(inconsistent);
LRIsTheSame_inc = LRIsTheSame_unique(inconsistent);
repValues_inc = repValues_unique(inconsistent,:);

% Finally sort responses by descending STD
[repPairStd_final, sortInd_desc] = sort(repPairStd_inc, 'descend');
containingFolder_final = containingFolder_inc(sortInd_desc);
fileNames_final = fileNames_inc(sortInd_desc);
SurveyIDs_sorted_final = fileID_inc(sortInd_desc);
LRIsTheSame_final = LRIsTheSame_inc(sortInd_desc);
repValues_final = repValues_inc(sortInd_desc,:);

if nargout > 1
    varargout{1} = repPairStd_final;
    varargout{2} = fileNames_final;
    varargout{3} = containingFolder_final;
    varargout{4} = LRIsTheSame_final;
    varargout{5} = repValues_final;
end

end
