function [ LRIsTheSame, fileNames, varargout ] = getRepLRInDir( responseDir, varargin )
%   Get value that says if respondant has chosen the same of the repeated pairs
%   all times. Calcualted after scaling and sign adjustment.
%
%   Uses:
%   [LRIsTheSame, fileNames] = getRepLRInDir( responseDir )
%   [LRIsTheSame, fileNames] = getRepLRInDir( responseDir, NInfoStrings, endString )
%   [LRIsTheSame, fileNames, repValues] = getRepLRInDir( responseDir )
%
%   Input:
%   -   responseDir is the directory with response files
%   -   NInfoString is a survey parameter that says how many initial 
%       fields the csv-files have before the custom fields start appearing.
%       DEFAULT value is set to 6 (3DMassomics survey)
%   -   endString is a survey specific paramter that says what the field
%       name is in the csv-files.
%       DEFAULT value is set to 'QEval_CommentTime' (3DMassomics survey)
%    
%   Output:
%   -   LRIsTheSame is a boolean that says if respondant has chosen the 
%       same of the repeated pairs all times.
%   -   fileNames are the filenames of the files.
%   -   repValues are the scaled slider values of the repeated pairs, in
%       the planning "space" of reference.

%% Input check
if nargin == 1
    NInfoStrings = 6;
    endString = 'QEval_CommentTime';
elseif nargin == 2
    error('Must be either 1 or 3 input paramters')
else
    if isnumeric(varargin{1}) && ischar(varargin{2})
        NInfoStrings = varargin{1};
        endString = varargin{2};
    else
        error('Input ''NInfoStrings'' must be numeric and ''endString'' must be a string')
    end
end

%% Calculate
[SurveyStructCell, fileNames] = ...
    importCsvFiles(responseDir, NInfoStrings, endString);
NFiles = length(SurveyStructCell);

if NFiles == 0
    LRIsTheSame = [];
    fileNames = {};
    
else
    LRIsTheSame = nan(NFiles,1);
    repValues = zeros(NFiles,3);
    for k = 1:NFiles
        % Scale and adjust sign of slider values
        [~, repSliderVal_surveyScaled] = scaleSliderValue(SurveyStructCell{k}.User.sliderValue_survey, ...
            SurveyStructCell{k}.User.repPairSliderValue_survey);
        repSliderVal_planScaled = adjustSliderSign(repSliderVal_surveyScaled, ...
            SurveyStructCell{k}.User.repPairLRFlipValue);
        
        repValues(k,:) = repSliderVal_planScaled;
        LRIsTheSame(k) = all(repSliderVal_planScaled < 0) || all(repSliderVal_planScaled > 0);
    end
end

if nargout > 2
    varargout{1} = repValues;
end

end

