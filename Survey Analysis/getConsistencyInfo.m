function [ repPairStd, fileNames, containingFolder, LRIsTheSame, repValues ] = getConsistencyInfo( parentDir, mode)
%UNTITLED3 Summary of this function goes here
%   Gets consistency information of responses based on where in the folder
%   structure they are. 
%   This functions assumes you have sorted the csv-files into the correct
%   OK/REMOVED folder.
%
%   ... = getConsistencyInfo( parentDir, mode)
%   mode = 'consistent'
%   mode = 'inconsistent'
%   mode = 'all'

verifySurveyExportFolderStructure(parentDir);

folderNames = findFoldersWithName( parentDir, 'Run', 'sortByEndNumber' );
NFiles = length(folderNames);

repPairStd = [];
fileNames = {};
containingFolder = {};
LRIsTheSame = [];
repValues = [];

for k = 1:NFiles
    if strcmpi(mode, 'consistent')
        directory = fullfile(parentDir, folderNames{k}, 'OK');
    elseif strcmpi(mode, 'inconsistent')
        directory = fullfile(parentDir, folderNames{k}, 'REMOVED');
    elseif strcmpi(mode, 'all')
        directory = fullfile(parentDir, folderNames{k});
    else
        error('Unknown string for input ''mode''')
    end
    
    [tmp1, tmp2] = getRepSTDInDir( directory );
    repPairStd = [repPairStd; tmp1];
    fileNames = [fileNames; tmp2];
    
    [tmp1, ~, tmp2] = getRepLRInDir( directory );
    LRIsTheSame = [LRIsTheSame; tmp1];
    repValues = [repValues; tmp2];
    
    containingFolder = [containingFolder; repmat(folderNames(k), length(fileNames), 1)];
    
end

end

