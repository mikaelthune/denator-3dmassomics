function [ stdValues, fileNames ] = getRepSTDInDir( responseDir, varargin )
%getRepSTDInDir Summary of this function goes here
%   Get standard deviation of repeated pairs in responses in directory
%   Standard deviation is calcualted after scaling and sign adjustment
%
%   Uses:
%   [ stdValues, fileNames ] = getRepSTDInDir( responseDir )
%   [ stdValues, fileNames ] = getRepSTDInDir( responseDir, NInfoStrings, endString )
%
%   Input:
%   -   responseDir is the directory with response files
%   -   NInfoString is a survey parameter that says how many initial 
%       fields the csv-files have before the custom fields start appearing.
%       DEFAULT value is set to 6 (3DMassomics survey)
%   -   endString is a survey specific paramter that says what the field
%       name is in the csv-files.
%       DEFAULT value is set to 'QEval_CommentTime' (3DMassomics survey)
%
%   Output:
%   -   stdValues are the standard deviation values for the repeated pairs
%       of all the response files in directory.
%   -   fileNames are the filenames of the files.

%% Input check
if nargin == 1
    NInfoStrings = 6;
    endString = 'QEval_CommentTime';
elseif nargin == 2
    error('Must be either 1 or 3 input paramters')
else
    if isnumeric(varargin{1}) && ischar(varargin{2})
        NInfoStrings = varargin{1};
        endString = varargin{2};
    else
        error('Input ''NInfoStrings'' must be numeric and ''endString'' must be a string')
    end
end

%% Calculate
[SurveyStructCell, fileNames] = ...
    importCsvFiles(responseDir, NInfoStrings, endString);
NFiles = length(SurveyStructCell);

if NFiles == 0
    stdValues = [];
    fileNames = {};
    
else
    stdValues = zeros(NFiles,1);
    for k = 1:NFiles
        % Scale and adjust sign of slider values
        [~, repSliderVal_surveyScaled] = scaleSliderValue(SurveyStructCell{k}.User.sliderValue_survey, ...
            SurveyStructCell{k}.User.repPairSliderValue_survey);
        repSliderVal_planScaled = adjustSliderSign(repSliderVal_surveyScaled, ...
            SurveyStructCell{k}.User.repPairLRFlipValue);
        
        stdValues(k) = std(repSliderVal_planScaled);
    end
end

end

