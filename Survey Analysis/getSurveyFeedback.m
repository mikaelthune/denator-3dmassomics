function [AgreementStatements, Duration, NumberOfPairs,Experience,Work, QualityAsessment,AdditionalComments,Email] = getSurveyFeedback(FullSurveyStruct)

n_raters = length(FullSurveyStruct.Response);

AgreementStatements = cell(n_raters,4);
Duration = cell(n_raters,1);
NumberOfPairs = cell(n_raters,1);
Experience = cell(n_raters,1);
Work = cell(n_raters,1);
QualityAsessment = cell(n_raters,1);
AdditionalComments = cell(n_raters,1);
Email = cell(n_raters,1);
for r=1:n_raters
    AgreementStatements(r,:) = FullSurveyStruct.Response(r).Eval.AgreementStatements;
    Duration{r} = FullSurveyStruct.Response(r).Eval.Duration;
    NumberOfPairs{r} = FullSurveyStruct.Response(r).Eval.NumberOfPairs;
    
    Experience{r} = FullSurveyStruct.Response(r).Experience;
    Work{r} = FullSurveyStruct.Response(r).Work;
    QualityAsessment{r} = FullSurveyStruct.Response(r).Eval.QualityAsessment;
    AdditionalComments{r} = FullSurveyStruct.Response(r).Eval.AdditionalComments;
    Email{r} = FullSurveyStruct.Response(r).Mail;

end