function countConsistentInParentDir( dirWithRuns )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

verifySurveyExportFolderStructure( dirWithRuns );


runDirNames = findFoldersWithName( dirWithRuns, 'Run', 'sortByEndNumber');

NRunDirs = length(runDirNames);
FileNameCell_ok = {};
FileNameCell_removed = {};
for k = 1:NRunDirs
    tmp = findAllCsvFiles( fullfile(dirWithRuns,runDirNames{k},'OK') );
    FileNameCell_ok = {FileNameCell_ok{:} tmp{:} };
    
    tmp = findAllCsvFiles( fullfile(dirWithRuns,runDirNames{k},'REMOVED') );
    FileNameCell_removed = {FileNameCell_removed{:} tmp{:} };

end
FileNameCell_ok = FileNameCell_ok';
FileNameCell_removed = FileNameCell_removed';

disp(' ')
disp('------------------- Consistent -------------------')
for k = 1:length(FileNameCell_ok)
    disp(num2str(FileNameCell_ok{k}))
end
disp('----------------- END Consistent -----------------')

disp(' ')
disp('----------------- Not Consistent -----------------')
for k = 1:length(FileNameCell_removed)
    disp(num2str(FileNameCell_removed{k}))
end
disp('--------------- END Not Consistent ---------------')

disp(' ')
disp('SUMMARY:')
disp(['Total number of responses: ' num2str(length(FileNameCell_ok) + length(FileNameCell_removed))])
disp(['Total number of UNIQUE responses: ' num2str(length(unique( [FileNameCell_ok; FileNameCell_removed] ))) ])
disp(' ')
disp(['Total number of Consistent responses: ' num2str(length(FileNameCell_ok))])
disp(['Total number of INconsistent responses: ' num2str(length(FileNameCell_removed))])
disp(' ')
disp(['Total number of UNIQUE Consistent responses: ' num2str(length(unique(FileNameCell_ok)))])
disp(['Total number of UNIQUE INconsistent responses: ' num2str(length(unique(FileNameCell_removed)))])

tmp = length(unique( [FileNameCell_ok; FileNameCell_removed] )) - length(unique(FileNameCell_ok));

disp(['Total number of UNIQUE INconsistent responses after prioritising consistent: ' num2str(tmp) ])

end

