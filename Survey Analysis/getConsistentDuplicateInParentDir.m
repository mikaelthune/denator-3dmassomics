function [worstDuplicateLocation, varargout] = getConsistentDuplicateInParentDir( parentDir, stdCondition )
%UNTITLED Summary of this function goes here
%   Finds eventual duplicates of consistent responses in parent directory 
%   and returns the location of the WORST duplicate(s). 
%
%   Can also return information on all the consistent responses.
%
%   [worstDuplicateLocation, surveyIDs_sorted, repPairStd, fileNames, containingFolder, LRIsTheSame, repValues] = getConsistentDuplicateInParentDir( parentDir, stdCondition )
%
%   NOTE: Only works for 3DMassomics Survey! Look at
%   getSortedInconsistentInParentDir to adapt this function in the same
%   manner.

% Check that the folder structure is ok
verifySurveyExportFolderStructure( parentDir );

folderNames = findFoldersWithName( parentDir, 'Run', 'sortByEndNumber');
NFolders = length(folderNames);

% One cell index for each Run-folder
FileNameCell = cell(NFolders,1);            % File names of responses
RepPairStdCell = cell(NFolders,1);          % Standard deviation for responses in folders
ContainingFolderCell = cell(NFolders,1);    % Name of containing folder for every File
LRIsTheSameCell = cell(NFolders,1);         % bool for describing if responses in folders favoured the same repeated pair all 3 times
repValuesCell = cell(NFolders,1);           % slider values for the repeated pairs

% Initiate growing list
fileNames_all = {};
fileID_all = [];
repPairStd_all = [];
containingFolder_all = {};
LRIsTheSame_all = [];
repValues_all = [];

for k = 1:NFolders
    [RepPairStdCell{k}, FileNameCell{k}] = ...
        getRepSTDInDir( fullfile(parentDir, folderNames{k}) );
    [LRIsTheSameCell{k}, ~, repValuesCell{k}] = ...
        getRepLRInDir( fullfile(parentDir, folderNames{k}) );
    
    NFiles = length(FileNameCell{k});
    ContainingFolderCell{k} = repmat(folderNames(k), NFiles, 1);
    
    fileNames_all = [fileNames_all; FileNameCell{k}];
    containingFolder_all = [containingFolder_all; ContainingFolderCell{k}];
    repPairStd_all = [repPairStd_all; RepPairStdCell{k}];
    fileID_all = [fileID_all; extractSurveyIDFromFilenames(FileNameCell{k})];
    LRIsTheSame_all = [LRIsTheSame_all; LRIsTheSameCell{k}];
    repValues_all = [repValues_all; repValuesCell{k}];
    
end

% Sort responses by ascending STD. This is done so the
% unique-function can remove the worst response of a duplicate.
[repPairStd_sort, sortInd_asc] = sort(repPairStd_all);
containingFolder_sort = containingFolder_all(sortInd_asc);
fileNames_sort = fileNames_all(sortInd_asc);
fileID_sort = fileID_all(sortInd_asc);
LRIsTheSame_sort = LRIsTheSame_all(sortInd_asc);
repValues_sort = repValues_all(sortInd_asc,:);

% Remove all INconsistent responses from calculations
consistent = repPairStd_sort < stdCondition;
repPairStd_consist = repPairStd_sort(consistent);
containingFolder_consist = containingFolder_sort(consistent);
fileNames_consist = fileNames_sort(consistent);
fileID_consist = fileID_sort(consistent);
LRIsTheSame_consist = LRIsTheSame_sort(consistent);
repValues_consist = repValues_sort(consistent,:);

% Remove 'most inconsistent' response in eventual duplicates
[~, uniqueInd, ~] = unique(fileID_consist, 'stable');
repPairStd_unique = repPairStd_consist(uniqueInd);
containingFolder_unique = containingFolder_consist(uniqueInd);
fileNames_unique = fileNames_consist(uniqueInd);
fileID_unique = fileID_consist(uniqueInd);
LRIsTheSame_unique = LRIsTheSame_consist(uniqueInd);
repValues_unique = repValues_consist(uniqueInd,:);

% Finally sort responses by descending STD
[repPairStd_final, sortInd_desc] = sort(repPairStd_unique, 'descend');
containingFolder_final = containingFolder_unique(sortInd_desc);
fileNames_final = fileNames_unique(sortInd_desc);
surveyIDs_sorted_final = fileID_unique(sortInd_desc);
LRIsTheSame_final = LRIsTheSame_unique(sortInd_desc);
repValues_final = repValues_unique(sortInd_desc,:);

% Find which duplicate(s) was removed
duplicateInd = setdiff(1:length(fileID_consist), uniqueInd);
NDuplicates = length(duplicateInd);
worstDuplicateLocation = cell(NDuplicates,1);
for k = 1:length(duplicateInd)
    worstDuplicateLocation{k} = fullfile(containingFolder_consist{duplicateInd}, ...
        fileNames_consist{duplicateInd});
end

if nargout > 1
    varargout{1} = surveyIDs_sorted_final;
    varargout{2} = repPairStd_final;
    varargout{3} = fileNames_final;
    varargout{4} = containingFolder_final;
    varargout{5} = LRIsTheSame_final;
    varargout{6} = repValues_final;
end

end

