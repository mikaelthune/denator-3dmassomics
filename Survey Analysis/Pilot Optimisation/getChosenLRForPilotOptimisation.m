function [chosenLR_adj, ImPairs_adj] = getChosenLRForPilotOptimisation( ResponseSummary, cutOff )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

chosenLR = chosenLRFromResponseSummary( ResponseSummary );

% Remove pairs with less than 3 ans (or only zero answers)
[chosenLR_adj, ImPairs_adj] = ...
    removePairsTooFewAns(chosenLR, ResponseSummary.ImPairs, 3);

% Remove non-consensus pairs
[ImPairs_adj, chosenLR_adj] = ...
    removeNonConsensusPairs(ImPairs_adj, chosenLR_adj, cutOff);

end

