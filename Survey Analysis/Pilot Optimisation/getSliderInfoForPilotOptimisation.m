function [SliderVals, ImPairs_adj] = getSliderInfoForPilotOptimisation( ResponseSummary, cutOff )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

SliderVals = ResponseSummary.AdjustedResponses;
ImPairs_adj = ResponseSummary.ImPairs;
NAnswers = sum(~isnan(SliderVals),2);

% Remove ImPairs that have < 3 answers
indRemove = find(NAnswers < 3);
SliderVals(indRemove,:) = [];
ImPairs_adj(indRemove) = [];

% Remove non-consensus pairs



end

