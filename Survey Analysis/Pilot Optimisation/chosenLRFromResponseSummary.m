function chosenLR = chosenLRFromResponseSummary( ResponseSummary )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

warning('Currently ALL Zero-values are treated as NO Answer!')

Responses = ResponseSummary.AdjustedResponses;
Nvals = size(Responses,1);

chosenLR = nan(Nvals,2);

chosenLR(:,1) = sum(Responses < 0, 2);
chosenLR(:,2) = sum(Responses > 0, 2);

end