function [ CompResults, varargout ] = compareSurveyVsMethods( SurveyStruct, MethodResults, ImPairs )
%UNTITLED Summary of this function goes here
%   CompResults = compareSurveyVsMethods( SurveyStruct, MethodResults, ImPairs )
%   [CompResults, finalChosenLR, finalImPairs, originalInd] = compareSurveyVsMethods( SurveyStruct, MethodResults, ImPairs )

SurveyResults = evaluateSurveyStruct( SurveyStruct, ImPairs );

% Remove pairs with no answers (or only zero answers)
[chosenLR_nonEmpty, ImPairs_nonEmpty] = ...
    removeEmptyPairs(SurveyResults.totalChosenLR, SurveyResults.imPairs);

% Remove pairs with < 3 answers
[chosenLR_full, ImPairs_full] = removePairsTooFewAns(chosenLR_nonEmpty, ImPairs_nonEmpty, 3);

% Remove non-consensus pairs
[ImPairs_consensus, chosenLR_consensus, nonConsensusPairs] = ...
    removeNonConsensusPairs(ImPairs_full, chosenLR_full);

% Compare method against humans
CompResults = evaluateByPairComp( ImPairs_consensus, MethodResults, chosenLR_consensus  );

if nargout > 2
    varargout{1} = chosenLR_consensus;
    varargout{2} = ImPairs_consensus;
end
warning('Fix so original ImPair indices are returned')
end

