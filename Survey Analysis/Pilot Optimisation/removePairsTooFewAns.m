function [ chosenLR_adj, ImPairs_adj ] = removePairsTooFewAns( chosenLR, ImPairs, lessThan )
%UNTITLED2 Summary of this function goes here
%   [chosenLR_adj, ImPairs_adj] = removePairsTooFewAns( chosenLR, ImPairs, lessThan )

test = sum(chosenLR,2);
tooFewAns = test < lessThan;

chosenLR_adj = chosenLR;
chosenLR_adj(tooFewAns,:) = [];

ImPairs_adj = ImPairs;
ImPairs_adj(tooFewAns,:) = [];

end

