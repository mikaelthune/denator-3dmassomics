function [SurveyStruct_consistent, varargout] = removeNonConsistentUsers( SurveyStruct )
%UNTITLED2 Summary of this function goes here
%   SurveyStruct_consistent = removeNonConsistentUsers( SurveyStruct )
%   [SurveyStruct_consistent, removed] = removeNonConsistentUsers( SurveyStruct )

Nresp = SurveyStruct.numberOfResponses;

SurveyStruct_consistent.structCreated = datestr(clock);
SurveyStruct_consistent.numberOfResponses = 0;

count = 0;
removed = [];
for k = 1:Nresp
    LRFlip = SurveyStruct.User(k).repPairLRFlipValue;
    sliderVal = SurveyStruct.User(k).repPairSliderValue;
    sliderVal_adj = adjustSliderValue(sliderVal,LRFlip);
    
    if all(sliderVal_adj < 0) || all(sliderVal_adj > 0) || all(sliderVal_adj == 0)
        SurveyStruct_consistent.User(count+1) = SurveyStruct.User(k);
        count = count + 1;
    else
        removed = [removed; k];
    end
    
end

if count == 0
    error('SurveyStruct contains no consistent users!')
end

SurveyStruct_consistent.numberOfResponses = count;

if nargout > 1
    varargout{1} = removed;
end


end

