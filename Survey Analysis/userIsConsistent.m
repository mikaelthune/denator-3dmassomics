function consistentBool = userIsConsistent( UserStruct, methodString, varargin )
%UNTITLED Summary of this function goes here
%   userIsConsistent( UserStruct, methodString )
%   userIsConsistent( UserStruct, 'std', stdVal )
%
%   Adding 'NoDisp' as input paramter will diable printouts in command
%   window.
%
%   methodString: 'equalSign', 'std'

LRFlip = UserStruct.repPairLRFlipValue;
repSliderVal = UserStruct.repPairSliderValue_survey;
sliderVal = UserStruct.sliderValue_survey;

[~, repSliderVal_adj] = scaleSliderValue(sliderVal, repSliderVal);
repSliderVal_adj = adjustSliderSign(repSliderVal_adj, LRFlip);

stdVal = 0.5;   % Default consistency cut off to use if other is not specified in input.

if nargin > 2 && sum(strcmpi(varargin, 'NoDisp')) >= 1
    dispText = false;
else
    dispText = true;
end

if strcmpi(methodString, 'equalSign')
    if all(repSliderVal_adj < 0) || all(repSliderVal_adj > 0) || all(repSliderVal_adj == 0)
        consistentBool = true;
    else
        consistentBool = false;
    end
    
elseif strcmpi(methodString, 'std')
    
    if nargin > 2
        indNumeric = find(cellfun(@isnumeric, varargin));
        if length(indNumeric) == 1
            if varargin{indNumeric} > 0
                stdVal = varargin{indNumeric};
            else
                error('standard deviation must be positive')
            end
            
        elseif length(indNumeric) > 1
            error('Too many numeric input arguments')
            
        end
    end
    
    if std(repSliderVal_adj) < stdVal
        consistentBool = true;
    else
        consistentBool = false;
    end
    
else
    error('Unknown method')
end

if dispText
    if consistentBool
        disp(['User ' num2str(UserStruct.id) ' [OK]'])
    else
        disp('-------------------------------------------------------')
        disp(['User ' num2str(UserStruct.id) ' [INCONSISTENT]'])
        disp(['IP = ' num2str(UserStruct.IPAddress)])
        disp('-------------------------------------------------------')
    end
end

end

