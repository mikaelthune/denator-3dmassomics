function ImPairs = generateImPairs( ImInfo, varargin )
%GENERATEIMPAIRS Generate random pairs with no image appearing more than 
%   once
%   ImPairs = generateImPairs( ImCube, #ofPairs )
%   ImPairs = generateImPairs( indVector, #ofPairs )

if ndims(ImInfo) == 2
    chosenInd = ImInfo;
    
elseif ndims(ImInfo) == 3
    chosenInd = 1:size(ImInfo,3);
    
end
    
tmpImPairs = nchoosek( chosenInd( randperm(length(chosenInd)) ), 2 );
NPossiblePairs = size(tmpImPairs,1);
ind = randperm(NPossiblePairs);
tmpImPairs = tmpImPairs(ind,:);

if nargin > 1 && isnumeric(varargin{1})
    
    if floor(length(chosenInd)/2) >= varargin{1}
        NPairs = varargin{1};
    else
        warning('Not enough images to generate desired amount of pairs')
        NPairs = floor(length(chosenInd)/2);
    end
    
    ImPairs = zeros(NPairs,2);
    for k = 1:NPairs
        ImPairs(k,:) = tmpImPairs(1,:);
        tmpImPairs( or(tmpImPairs ==  ImPairs(k,1), tmpImPairs(:,[2 1]) ==  ImPairs(k,1) ) ) = [];
        tmpImPairs = reshape(tmpImPairs, length(tmpImPairs)/2, 2);
        tmpImPairs( or(tmpImPairs ==  ImPairs(k,2), tmpImPairs(:,[2 1]) ==  ImPairs(k,2) ) ) = [];
        tmpImPairs = reshape(tmpImPairs, length(tmpImPairs)/2, 2);
    end
    
else
    ImPairs = tmpImPairs;
end



end

