function [chosenLR,t] = chooseBestFromPair( ImCube, ImPairs, varargin )
%UNTITLED Summary of this function goes here
%   chosenLR = chooseBestFromPair( ImCube, ImPairs, PrevChosenLR, 'color' )

NPairs = size(ImPairs,1);

tmp = cellfun(@isnumeric, varargin);
tmpInd = find(tmp == 1);
if nargin > 2 && ~isempty(tmpInd)
    chosenLR = varargin{tmpInd};
else
    chosenLR = zeros(NPairs,2);
end

tmp = cellfun(@ischar, varargin);
tmpInd = find(tmp == 1);
if nargin > 2 && ~isempty(tmpInd)
    color = varargin{tmpInd};
else
    color = 'jet';
end

f = figure('Name','Evaluate Methods by Comparison - Choose the best image',...
    'NumberTitle','off');

h_l = subaxis(1,2,1, 'Spacing',0.01, 'SpacingVert',0.05,...
    'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.2, 'Padding',0);

h_r = subaxis(1,2,2, 'Spacing',0.01, 'SpacingVert',0.05,...
    'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.2, 'Padding',0);

uicontrol('Style','pushbutton', 'String','Left', 'Units','normalized',...
    'Position',[(0.25-0.1) 0.05 0.2 0.1], 'Callback', {@buttonPress_lr,'left'});
uicontrol('Style','pushbutton', 'String','Right', 'Units','normalized',...
    'Position',[(0.75-0.1) 0.05 0.2 0.1], 'Callback', {@buttonPress_lr,'right'});

txt = uicontrol('Style','text', 'String','', 'Units','normalized',...
    'FontSize',10, 'Position',[0.43 0.93 0.14 0.05]);

set(f,'UserData',{'' 0});
t = zeros(NPairs,1);
for k = 1:NPairs
    set(txt, 'String',[num2str(NPairs - k + 1) ' pairs left'])
    tic
    isLeftBest = compareImPair( ImCube(:,:, ImPairs(k,1) ), ImCube(:,:, ImPairs(k,2) ), f, h_l, h_r, color);
    t(k) = toc;
    if isLeftBest
        chosenLR(k,1) = chosenLR(k,1) + 1;
    else
        chosenLR(k,2) = chosenLR(k,2) + 1;
    end
    
end

close(f)

end

