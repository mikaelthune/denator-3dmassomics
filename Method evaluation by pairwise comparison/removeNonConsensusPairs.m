function [ ImPairs_adj, chosenLR_adj, varargout ] = removeNonConsensusPairs( ImPairs, chosenLR, cutOff)
%UNTITLED Summary of this function goes here
%   [ImPairs_adj, chosenLR_adj] = removeNonConsensusPairs( ImPairs, chosenLR, cuttOff )
%   [ImPairs_adj, chosenLR_adj, removedPairs] = removeNonConsensusPairs( ImPairs, chosenLR, cuttOff )
%   cutOff = 1 means all users must agree on the same image.

disp(' ')
if cutOff <= 0.5
    error('cut off value must be > 0.5')
end
warning(['Using agreement cut-off = ' num2str(cutOff)])

rowSum = sum(chosenLR,2);
ratioLeft = chosenLR(:,1) ./ rowSum;
ratioRight = chosenLR(:,2) ./ rowSum;

nonConsensusPairs = and( ratioLeft < cutOff, ratioRight < cutOff);
 
chosenLR_adj = chosenLR;
chosenLR_adj(nonConsensusPairs,:) = [];

ImPairs_adj = ImPairs;
ImPairs_adj(nonConsensusPairs,:) = [];

% Removed Pairs
if nargout > 2
    varargout{1} = ImPairs(nonConsensusPairs,:); 
end

end