function [ ImPairs_merged, chosenLR_merged ] = mergeShuffledPairs( ImPairs_cell, chosenLR_cell )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if length(ImPairs_cell) ~= length(chosenLR_cell)
    error('both input arguments must have the same number of cells')
end

if size(ImPairs_cell{1},1) ~= size(chosenLR_cell{1},1)
    error('Contents in the input cells must have the same dimensionality')
end

Nsets = length(ImPairs_cell);
Npairs = size(ImPairs_cell{1},1);

ImPairs_cell_sort = cell(Nsets,1);
chosenLR_cell_sort = cell(Nsets,1);
for k = 1:Nsets
    [ImPairs_cell_sort{k}, Ind] = sort(ImPairs_cell{k},2);
    
    for p = 1:Npairs
        chosenLR_cell_sort{k}(p,:) = chosenLR_cell{k}(p, Ind(p,:) );
    end
end

ImPairs_merged = ImPairs_cell_sort{1};
chosenLR_merged = chosenLR_cell_sort{1};

for k = 2:Nsets
    for p = 1:Npairs
        [row1,~] = find(ImPairs_cell_sort{k} == ImPairs_merged(p,1));
        [row2,~] = find(ImPairs_cell_sort{k} == ImPairs_merged(p,2));
        trueRow = intersect(row1,row2);
        
        chosenLR_merged(p,:) = chosenLR_merged(p,:) + chosenLR_cell_sort{k}(trueRow,:);
    end
end









end

