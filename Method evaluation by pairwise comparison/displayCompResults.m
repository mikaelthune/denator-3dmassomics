function displayCompResults( ImCube, CompResults, selectedMethod, varargin )
%DISPLAYCOMPRESULTS Displays image pairs in the CompResult struct
%   displayCompResults( ImCube, CompResults, selectedMethod, 'Unison', 'gray' )
%   displayCompResults( ImCube, CompResults, selectedMethod, 'NonUnison', 'gray' )
warning('does not work for old parameters')

if nargin > 3
    if sum( strcmpi(varargin,'gray') ) >= 1
        color = 'gray';
    else
        color = 'jet';
    end
    
    if sum( strcmpi(varargin,'Unison') ) >= 1
        ImPairs = CompResults.method(selectedMethod).unisonPairs;
        pairInd = CompResults.method(selectedMethod).unisonInd;
        chosenLR = CompResults.chosenLR_human( pairInd,: );
        
    elseif sum( strcmpi(varargin,'NonUnison') ) >= 1
        ImPairs = CompResults.method(selectedMethod).nonUnisonPairs;
        pairInd = CompResults.method(selectedMethod).nonUnisonInd;
        chosenLR = CompResults.chosenLR_human( pairInd,: );
    end
    
else
    ImPairs = CompResults.imPairs;
    chosenLR = CompResults.chosenLR_human;
end

values = CompResults.method(selectedMethod).imageValues(pairInd,:);

Npairs = size(ImPairs,1);
Nfigures = ceil(Npairs/4);

for k = 1:Nfigures
    f = figure;
    if (k-1)*4 + 4 > Npairs
        set(f,'Name', [CompResults.method(selectedMethod).name ', Pair '...
            num2str( 4*(k-1) + 1 ) ' - ' num2str( Npairs )],'NumberTitle','off');
    else
        set(f,'Name', [CompResults.method(selectedMethod).name ', Pair '...
            num2str( 4*(k-1) + 1 ) ' - ' num2str( 4*k )],'NumberTitle','off');
    end
    
    count = 1;
    for p = 1:4
        imPairRow = (k-1)*4+p;
        
        subaxis(4,2,count, 'Spacing',0.01, 'SpacingVert',0.08, 'MR',0.01,...
            'ML',0.01,'MT',0.08,'MB',0.01, 'Padding',0);
        imagesc( histEq( ImCube(:,:,ImPairs(imPairRow,1) ) ) );
        axis image
        set(gca,'XTick',[])
        set(gca,'YTick',[])
        
        title({ ['ind = ' num2str(ImPairs(imPairRow,1))],...
            [ 'value = ' num2str(values(imPairRow,1)) ],...
            [ 'human votes = ' num2str(chosenLR(imPairRow,1)) ] })
        
        
        colormap(color)
        count = count + 1;
        
        subaxis(4,2,count, 'Spacing',0.01, 'SpacingVert',0.08, 'MR',0.01,...
            'ML',0.01,'MT',0.08,'MB',0.01, 'Padding',0);
        imagesc( histEq( ImCube(:,:,ImPairs(imPairRow,2) ) ) );
        axis image
        set(gca,'XTick',[])
        set(gca,'YTick',[])

        title({ ['ind = ' num2str(ImPairs(imPairRow,2))],...
            [ 'value = ' num2str(values(imPairRow,2)) ],...
            [ 'human votes = ' num2str(chosenLR(imPairRow,2)) ] })

        
        colormap(color)
        
        if (k-1)*4 + p >= Npairs
            break
        else
            count = count + 1;
        end
        
    end
    
end

