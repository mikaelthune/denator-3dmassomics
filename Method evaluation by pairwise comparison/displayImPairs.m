function displayImPairs( ImCube, ImPairs, varargin )
%UNTITLED3 Summary of this function goes here
%   displayImPairs( ImCube, ImPairs, 'gray', MethodResult )

Npairs = size(ImPairs,1);

if nargin > 2 && sum( strcmpi(varargin,'gray') ) >= 1
    color = 'gray';
else
    color = 'jet';
end

NRows = 3;
Nfigures = ceil(Npairs/NRows);

for k = 1:Nfigures
    f = figure;
    if (k-1)*NRows + NRows > Npairs
        set(f,'Name', ['Pair ' num2str( NRows*(k-1) + 1 ) ' - ' num2str( Npairs )],...
            'NumberTitle','off');
    else
        set(f,'Name', ['Pair ' num2str( NRows*(k-1) + 1 ) ' - ' num2str( NRows*k )],...
            'NumberTitle','off');
    end
    
    count = 1;
    for p = 1:NRows
        imPairRow = (k-1)*NRows+p;
        
        subaxis(NRows,2,count, 'Spacing',0.01, 'SpacingVert',0.07, 'MR',0.01,...
            'ML',0.01,'MT',0.06,'MB',0.01, 'Padding',0);
        imagesc( histEq( ImCube(:,:,ImPairs(imPairRow,1) ) ) );
        axis image
        set(gca,'XTick',[])
        set(gca,'YTick',[])
        ind = cellfun(@isstruct, varargin);
        if nargin > 2 && sum(ind) >= 1
            title({ ['ind = ' num2str(ImPairs(imPairRow,1))],...
                ['value = ' num2str(varargin{ind}.values(ImPairs(imPairRow,1)))] })
        else
            title( num2str( ImPairs(imPairRow,1) ) )
        end
        
        colormap(color)
        count = count + 1;
        
        subaxis(NRows,2,count, 'Spacing',0.01, 'SpacingVert',0.07, 'MR',0.01,...
            'ML',0.01,'MT',0.06,'MB',0.01, 'Padding',0);
        imagesc( histEq( ImCube(:,:,ImPairs(imPairRow,2) ) ) );
        axis image
        set(gca,'XTick',[])
        set(gca,'YTick',[])
        if nargin > 2 && sum(ind) >= 1
            title({ ['ind = ' num2str(ImPairs(imPairRow,2))],...
                ['value = ' num2str(varargin{ind}.values(ImPairs(imPairRow,2)))] })
        else
            title( num2str( ImPairs(imPairRow,2) ) )
        end
        
        colormap(color)
        
        if (k-1)*NRows + p >= Npairs
            break
        else
            count = count + 1;
        end
        
    end
    
end

