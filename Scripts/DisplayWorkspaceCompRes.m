strAll = who;

% Find all CompRes
nonEmptyAll = not( cellfun(@isempty, strfind( upper(strAll), upper('CompRes'))));
indComp = find(nonEmptyAll);
strComp = strAll(indComp);

% Find all SC param versions
nonEmptyComp = not( cellfun(@isempty, strfind( upper(strComp), upper('SC'))));
indSC = find(nonEmptyComp);
strSC = strComp(indSC);

% Divide into two
strOther = strComp; strOther(indSC) = [];
NSC = length(strSC);
NOther = length(strOther);

% Display Other methods
disp('------------------------------------------------------------')
for k = 1:NOther
    disp(strOther{k})
    printCompResults( eval(strOther{k}) )
    disp('------------------------------------------------------------')
end
disp('------------------------------------------------------------')

disp('--------------------Spatial Chaos only-----------------------')

disp(strOther{1})
printCompResults( eval(strOther{1}), 6 )
disp('------------------------------------------------------------')
for k = 1:NSC
    disp(strSC{k})
    printCompResults( eval(strSC{k}), 6 )
    disp('------------------------------------------------------------')
end

clear strAll nonEmptyAll indComp strComp nonEmptyComp indSC strSC strOther NSC NOther k;