
dataDir =  'E:\Dokument\Dropbox\Denator\Data\Reproducibility datasets\ImCubes\Noise Added\tmp\';
saveDir = 'E:\Dokument\Dropbox\Denator\Data\Reproducibility datasets\Results From Methods\Unprocessed dataset\13x13\Noise added\';
window = [13 13];
% Copy this into loop

noiseInDatStr = ~isempty(strfind(lower(dataDir),'noise'));
noiseInSavStr = ~isempty(strfind(lower(saveDir),'noise'));
if noiseInDatStr && noiseInSavStr
    noiseAdd = true;
elseif ~noiseInDatStr && ~noiseInSavStr
    noiseAdd = false;
else
    error('Directories does not match')
end

disp(' ')
if noiseAdd
    disp('Using NOISE data')
    disp(['Window = ' num2str(window(1)) 'x' num2str(window(2)) ])
else
    disp('Using DEFAULT data')
    disp(['Window = ' num2str(window(1)) 'x' num2str(window(2)) ])
end

FileNameCell = findAllMatFiles( dataDir );
NFiles = length(FileNameCell);
disp(['Found ' num2str(NFiles) ' mat-files ind directory'])

for k = 1:NFiles
    % Copied --------------------------------------
    dataDir =  'E:\Dokument\Dropbox\Denator\Data\Reproducibility datasets\ImCubes\Noise Added\tmp\';
    saveDir = 'E:\Dokument\Dropbox\Denator\Data\Reproducibility datasets\Results From Methods\Unprocessed dataset\13x13\Noise added\';
    window = [13 13];
    noiseInDatStr = ~isempty(strfind(lower(dataDir),'noise'));
    noiseInSavStr = ~isempty(strfind(lower(saveDir),'noise'));
    if noiseInDatStr && noiseInSavStr
        noiseAdd = true;
    elseif ~noiseInDatStr && ~noiseInSavStr
        noiseAdd = false;
    else
        error('Directories does not match')
    end
    % Copied --------------------------------------
    
    FileNameCell = findAllMatFiles( dataDir );
    
    tic;
    load(fullfile(dataDir,FileNameCell{k}))
    
    if noiseAdd
        eval(['ImCube = ImCube_Noise_' FileNameCell{k}(1:16) ';']);
        eval(['mz = mz_' FileNameCell{k}(1:16) ';']);
        
    else
        eval(['ImCube = ImCube_' FileNameCell{k}(1:end-4) ';']);
        eval(['mz = mz_' FileNameCell{k}(1:end-4) ';']);
        
    end
    
    ResultFromImcube = evaluateMethodsParallel(ImCube, mz, window);
    
    save(fullfile(saveDir,['MethodResult_' FileNameCell{k}]), 'ResultFromImcube');
    
    eval(['clear ImCube_Noise_' FileNameCell{k}(1:end-4) 'mz_' FileNameCell{k}(1:end-4)]);
    
    disp([FileNameCell{k} ' Finished!'])
    t = toc;
    timeLeft =t*(length(FileNameCell)-k);
    disp(['Estimated time remaining: ' num2str(round(timeLeft/60)) ' minutes'])
    
    clear classes
    clear java
end

