%%
testMethodResult1 = ResultCell_13x13_default;
test1_1 = nan(36,1);
test1_2 = nan(36,1);
test1_3 = nan(36,1);
test1_4 = nan(36,1);
for k = 1:36
    test1_1(k) = testMethodResult1{k}.method(1).values(1);
    test1_2(k) = testMethodResult1{k}.method(1).values(1002);
    test1_3(k) = testMethodResult1{k}.method(6).values(13);
    test1_4(k) = testMethodResult1{k}.method(6).values(547);
end

testMethodResult2 = ResultCell_9x9_default;
test2_1 = nan(36,1);
test2_2 = nan(36,1);
test2_3 = nan(36,1);
test2_4 = nan(36,1);
for k = 1:36
    test2_1(k) = testMethodResult2{k}.method(1).values(1);
    test2_2(k) = testMethodResult2{k}.method(1).values(2452);
    test2_3(k) = testMethodResult2{k}.method(4).values(13);
    test2_4(k) = testMethodResult2{k}.method(4).values(547);
end

testMethodResult3 = ResultCell_5x5_default;
test3_1 = nan(36,1);
test3_2 = nan(36,1);
test3_3 = nan(36,1);
test3_4 = nan(36,1);
for k = 1:36
    test3_1(k) = testMethodResult3{k}.method(3).values(1);
    test3_2(k) = testMethodResult3{k}.method(3).values(3002);
    test3_3(k) = testMethodResult3{k}.method(5).values(13);
    test3_4(k) = testMethodResult3{k}.method(5).values(547);
end


%% 
[length(unique( test1_1 )); ...
length(unique( test1_2 )); ...
length(unique( test1_3 )); ...
length(unique( test1_4 )) ]

[length(unique( test2_1 )); ...
length(unique( test2_2 )); ...
length(unique( test2_3 )); ...
length(unique( test2_4 )) ]

[length(unique( test3_1 )); ...
length(unique( test3_2 )); ...
length(unique( test3_3 )); ...
length(unique( test3_4 )) ]
