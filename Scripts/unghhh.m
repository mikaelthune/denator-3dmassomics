% Brain ----------------------------
CompRes_Brain_Set1_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_5x5_SC_params3, chosenLR_Brain_Set1_30_adj,'nodisp');

CompRes_Brain_Set2_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_5x5_SC_params3, chosenLR_Brain_Set2_40_adj,'nodisp');


% Kidney ----------------------------
CompRes_Kidney_Set1_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_5x5_SC_params3, chosenLR_Kidney_Set1_40x2_adj,'nodisp');

CompRes_Kidney_Set2_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_5x5_SC_params3, chosenLR_Kidney_Set2_40_adj,'nodisp');

% Pan1 ----------------------------
CompRes_Pan1_Set1_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_5x5_SC_params3, chosenLR_Pan1_Set1_50_adj,'nodisp');

CompRes_Pan1_Set2_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_5x5_SC_params3, chosenLR_Pan1_Set2_40_adj,'nodisp');







