% matlabpool open

%% Pilot Study
load('E:\Dokument\Dropbox\Denator\Data\Pilot Survey\Images\pairInfo_randIms.mat', 'ImPairs_randIms')
load('E:\Dokument\Dropbox\Denator\Data\Pilot Survey\Images\ImCubes_pilot_NaNFixed.mat', 'ImCube_randIms')
load('E:\Dokument\Dropbox\Denator\Data\Pilot Survey\Results from Survey\SurveyStruct 11 responses.mat', 'SurveyStruct')

FullSurveyStruct_plt = convertPilotSurveyStruct( SurveyStruct, ImPairs_randIms );
ResponseSummary_plt = createResponseSummary( FullSurveyStruct_plt );
cutOff = 0.85;
[chosenLR_adj, ImPairs_adj] = getChosenLRForPilotOptimisation( ResponseSummary_plt, cutOff );


%% Default  ------------------------------------------------------------------------------------
NPairs = size(ImPairs_adj,1);
[mzInd, ~, ic] = unique(ImPairs_adj(:));
Nind = length(mzInd);

methodVals = zeros(Nind,1);
parfor k = 1:Nind
    methodVals(k) = calcMoC(ImCube_randIms(:,:,mzInd(k)));
end

ValuePairs = reshape(methodVals(ic),NPairs,2);
IsLeftBest_method = ValuePairs(:,1) < ValuePairs(:,2);

isLeftBest_human = descisionListFromHumanComp(chosenLR_adj);

accuracy = -sum( isLeftBest_human == IsLeftBest_method )./NPairs;

save(['E:\Dokument\Dropbox\Denator\Data\Pilot Survey\Optimisation\DefaultParams_cutOff' num2str(cutOff) '.mat'],...
    'accuracy', 'methodVals', 'ValuePairs');

clear NPairs mzInd ic methodVals ValuePairs IsLeftBest_method accuracy
%--------------------------------------------------------------------------------------


%%  Using chosenLR-----------------------------------------------------------------------------------------
[params_plt, fval_plt, exitflag_plt, output_plt, population_plt, scores_plt] = ...
    optimiseMoC( ImCube_randIms, ImPairs_adj, chosenLR_adj, 1 );
save(['E:\Dokument\Dropbox\Denator\Data\Pilot Survey\Optimisation\optimResults1_cutOff' num2str(cutOff) '.mat'],...
    'params_plt', 'fval_plt', 'exitflag_plt', 'output_plt', 'population_plt', 'scores_plt');

clear params_plt fval_plt exitflag_plt output_plt population_plt scores_plt
% -----------------------------------------------------------------------------------------
[params_plt, fval_plt, exitflag_plt, output_plt, population_plt, scores_plt] = ...
    optimiseMoC( ImCube_randIms, ImPairs_adj, chosenLR_adj, 1 );
save(['E:\Dokument\Dropbox\Denator\Data\Pilot Survey\Optimisation\optimResults2_cutOff' num2str(cutOff) '.mat'],...
    'params_plt', 'fval_plt', 'exitflag_plt', 'output_plt', 'population_plt', 'scores_plt');

clear params_plt fval_plt exitflag_plt output_plt population_plt scores_plt
% -----------------------------------------------------------------------------------------
[params_plt, fval_plt, exitflag_plt, output_plt, population_plt, scores_plt] = ...
    optimiseMoC( ImCube_randIms, ImPairs_adj, chosenLR_adj, 1 );
save(['E:\Dokument\Dropbox\Denator\Data\Pilot Survey\Optimisation\optimResults3_cutOff' num2str(cutOff) '.mat'],...
    'params_plt', 'fval_plt', 'exitflag_plt', 'output_plt', 'population_plt', 'scores_plt');

clear params_plt fval_plt exitflag_plt output_plt population_plt scores_plt
% -----------------------------------------------------------------------------------------
