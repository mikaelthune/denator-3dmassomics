% Brain ----------------------------
CompRes_Brain_Set1_3x3_SC_orig_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_3x3_SC_orig, chosenLR_Brain_Set1_30_adj,'nodisp');
CompRes_Brain_Set1_5x5_SC_orig_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_5x5_SC_orig, chosenLR_Brain_Set1_30_adj,'nodisp');
CompRes_Brain_Set1_9x9_SC_orig_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_9x9_SC_orig, chosenLR_Brain_Set1_30_adj,'nodisp');
CompRes_Brain_Set1_13x13_SC_orig_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_13x13_SC_orig, chosenLR_Brain_Set1_30_adj,'nodisp');

CompRes_Brain_Set2_3x3_SC_orig_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_3x3_SC_orig, chosenLR_Brain_Set2_40_adj,'nodisp');
CompRes_Brain_Set2_5x5_SC_orig_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_5x5_SC_orig, chosenLR_Brain_Set2_40_adj,'nodisp');
CompRes_Brain_Set2_9x9_SC_orig_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_9x9_SC_orig, chosenLR_Brain_Set2_40_adj,'nodisp');
CompRes_Brain_Set2_13x13_SC_orig_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_13x13_SC_orig, chosenLR_Brain_Set2_40_adj,'nodisp');
disp('B')

% Kidney ----------------------------
CompRes_Kidney_Set1_3x3_SC_orig_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_3x3_SC_orig, chosenLR_Kidney_Set1_40x2_adj,'nodisp');
CompRes_Kidney_Set1_5x5_SC_orig_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_5x5_SC_orig, chosenLR_Kidney_Set1_40x2_adj,'nodisp');
CompRes_Kidney_Set1_9x9_SC_orig_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_9x9_SC_orig, chosenLR_Kidney_Set1_40x2_adj,'nodisp');
CompRes_Kidney_Set1_13x13_SC_orig_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_13x13_SC_orig, chosenLR_Kidney_Set1_40x2_adj,'nodisp');

CompRes_Kidney_Set2_3x3_SC_orig_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_3x3_SC_orig, chosenLR_Kidney_Set2_40_adj,'nodisp');
CompRes_Kidney_Set2_5x5_SC_orig_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_5x5_SC_orig, chosenLR_Kidney_Set2_40_adj,'nodisp');
CompRes_Kidney_Set2_9x9_SC_orig_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_9x9_SC_orig, chosenLR_Kidney_Set2_40_adj,'nodisp');
CompRes_Kidney_Set2_13x13_SC_orig_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_13x13_SC_orig, chosenLR_Kidney_Set2_40_adj,'nodisp');
disp('K')

% Pan1 ----------------------------
CompRes_Pan1_Set1_3x3_SC_orig_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_3x3_SC_orig, chosenLR_Pan1_Set1_50_adj,'nodisp');
CompRes_Pan1_Set1_5x5_SC_orig_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_5x5_SC_orig, chosenLR_Pan1_Set1_50_adj,'nodisp');
CompRes_Pan1_Set1_9x9_SC_orig_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_9x9_SC_orig, chosenLR_Pan1_Set1_50_adj,'nodisp');
CompRes_Pan1_Set1_13x13_SC_orig_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_13x13_SC_orig, chosenLR_Pan1_Set1_50_adj,'nodisp');

CompRes_Pan1_Set2_3x3_SC_orig_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_3x3_SC_orig, chosenLR_Pan1_Set2_40_adj,'nodisp');
CompRes_Pan1_Set2_5x5_SC_orig_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_5x5_SC_orig, chosenLR_Pan1_Set2_40_adj,'nodisp');
CompRes_Pan1_Set2_9x9_SC_orig_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_9x9_SC_orig, chosenLR_Pan1_Set2_40_adj,'nodisp');
CompRes_Pan1_Set2_13x13_SC_orig_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_13x13_SC_orig, chosenLR_Pan1_Set2_40_adj,'nodisp');
disp('P')







