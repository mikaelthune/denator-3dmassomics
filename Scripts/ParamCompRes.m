% Brain ----------------------------
CompRes_Brain_Set1_5x5_SC_params1_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_5x5_SC_params1, chosenLR_Brain_Set1_30_adj);
CompRes_Brain_Set1_5x5_SC_params2_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_5x5_SC_params2, chosenLR_Brain_Set1_30_adj);
CompRes_Brain_Set1_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_5x5_SC_params3, chosenLR_Brain_Set1_30_adj);
CompRes_Brain_Set1_5x5_SC_paramsPan_adj = evaluateByPairComp(ImPairs_Brain_Set1_30_adj, ResultBrain_5x5_SC_paramsPan, chosenLR_Brain_Set1_30_adj);

CompRes_Brain_Set2_5x5_SC_params1_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_5x5_SC_params1, chosenLR_Brain_Set2_40_adj);
CompRes_Brain_Set2_5x5_SC_params2_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_5x5_SC_params2, chosenLR_Brain_Set2_40_adj);
CompRes_Brain_Set2_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_5x5_SC_params3, chosenLR_Brain_Set2_40_adj);
CompRes_Brain_Set2_5x5_SC_paramsPan_adj = evaluateByPairComp(ImPairs_Brain_Set2_40_adj, ResultBrain_5x5_SC_paramsPan, chosenLR_Brain_Set2_40_adj);
disp('Brain CompRes')

% Kidney ----------------------------
CompRes_Kidney_Set1_5x5_SC_params1_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_5x5_SC_params1, chosenLR_Kidney_Set1_40x2_adj);
CompRes_Kidney_Set1_5x5_SC_params2_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_5x5_SC_params2, chosenLR_Kidney_Set1_40x2_adj);
CompRes_Kidney_Set1_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_5x5_SC_params3, chosenLR_Kidney_Set1_40x2_adj);
CompRes_Kidney_Set1_5x5_SC_paramsPan_adj = evaluateByPairComp(ImPairs_Kidney_Set1_40x2_adj, ResultKidney_5x5_SC_paramsPan, chosenLR_Kidney_Set1_40x2_adj);

CompRes_Kidney_Set2_5x5_SC_params1_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_5x5_SC_params1, chosenLR_Kidney_Set2_40_adj);
CompRes_Kidney_Set2_5x5_SC_params2_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_5x5_SC_params2, chosenLR_Kidney_Set2_40_adj);
CompRes_Kidney_Set2_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_5x5_SC_params3, chosenLR_Kidney_Set2_40_adj);
CompRes_Kidney_Set2_5x5_SC_paramsPan_adj = evaluateByPairComp(ImPairs_Kidney_Set2_40_adj, ResultKidney_5x5_SC_paramsPan, chosenLR_Kidney_Set2_40_adj);
disp('kidney CompRes')

% Pan1 ----------------------------
CompRes_Pan1_Set1_5x5_SC_params1_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_5x5_SC_params1, chosenLR_Pan1_Set1_50_adj);
CompRes_Pan1_Set1_5x5_SC_params2_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_5x5_SC_params2, chosenLR_Pan1_Set1_50_adj);
CompRes_Pan1_Set1_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_5x5_SC_params3, chosenLR_Pan1_Set1_50_adj);
CompRes_Pan1_Set1_5x5_SC_paramsPan_adj = evaluateByPairComp(ImPairs_Pan1_Set1_50_adj, ResultPan1_5x5_SC_paramsPan, chosenLR_Pan1_Set1_50_adj);

CompRes_Pan1_Set2_5x5_SC_params1_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_5x5_SC_params1, chosenLR_Pan1_Set2_40_adj);
CompRes_Pan1_Set2_5x5_SC_params2_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_5x5_SC_params2, chosenLR_Pan1_Set2_40_adj);
CompRes_Pan1_Set2_5x5_SC_params3_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_5x5_SC_params3, chosenLR_Pan1_Set2_40_adj);
CompRes_Pan1_Set2_5x5_SC_paramsPan_adj = evaluateByPairComp(ImPairs_Pan1_Set2_40_adj, ResultPan1_5x5_SC_paramsPan, chosenLR_Pan1_Set2_40_adj);
disp('pan1 CompRes')