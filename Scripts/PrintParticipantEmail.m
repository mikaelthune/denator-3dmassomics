directory = 'C:\Users\mikael.thune\Dropbox\Denator\Survey\Full Survey\Results\ConsistencyCheck';
endString = 'QEval_CommentTime';
load('C:\Users\mikael.thune\Dropbox\Denator\Data\Full Survey\Images\ImPairs 75 Surveys.mat', 'SurveyPairList', 'ImPairs_rand');

NRuns = 11;

warning(['Number of runs = ' num2str(NRuns)])

Mails = {};
IPs = {};
for k = 1:NRuns
    FullSurveyStruct = createFullSurveyStruct( ...
        fullfile(directory,['Run' num2str(k)]), 6, endString, SurveyPairList, ImPairs_rand);
    Mails = [Mails {FullSurveyStruct.Response(:).Mail}];
    IPs = [IPs {FullSurveyStruct.Response(:).IPAddress}];
end

Mails = Mails';
IPs = IPs';
warning(['Number of runs = ' num2str(NRuns)])
clear FullSurveyStruct NRuns directory k