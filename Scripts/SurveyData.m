Nresp = SurveyStruct.numberOfResponses;

%% Find slider values = 0
sliderZero = cell(Nresp,2);
for k = 1:Nresp
    sliderZero{k,1} = SurveyStruct.User(k).sliderValue == 0;
    ind = find(sliderZero{k,1});
    sliderZero{k,2} = length(ind);
    sliderZero{k,3} = SurveyStruct.User(k).pairNumber(ind);
end

pairsWithZeroOccurence = cell2mat(sliderZero(:,3));
zeroPairs = unique(pairsWithZeroOccurence);
zeroCount = histc(pairsWithZeroOccurence, zeroPairs);

bar(zeroPairs,zeroCount)
title('Number of Slider Zero-values For Each Pair')
xlabel('Pair Number')
ylabel('Number of Zeros')

%% Display pairs that a user chose zero on

close all
for m = 1:Nresp
    pairsToDisp = ImPairs_randIms(sliderZero{m,3},:);
    Npairs = size(pairsToDisp,1);
    color = 'jet';
    if Npairs > 0
        Nfigures = ceil(Npairs/4);
        if Npairs < 4
            rows = Npairs;
        else
            rows = 4;
        end
        pairCount = 1;
        for k = 1:Nfigures
            f = figure;
            set(gcf,'Name',['User ' num2str(m) ', fig ' num2str(k)],'NumberTitle','off')
            
            count = 1;
            for p = 1:rows
                imPairRow = (k-1)*4+p;
                
                subaxis(rows,2,count, 'Spacing',0.01, 'SpacingVert',0.05, 'MR',0.01,...
                    'ML',0.01,'MT',0.06,'MB',0.01, 'Padding',0);
                h = imagesc( ImCube_randIms(:,:,pairsToDisp(imPairRow,1) ) );
                axis image
                set(gca,'XTick',[])
                set(gca,'YTick',[])
                title( num2str( sliderZero{m,3}(pairCount) ) )
                set(gca,'xcolor','w', 'ycolor','w', 'box','off')
                set(h, 'alphadata', tissuemask);
                
                colormap(color)
                count = count + 1;
                
                subaxis(rows,2,count, 'Spacing',0.01, 'SpacingVert',0.05, 'MR',0.01,...
                    'ML',0.01,'MT',0.06,'MB',0.01, 'Padding',0);
                h = imagesc( ImCube_randIms(:,:,pairsToDisp(imPairRow,2) ) );
                axis image
                set(gca,'XTick',[])
                set(gca,'YTick',[])
                set(gca,'xcolor','w', 'ycolor','w', 'box','off')
                set(h, 'alphadata', tissuemask);
                
                colormap(color)
                
                if (k-1)*4 + p >= Npairs
                    break
                else
                    count = count + 1;
                end
                pairCount = pairCount + 1;
                
            end
        end
    end
end
