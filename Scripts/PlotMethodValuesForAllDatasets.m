window = [5 5];

dataDir1 = 'E:\Dokument\Dropbox\Denator\Data\Reproducibility datasets\Results From Methods';
dataDir2 = 'Unprocessed dataset\';
dataDir3 = [num2str(window(1)) 'x' num2str(window(2)) '\'];
dataDir4 = 'Default\';

mainSaveDir = 'E:\Dokument\Dropbox\Denator\Temp\datasets plots\Image scores in reproducibility data\';
savedir = fullfile(mainSaveDir,dataDir2,dataDir3,dataDir4);

titleStr{1} = 'STD-Mean';
titleStr{2} = 'STD-Median';
titleStr{3} = 'SNR-Mean';
titleStr{4} = 'SNR-Median';
titleStr{5} = 'MSE';
titleStr{6} = 'Spatial Chaos';

load(fullfile(dataDir1,dataDir2,dataDir3,dataDir4,'All.mat'), 'ResultCell');
NResults = length(ResultCell);

dataName = {};
for k = 1:3
    for m = 1:12
        if m < 10
            dataName = {dataName{:} ['block' num2str(k) ' section0' num2str(m)]};
        else
            dataName = {dataName{:} ['block' num2str(k) ' section' num2str(m)]};
        end
    end
end

rng = [1:4;5:8;9:12;13:16;17:20;21:24;25:28;29:32;33:36];

for m = 1:6
    for k = 1:NResults/4
        
%         Result1 = AllResultsCell{rng(k,1)}.method(m).values ./ abs(max(AllResultsCell{rng(k,1)}.method(m).values));
%         Result2 = AllResultsCell{rng(k,2)}.method(m).values ./ abs(max(AllResultsCell{rng(k,2)}.method(m).values));
%         Result3 = AllResultsCell{rng(k,3)}.method(m).values ./ abs(max(AllResultsCell{rng(k,3)}.method(m).values));
%         Result4 = AllResultsCell{rng(k,4)}.method(m).values ./ abs(max(AllResultsCell{rng(k,4)}.method(m).values));
        Result1 = ResultCell{rng(k,1)}.method(m).values;
        Result2 = ResultCell{rng(k,2)}.method(m).values;
        Result3 = ResultCell{rng(k,3)}.method(m).values;
        Result4 = ResultCell{rng(k,4)}.method(m).values;
        
        f = figure; plot([sort(Result1) sort(Result2) sort(Result3) sort(Result4)]);
        
        yMax = max([Result1;Result2;Result3;Result4]);
        tmp = axis;
        axis([tmp(1) tmp(2) 0 yMax])
        
        if m >= 5
            title({['Method: ' titleStr{m}] ['Sorted scores from ' dataDir4(1:end-1) ' datasets: ' num2str(rng(k,:))]});
        else
            title({['Method: ' titleStr{m} ' ' dataDir3(1:end-1)] ['Sorted scores from ' dataDir4(1:end-1) ' datasets: ' num2str(rng(k,:))]});
        end
        ylabel('Score')
        legend(num2str(dataName{rng(k,1)}), num2str(dataName{rng(k,2)}),...
            num2str(dataName{rng(k,3)}), num2str(dataName{rng(k,4)}), 'Location', 'EastOutside')
        pos = get(f,'Position');
        pos = pos + [-300, -150, 400, 150];
        set(f,'Position',pos);
        saveas(f,fullfile( savedir, [titleStr{m} '_' num2str(window(1)) 'x' num2str(window(2)) ' ' num2str( rng(k,:) ) '.fig'] ))
        saveas(f,fullfile( savedir, [titleStr{m} '_' num2str(window(1)) 'x' num2str(window(2)) ' ' num2str( rng(k,:) ) '.png'] ))
        
    end
    close all
end

clear Result1 Result2 Result3 Result4 f rng yMax tmp pos titleStr NResults;

