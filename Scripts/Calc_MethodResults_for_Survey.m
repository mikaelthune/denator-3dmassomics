% MethodResPilotRep_high_3x3 = evaluateMethodsParallel(ImCube_repeat, labels_repeat, [3 3], 'labels');
% MethodResPilotRep_high_5x5 = evaluateMethodsParallel(ImCube_repeat, labels_repeat, [5 5], 'labels');
% MethodResPilotRep_high_9x9 = evaluateMethodsParallel(ImCube_repeat, labels_repeat, [9 9], 'labels');

MethodResPilotRep_SC1_5x5 = evaluateMethodsParallel(ImCube_repeat, labels_repeat, [5 5], params1, 'labels');
MethodResPilotRep_SC2_5x5 = evaluateMethodsParallel(ImCube_repeat, labels_repeat, [5 5], params2, 'labels');
MethodResPilotRep_SC3_5x5 = evaluateMethodsParallel(ImCube_repeat, labels_repeat, [5 5], paramsDouble, 'labels');

MethodResPilot_SC1_5x5 = evaluateMethodsParallel(ImCube_randIms, labels_randIms, [5 5], params1, 'labels');
MethodResPilot_SC2_5x5 = evaluateMethodsParallel(ImCube_randIms, labels_randIms, [5 5], params2, 'labels');
MethodResPilot_SC3_5x5 = evaluateMethodsParallel(ImCube_randIms, labels_randIms, [5 5], paramsDouble, 'labels');

