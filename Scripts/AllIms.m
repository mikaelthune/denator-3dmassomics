

figure
for k = 1:15
    subaxis(3,5,k, 'Spacing',0.01, 'SpacingVert',0.05, 'MR',0.01,...
        'ML',0.01,'MT',0.06,'MB',0.01, 'Padding',0);
    h = imagesc( ImCube_randIms(:,:,k) );
    axis image
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    set(gca,'xcolor','w', 'ycolor','w', 'box','off', 'visible','off')
    set(h, 'alphadata', tissuemask);
end
clear h;

% figure
% for k = 1:2
%     subaxis(1,2,k, 'Spacing',0.01, 'SpacingVert',0.05, 'MR',0.01,...
%         'ML',0.01,'MT',0.06,'MB',0.01, 'Padding',0);
%     h = imagesc( ImCube_repeat(:,:,k) );
%     axis image
%     set(gca,'XTick',[])
%     set(gca,'YTick',[])
%     set(gca,'xcolor','w', 'ycolor','w', 'box','off', 'visible','off')
%     set(h, 'alphadata', tissuemask);
% end
% clear h;