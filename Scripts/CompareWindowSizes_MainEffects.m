%% Initialise
directory = 'C:\Users\mikael.thune\Dropbox\Denator\Data\Reproducibility datasets\Dataset Descriptors\';
load(fullfile(directory,'DatasetDescriptorCells.mat'), ...
    'DatasetDescriptorCell_13x13_default', 'DatasetDescriptorCell_9x9_default', 'DatasetDescriptorCell_5x5_default');
DatasetDescriptorWinSize = {DatasetDescriptorCell_5x5_default, ...
    DatasetDescriptorCell_9x9_default, DatasetDescriptorCell_13x13_default};

NWin = length(DatasetDescriptorWinSize);
NImDescriptors = length(DatasetDescriptorWinSize{1}{1}.imMethod);
NDatDescriptors = length(DatasetDescriptorWinSize{1}{1}.imMethod(1).descriptor);

%% Find indices of the STD and SNR Image descriptors
% (These are the ones that depend on kernel window size)
methodNames = cell(NImDescriptors,1);
for k = 1:NImDescriptors
    methodNames{k} = DatasetDescriptorWinSize{1}{1}.imMethod(k).name;
end
wasFound = or(findStringInCell(methodNames, 'SNR'), findStringInCell(methodNames, 'STD'));
methodsOfInterest = find(wasFound);
NOfInterest = length(methodsOfInterest);

%% Loop thorugh different window sizes
TestMatrixCell = cell(NWin,1);
for w = 1:NWin
    factorLevels = extractFactorLevelsFromDatasetDescriptors(DatasetDescriptorWinSize{w});
    NFactors = length(DatasetDescriptorWinSize{w}{1}.factor);
    
    testMatrix = nan(2*NFactors, NImDescriptors*NDatDescriptors);
    count = 0;
    %% Loop through the different Image descriptors
    for im = 1:NImDescriptors
        %% Loop through the different Dataset descriptors
        for d = 1:NDatDescriptors
            response = extractResponseFromDatasetDescriptors(DatasetDescriptorWinSize{w}, im, d);
            [lowValues, highValues] = calculateMainEffectsLowHigh(response, factorLevels);
            
            count = count + 1;
            % Interlace lowValues and highValues into single vector
            tmp = [lowValues highValues]';
            testMatrix(:,count) = reshape(tmp, NFactors*2, 1);
        end
    end
    
    % Save results for window
    TestMatrixCell{w} = testMatrix;
    
end

%% Compare results for the different window sizes
% Find indices of interest
indOfInterest = nan(1, NOfInterest*NDatDescriptors);
for k = 1:NOfInterest
    startInd = (methodsOfInterest(k)-1)*NDatDescriptors+1;
    indOfInterest((k-1)*NDatDescriptors+1:k*NDatDescriptors) = ...
        [startInd:startInd+NDatDescriptors-1];
end

% Correlation for methods that depend on kernel size
dependCorrFor1_2 = nan(NOfInterest*NDatDescriptors, 1);
dependCorrFor1_3 = nan(NOfInterest*NDatDescriptors, 1);
dependCorrFor2_3 = nan(NOfInterest*NDatDescriptors, 1);
count = 0;
for k = indOfInterest
    count = count + 1;
    dependCorrFor1_2(count) = corr(TestMatrixCell{1}(:,k), TestMatrixCell{2}(:,k));
    dependCorrFor1_3(count) = corr(TestMatrixCell{1}(:,k), TestMatrixCell{3}(:,k));
    dependCorrFor2_3(count) = corr(TestMatrixCell{2}(:,k), TestMatrixCell{3}(:,k));
end

% Check other methods as well. Should be = 1
indShouldBe1 = setdiff(1:NDatDescriptors*NImDescriptors, indOfInterest);
tmpCorrFor1_2 = nan((NImDescriptors-NOfInterest)*NDatDescriptors, 1);
tmpCorrFor1_3 = nan((NImDescriptors-NOfInterest)*NDatDescriptors, 1);
tmpCorrFor2_3 = nan((NImDescriptors-NOfInterest)*NDatDescriptors, 1);
count = 0;
for k = indShouldBe1
    count = count + 1;
    tmpCorrFor1_2(count) = corr(TestMatrixCell{1}(:,k), TestMatrixCell{2}(:,k));
    tmpCorrFor1_3(count) = corr(TestMatrixCell{1}(:,k), TestMatrixCell{3}(:,k));
    tmpCorrFor2_3(count) = corr(TestMatrixCell{2}(:,k), TestMatrixCell{3}(:,k));
end

%% Plot
figure('Name','Correlation between methods that depend on kernel size');
subaxis(1,3,1, 'Spacing',0.09, 'SpacingVert',0.1, 'MR',0.02,...
            'ML',0.1,'MT',0.1,'MB',0.1, 'Padding',0);
plot(dependCorrFor1_2, '*'); title('5x5 vs 9x9')
axis([0, length(indOfInterest), min([dependCorrFor1_2;dependCorrFor1_3;dependCorrFor2_3]), 1])
ylabel('Correlation')
xlabel('Index')

subaxis(1,3,2, 'Spacing',0.09, 'SpacingVert',0.1, 'MR',0.02,...
            'ML',0.1,'MT',0.1,'MB',0.1, 'Padding',0);
plot(dependCorrFor1_3, '*'); title('5x5 vs 13x13')
axis([0, length(indOfInterest), min([dependCorrFor1_2;dependCorrFor1_3;dependCorrFor2_3]), 1])
xlabel('Index')

subaxis(1,3,3, 'Spacing',0.09, 'SpacingVert',0.1, 'MR',0.02,...
            'ML',0.1,'MT',0.1,'MB',0.1, 'Padding',0);
plot(dependCorrFor2_3, '*'); title('9x9 vs 13x13')
axis([0, length(indOfInterest), min([dependCorrFor1_2;dependCorrFor1_3;dependCorrFor2_3]), 1])
xlabel('Index')

%% Clear some temporary variables
clear count d im k w tmp response startInd NFactors lowValues highValues
