FullSurveyStruct = createFullSurveyStruct( 'C:\Users\mikael.thune\Dropbox\Denator\Survey\Full Survey\Results\ConsistencyCheck\Temp', 6, endString, SurveyPairList, ImPairs_rand )

repSliderVal = [FullSurveyStruct.Response(:).repPairSliderValue];
repLRFlipValue = [FullSurveyStruct.Response(:).repPairLRFlipValue];
repSliderVal(repLRFlipValue) = -1 * repSliderVal(repLRFlipValue);

for k = 1:size(repSliderVal,2);
    sliderValue = FullSurveyStruct.Response(k).sliderValue;
    [~, repSliderVal(:,k)] = scaleSliderValue( sliderValue, repSliderVal(:,k) );
end

stdVals = std(repSliderVal);

%%
figure; plot(sort(stdVals),'*')
figure; hist(stdVals)