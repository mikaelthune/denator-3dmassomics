h = figure('Position', [50, 50, 300, 100]);
dir = 'E:\Dokument\Dropbox\Denator\Formul�r\testImages\Numbers';
for k = 1:200
    title(['Par ' num2str(k)])
    
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    
    set(gcf,'units','centimeters')
    pos = get(gcf,'Position');
    
    set(gcf, 'PaperUnits','centimeters');
    set(gcf, 'PaperSize', [pos(3) pos(4)]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition',[0 0 pos(3) pos(4)]);
    
    saveas(h,fullfile( dir, [num2str(k) '.png'] ))
end




