

[~, repSliderVal_adj] = scaleSliderValue(SurveyStruct.User.sliderValue, ...
    SurveyStruct.User.repPairSliderValue);
repSliderVal_adj = adjustSliderSign(repSliderVal_adj, ...
    SurveyStruct.User.repPairLRFlipValue);

disp(['Run: ' num2str(std(repSliderVal_adj))])