xy=zeros(length(coord),2);

for k = 1:length(coord)
    numChar = length(coord{k});
    for c = 1:numChar
        tmp=coord{k};
        if tmp(c) == 'X'
            xy(k,1)=str2num(tmp((c+1):(c+3)));
            xy(k,2)=str2num(tmp((c+5):(c+7)));
            break
        end
    end
end