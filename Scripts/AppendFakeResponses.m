consistencyCheckDir = 'C:\Users\mikael.thune\Dropbox\Denator\Survey\Full Survey\Results\ConsistencyCheck';
[SurveyIDs_sorted, ~, ~, ~, ~, ~] = getSortedInconsistentInParentDir( consistencyCheckDir, 0.2 );

consistentDir = 'C:\Users\mikael.thune\Dropbox\Denator\Survey\Full Survey\Results\Consistent Responses so far';

FullSurveyStruct_consistent = createFullSurveyStruct(consistentDir, 6, 'QEval_CommentTime', SurveyPairList, ImPairs_rand);
FakeSurveyIDs = SurveyIDs_sorted - 10000;
FullSurveyStruct_fakesAdded = generateFakeSurveyResponses( FullSurveyStruct_consistent, SurveyPairList, FakeSurveyIDs );