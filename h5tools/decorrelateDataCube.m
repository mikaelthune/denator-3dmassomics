function [correlationCube, correlationIdx] = decorrelateDataCube(datacube,corrTol)

mzidx = 1 : size(datacube,3);
[nRows, nCols, nMZ] = size(datacube);

datacube = reshape(datacube,nRows*nCols,nMZ);

nanMask = ~isnan(datacube(:,1));

correlationCube = zeros(size(datacube)); %wost case size
correlationIdx = zeros(size(mzidx));
c = 1;
tic
while ~isempty(mzidx)
   % pick a random m/z
    mzChosen = ceil(rand*length(mzidx));
    % calculate correlation to all others

    rho = corr(datacube(nanMask,mzChosen),datacube(nanMask,:));
    if sum(isnan(rho(:))) > 0
        fprintf('nan found in mzChosen %d check for Inf\n',mzChosen)
    end
    rhoThold = find(rho >= corrTol);

    % find maximum
    sumVal = sum(datacube(:,rhoThold));
    [~, maxIdx] = max(sumVal);
    correlationCube(:,c) = datacube(:,rhoThold(maxIdx));
    correlationIdx(c) = rhoThold(maxIdx);
    c= c +1;
    %clear values from mzidx and datacube
    datacube(:,rhoThold) = [];
    mzidx(rhoThold) = [];
    if toc > 1
%         plot(rho)
%         pause(0.01)
        fprintf('%d %d \n',size(mzidx))
        tic
    end
end
clear datacube
correlationCube = correlationCube(:,1:c-1);
correlationIdx = correlationIdx(:,1:c-1);

correlationCube = reshape(correlationCube,nRows,nCols,(c-1));