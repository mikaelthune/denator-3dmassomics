function h5ToMat(headerData,folderOut,dataLabel)
tic
progressbar('saving as .mat')
for n = 1 : length(headerData.spotIdx)
    [mzs,counts]=getPreprocessedSpectrum(headerData,headerData.getSpectrum,n,[],[],[]);
    if n == 1
            save([folderOut filesep dataLabel '_masterMZ' ],'mzs','-v7.3');
    end
    spectrum.mz = find(counts>0);
    spectrum.counts = counts(spectrum.mz);
    x = headerData.spots{n}.registeredCoordinates(1);
    y = headerData.spots{n}.registeredCoordinates(2);
    z = headerData.spots{n}.registeredCoordinates(3);
    spectrum.coordinates = [x,y,z];
    spectrum.coordinateIndex = headerData.spotIdx(n,:);
    spectrum.datasetname=headerData.name;
    save([folderOut filesep dataLabel '_' num2str(n)],'spectrum','-v7.3');
    if toc>3
        progressbar(n/length(headerData.spotIdx))
        tic
    end
end

progressbar(1)