function [datacube,mzs] = loadh5asDatacube(headerData,saveName)
if nargin<2
    saveName = [];
end
% % [~, name, ~] = fileparts(filename);
% % storage = Hdf5Storage('/home/ilia/project/RatH5/Rat_brain_1M_50um_97.h5');
% storage = Hdf5Storage(filename);
% % storage = headerData.storage;
% %Obtain the Region that contains the SpectraGroup of interest, simply use
% %the ID of the SpectraGroup itself, and remove the part after the last "/".
% %The Region is needed to determine the Spots for processing.
% region = scilsmsRegion(storage, '/Regions');
% %Obtain the Spots of the Region, these objects contain the real Spectra
% %data
% spots = region.getSpots;
% %Obtain the SpectraGroup, this object itself does not contain any data, it
% %is only used to obtain the corresponding Spectra from the Spots of the
% fprintf('open storage get spots\n')
% %Region
% spectragroup = scilsmsSpectraGroup(storage, '/Regions/InitialMeasurement');
% fprintf('get spectra group\n')
% spectrumObj = spots{1}.getSpectrum(spectragroup);
%  fprintf('get spec obj\n')
% mzs = spectrumObj.getSamplePositions().samplePositions;
[mzs] = getPreprocessedSpectrum(headerData,headerData.getSpectrum, 1,[],[],[]);
min_mzs = min(mzs);
max_mzs = max(mzs);

datacube = nan+zeros(headerData.nRows,headerData.nColumns,  length(mzs));
% datacube(:) = nan;

for n = 1:length(headerData.spotIdx)
    [~, counts] = getPreprocessedSpectrum(headerData,headerData.getSpectrum, n,[],[],[]);
    datacube(headerData.spotIdx(n,2), headerData.spotIdx(n,1), :) = counts;
    
    if (mod(n, 1000) == 0)
        fprintf('%d / %d\n',n,length(headerData.spotIdx));
    end
end

try
if ~isempty(saveName)
    % If the file name of a mat-file is 'name' then the variables within
%       must be called ImCube_name and mz_name
    [~,name,~] = fileparts(saveName);
    name = regexp(name,'\w+','match');
    name = [name{:}];
    eval(['ImCube_' name '=datacube;'])
    eval(['mz_' name '=mzs;'])
    eval(['save(saveName,''ImCube_' name  ''',''mz_' name ''');'])
    fprintf('saved\n')
end
catch e0
    warning('save probably failed')
    rethrow(e0)
end
