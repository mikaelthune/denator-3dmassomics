%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run this testForPresence - example via
% example_testGroup
%
% This example runs on one the in section 0. defined set of images which 
% has been constructed from the five 
% different structure types, see strucTypes.mat.
% 
%  The RESULT will be a plot of the eight most structured images in the
%             selected dataset
%
% Andreas Bartels, Theodore Alexandrov, University of Bremen
% Version 1.3 (June 2013)
% bartels@math.uni-bremen.de, theodore@math.uni-bremen.de


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function example_testGroup
%% 0. Load example data

data = load('Data/strucTypes');
allData = cat(3,data.rbUnstruct(:,:,5:20),...
                data.rbStruc_type1(:,:,5:15),...
                data.rbStruc_type2(:,:,5:15),...
                data.rbStruc_type3(:,:,5:15),...
                data.rbStruc_type4(:,:,5:15));

clear rbUnstruct rbStruc_type1 rbStruc_type2 rbStruc_type3 rbStruc_type4

numOfImages = size(allData,3);
measures = zeros(numOfImages,1); 


%% 2. Apply testForPresence-approach on each image
%, i.e. apply calcMoC on each image and save its measure
h = waitbar(0,'Calculating the measures...');
for k = 1:numOfImages
    waitbar(k / numOfImages,h,['Calculating the measures... (' num2str(k) '/' num2str(numOfImages) ')'])
    measures(k) = calcMoC(allData(:,:,k),'boundSkipFun',@boundSkip,'verbose',0);
    %waitbar(k / numOfImages)
end
close(h)

%% 3. Sort measures in ascending order and safe indices
[sortedMeasures,indices] = sort(measures,'ascend');

%% 3. Plot the first most structured (i.e. lowest measure of chaos)
verbose = 1; % Plotting (1) or not (0)
if verbose
    togglefig('test-general-group-example')
end

for k = 1:8
    subplot(2,4,k)
    imag = allData(:,:,indices(k));
    % Hotspot removal
    q=quantile(imag(:),0.99);
    imag(imag>q)=q;
    % Plotting
    h=imagesc(imag);
    set(h,'alphadata',~isnan(imag));
    axis image; axis off;
    title(['idx:' num2str(indices(k)) ...
           ', MoC:' num2str(sortedMeasures(k))]); 
end


end