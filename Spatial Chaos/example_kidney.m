%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run this testForPresence - example via
% example_kidney
%
% This example runs on one kidney-slice and show, how the measure of chaos
% (MoC) is being calculated
%
% Andreas Bartels, Theodore Alexandrov, University of Bremen
% Version 1.2 (May 2013)
% bartels@math.uni-bremen.de, theodore@math.uni-bremen.de


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function example_kidney
%% 1. Load example
addpath(genpath(pwd))
data = load('kidneySlice');
img = data.kidney;

%% 2. Apply testForPresence-approach
verbose = 1; % Plotting (1) or not (0)
if verbose
    togglefig('rat-kidney-example')
end
calcMoC(img,'omega',10,'sigma_d',3,'sigma_i',0.3,'quantVec',0.60:0.05:0.95,'boundSkipFun',@cutKidneyMask,'verbose',1);

end