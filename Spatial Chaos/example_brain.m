%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run this testForPresence - example via
% example_brain
%
% This example runs on one of the fifty images in one of the five 
% different structure types, see strucTypes.mat.
% For setting the parameters
%   type : (0-unstructured, 1-curves, 2-regions, 3-gradients, 4-islets)
%   index : 1-50
%
% Andreas Bartels, Theodore Alexandrov, University of Bremen
% Version 1.2 (May 2013)
% bartels@math.uni-bremen.de, theodore@math.uni-bremen.de


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function example_brain
%% 1. Load example
addpath(genpath(pwd))
% Some image type between 0 and 4
% 0 - unstructured, 1 - curves, 2 - regions, 3 - gradients, 4 - islets
type = 2;
% Some index between 1 and 50
index = 35;
% Now load the data
if type == 0
	data = load('strucTypes','rbUnstruct');
    eval(sprintf(['img = data.rbUnstruct(:,:,' num2str(index) ');']));
else
	data = load('strucTypes',['rbStruc_type' num2str(type)]);
    eval(sprintf(['img = data.rbStruc_type' num2str(type) '(:,:,' num2str(index) ');']));
end

%% 2. Apply testForPresence-approach
verbose = 1; % Plotting (1) or not (0)
if verbose
    togglefig('rat-brain-example')
end
tic
calcMoC(img,'omega',10,'sigma_d',3,'sigma_i',0.3,'quantVec',0.60:0.05:0.95,'boundSkipFun',@boundSkip,'verbose',1);
toc
end