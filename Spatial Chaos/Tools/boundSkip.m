
% Function booundSkip applies morphological erosion on an 
% image using a 'disk' with some 'radius'.
%  
% INPUT: 
%   cb : The dataset on which the 'radius' pixels shall be
%		 cutted off the boundary. Note that here 'cb' can be 
%	     a full datacube of size NxMxC where C denoted the number 
%		 of channels. Note further that the dataset should include 
%		 NaNs outside, but not inside the real data in each slice
%		 NxM 
%
%   radius : The radius for morphological erosion.
%	         Default: radius = 3.
%
% OUTPUT:
%   cb_edit : The datacube with cutted boundary
%
%
% Andreas Bartels, Theodore Alexandrov, University of Bremen
% Version 1.0 (August 2012)
% bartels@math.uni-bremen.de


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cb_edit = boundSkip(cb,radius)

if nargin < 2
    radius = 3;
end

[~,~,channels] = size(cb);
cb_edit = cb;

for i=1:channels
    bwimg = cb_edit(:,:,i);
    bwimg(~isnan(bwimg)) = 1;
    bwimg(isnan(bwimg)) = 0;

    % Now apply morphological erosion
    SE = strel('disk', radius);
    IM2 = imerode(bwimg,SE);
 
    cbimg = cb_edit(:,:,i);
    cbimg(IM2==0) = NaN;
    cb_edit(:,:,i) = cbimg;
end

end