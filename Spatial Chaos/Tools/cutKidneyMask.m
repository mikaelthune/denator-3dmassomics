function cutImg = cutKidneyMask(img)

%load cutting mask (created with Matlab-Function roipoly)
data = load('kidneyCutMask');

% Delete NaN-Area around the real data image
cutImg = img(7:end,13:end);

% Delete all NaNs withing the cutting mask that has been loaded above
cutImg((data.kidneyCutMask == 1) & isnan(cutImg)) = 0;

% Now it is time to apply our boundSkip function which is nothing else than
% morphological erosion using a disk of radius 3.
cutImg = boundSkip(cutImg);

end