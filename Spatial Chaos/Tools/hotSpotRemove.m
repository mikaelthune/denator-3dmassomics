function cbHSR = hotSpotRemove(cb,quant)
% Hot spot removal

[~,~,channels] = size(cb);
cbHSR = cb;

for i=1:channels
%     imsplot(cb);
    cbHSRimg = cbHSR(:,:,i);
    q = quantile(cbHSRimg(:),quant);
    cbHSRimg(cbHSRimg > q)  = q;
    cbHSR(:,:,i) = cbHSRimg;
    
%     pause;
% 
%     imsplot(cb);
%     pause;
%     close;
end

end