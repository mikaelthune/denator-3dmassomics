function isLeftBest = compareImPair( Image1, Image2, f, h_l, h_r, varargin )
%UNTITLED3 Summary of this function goes here
%   isFirstBest = compareImPair( Image1, Image2, f, 'color' )

if nargin > 5 && sum(strcmpi(varargin, 'gray')) >= 1
    col = 'gray';
else
    col = 'jet';
end

figure(f);

axes(h_l)
imagesc(histEq(Image1)); axis image; colormap(col);
set(gca,'XTick',[])
set(gca,'YTick',[])

axes(h_r)
imagesc(histEq(Image2)); axis image; colormap(col);
set(gca,'XTick',[])
set(gca,'YTick',[])

waitfor(f,'UserData');
buttonPressed = get(f,'UserData');

if strcmpi(buttonPressed{1},'left')
    isLeftBest = 1;
elseif strcmpi(buttonPressed{1},'right')
    isLeftBest = 0;
end



end
