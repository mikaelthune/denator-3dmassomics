function sortedImages = quicksortIm( ImCube, selectedIm, f, h_l, h_r, varargin)
%UNTITLED2 Summary of this function goes here
%   sortedImages = quicksortIm( ImCube, selectedIm, f, h_l, h_r, 'color')

Nim = length(selectedIm);

if Nim <= 1
    sortedImages = selectedIm;
else
    tmpInd = ceil(rand*Nim);
    pivot = selectedIm(tmpInd);
    selectedIm(tmpInd) = [];
    less = [];
    greater = [];
    
    for k = 1:Nim-1
        if nargin > 3
            isPivotBest = compareImPair( ImCube(:,:,pivot), ImCube(:,:,selectedIm(k)), f, h_l, h_r, varargin{1});
        else
            isPivotBest = compareImPair( ImCube(:,:,pivot), ImCube(:,:,selectedIm(k)), f, h_l, h_r );
        end
        
        if isPivotBest
            less = [less selectedIm(k)];
        else
            greater = [greater selectedIm(k)];
        end
    end
    
    sortedImages = [ quicksortIm(ImCube, greater, f,h_l,h_r,varargin{1})...
                    pivot quicksortIm(ImCube,less, f,h_l,h_r,varargin{1}) ];
    
end

end

