function sortedImages = sortByComparison( ImCube, selectedIm, varargin)
%UNTITLED2 Summary of this function goes here
%   sortedImages = sortByComparison( ImCube, selectedIm, 'color')

f = figure('Name','Sort by comparison - choose the best image',...
    'NumberTitle','off');

h_l = subaxis(1,2,1, 'Spacing',0.01, 'SpacingVert',0.05,...
    'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.2, 'Padding',0);

h_r = subaxis(1,2,2, 'Spacing',0.01, 'SpacingVert',0.05,...
    'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.2, 'Padding',0);

uicontrol('Style','pushbutton', 'String','Left', 'Units','normalized',...
    'Position',[(0.25-0.1) 0.05 0.2 0.1], 'Callback', {@buttonPress_lr,'left'});
uicontrol('Style','pushbutton', 'String','Right', 'Units','normalized',...
    'Position',[(0.75-0.1) 0.05 0.2 0.1], 'Callback', {@buttonPress_lr,'right'});

set(f,'UserData',{'' 0});

if nargin > 2
    sortedImages = quicksortIm(ImCube, selectedIm, f, h_l, h_r, varargin{1});
else
    sortedImages = quicksortIm(ImCube, selectedIm, f, h_l, h_r);
end

close(f)
disp('Finished')

end

