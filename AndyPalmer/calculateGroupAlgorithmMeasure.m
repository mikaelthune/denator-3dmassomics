function ResultCell = calculateGroupAlgorithmMeasure(ResultCell,x_keep_lin)
nFactParams = length(x_keep_lin) -1;

for n = 1: length(ResultCell)
    if length(ResultCell{n}.method) ~= nFactParams
        error(['wrong numberof methods in ' num2str(n)])
    end
    
    nMethods = length(ResultCell{n}.method);
    nMZs = length(ResultCell{n}.method(1).mz);
    temp_values = zeros(nMZs,nMethods);
    
    for m = 1 : nMethods
%         switch ResultCell{n}.method(m).bestValue
%             case 'High'
%                 s = 1;
%             case 'Low'
%                 s = -1;
%             otherwise
%                 error(' ')
%         end
        temp_values(:,m) =  ResultCell{n}.method(m).values;
    end
    
    merged_method = linearAlgorithmCombination(temp_values,x_keep_lin);
    
    ResultCell{n}.method(nMethods+1).name = 'groupAlgorithm';
    ResultCell{n}.method(nMethods+1).window = '';
    ResultCell{n}.method(nMethods+1).bestValue = 'High';
%     ResultCell{n}.method(nMethods+1).bestValue
    ResultCell{n}.method(nMethods+1).mz = ResultCell{n}.method(m).mz;
    ResultCell{n}.method(nMethods+1).values = merged_method;
    [~,ResultCell{n}.method(nMethods+1).ranks] = sort(merged_method);
end
