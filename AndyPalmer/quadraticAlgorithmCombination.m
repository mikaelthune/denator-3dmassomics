function value_out = quadraticAlgorithmCombination(slider_values,algorithm_table,algorithm_paramsIn)


calc_vector = sum(bsxfun(@mtimes,algorithm_table.^2,algorithm_paramsIn(1:6)),2) + ...
    sum(bsxfun(@mtimes,algorithm_table,algorithm_paramsIn(7:12)),2) +  algorithm_paramsIn(13);

value_out = -1*corr(slider_values,calc_vector); %minus so minimised will work :)