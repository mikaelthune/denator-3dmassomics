function [allArray, mzs] = TDM_generateDatacube(headerData)   
% This function loads a whole dataset into memory as a single 3D array
% (x,y,mz)

% get spectrum length
[mzs, ~] = TDM_scilsHdf5GetSpectrum(headerData,1,[],[]);

% make empty array
allArray = nan(headerData.nRows,headerData.nColumns,length(mzs));

% load every spectrum in turn into array
for spec = 1 : length(headerData.spotIdx)
        [~, allArray(headerData.spotIdx(spec,1),headerData.spotIdx(spec,2),:)] = TDM_scilsHdf5GetSpectrum(headerData,spec,[],[]);
end