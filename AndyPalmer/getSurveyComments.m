function [SurveyStructCell, surveyComments] = getSurveyComments(dir_list)
% dir list is cell of folders to parse

%defaults
NInfoStrings = 6;
endString = 'QEval_CommentTime';

% check input
if ~iscell(dir_list)
    dir_list = {dir_list};
end
ii = 1;
for n=1:length(dir_list)
    [SurveyStructCell, FileNames] = importCsvFiles(dir_list{n}, NInfoStrings, endString);
    NSurveys = length(SurveyStructCell);
    
    for k = 1:NSurveys
        beginInd = strfind(FileNames{k}, '100');
        SurveyID = str2double(FileNames{k}(beginInd + 3: beginInd + 4));
        tmpStruct = SurveyStructCell{k}.User;
        % rescale slider values
        sliderValue_plan = adjustSliderSign(tmpStruct.sliderValue_survey, tmpStruct.LRFlipValue);
        repSliderVal_plan = adjustSliderSign(tmpStruct.repPairSliderValue_survey, tmpStruct.repPairLRFlipValue);
        [~, repSliderVal_planScaled] = scaleSliderValue(sliderValue_plan, repSliderVal_plan);
        
        % record values for this user
        surveyComments{ii,1} = tmpStruct.Mail;
        surveyComments{ii,2} = num2str(SurveyID);
        surveyComments{ii,3} = num2str(std(repSliderVal_planScaled));
        surveyComments{ii,4} = tmpStruct.Eval.QualityAsessment;
        surveyComments{ii,5} = tmpStruct.Eval.AdditionalComments;
        ii = ii +1;
    end
end

surveyComments = cellfun(@x_nan,surveyComments,'UniformOutput',false);

function rval = x_nan(x)
if isnan(x) 
    rval = ''; 
else
    rval = x;
end
