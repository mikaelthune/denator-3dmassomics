function x_keep_lin = fixSigns(x_keep_lin,MethodResultsForSurveyIm)
for m = 1:length(MethodResultsForSurveyIm.method)

    switch MethodResultsForSurveyIm.method(m).bestValue
        case 'High'
            s = 1;
        case 'Low'
            s = -1;
    end
    x_keep_lin(m) =     x_keep_lin(m) * s;
    
end
