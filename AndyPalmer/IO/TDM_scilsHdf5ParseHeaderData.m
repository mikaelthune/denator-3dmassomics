% FileType: *.h5
% Description: SCILs Mass Spectrometry Imaging files (*.hdf5)
% getSpectrumFunction:scilsHdf5GetSpectrum

function headerData = TDM_scilsHdf5ParseHeaderData(filename,skiplist)
% This function will parse a h5 file stored in the SCiLS lab hdf5 implementation

% add SCiLS-API
% needs SCiLS API on matlab path

% check file exists
if ~exist(filename,'file')
    error('parseError:fileNotFound',['file ' filename ' was not found'])
end
% open file and make storage object
try
    headerData.storage = Hdf5Storage(filename);
catch e0
    fprintf('was MATLAB started with the scils hdf5 workaround shell?\n')
    rethrow(e0)
end

headerData.rootregion = headerData.storage.getRootRegion;

spots = headerData.rootregion.getSpots;

not_skip_idx = true(length(spots),1);
if nargin == 2
    not_skip_idx(skiplist) = false;
end
spots = spots(not_skip_idx);
% go through each spot and grab the coordinates (need a row and column
% index)
nSpots =  max(size(spots));
spotIdx = zeros(nSpots,3);
spotSize = getSpotSize(headerData.storage,headerData.rootregion);
assignin('base','spots',spots)
% if isfield(headerData.spots{1},'registeredCoordinates')
    coord = 'registeredCoordinates';
%     fprintf('registeredCoordinates')
% else
%     coord = 'unregisteredCoordinates';
% end
headerData.coord = zeros(nSpots,3);
for n = 1 :nSpots
    try
    coord = spots{n}.registeredCoordinates;%(coord); %todo check if these exist if registration hasn't happened
    headerData.coord(n,1) = coord(1);
    headerData.coord(n,2) = coord(2);
    headerData.coord(n,3) = coord(3);
    coord = coord ./ spotSize;
    spotIdx(n,1) = coord(1);
    spotIdx(n,2) = coord(2);
    spotIdx(n,3) = coord(3);
    catch 
        n
    end
end

% start the idx list at 1
fmin = min(spotIdx,[],1);

% headerData.coord = spotIdx;
headerData.spotIdx = round(bsxfun(@minus, spotIdx, fmin)) + 1;

maxVal = max(headerData.spotIdx);

headerData.nRows = maxVal(1);
headerData.nColumns = maxVal(2);
headerData.nStacks = maxVal(3);
headerData.imageAspectRatio = spotSize;
headerData.spots = spots;

headerData.tissueMask = zeros(headerData.nRows,headerData.nColumns);
for n = 1 :nSpots
    headerData.tissueMask(headerData.spotIdx(n,1),headerData.spotIdx(n,2)) = 1;
end


specGrp = headerData.rootregion.getSpectraGroups;
specGrpIdx = 1;

if length(specGrp) > 1
   for n = 1:length(specGrp)
       str{n} = specGrp{n}.name;
   end
    specGrpIdx = find(strcmp(str,'Initial Measurement'));

end

if isempty(specGrpIdx)

        [specGrpIdx, v] = listdlg('PromptString','Select initial measurement set:',...
                'SelectionMode','single',...
                'ListString',str);

end
headerData.spectragroup = scilsmsSpectraGroup( headerData.storage, specGrp{specGrpIdx}.id);

headerData.spectralChannelName = 'm/z';
headerData.getSpectrum = @TDM_scilsHdf5GetSpectrum.m;
headerData.name=filename;
