function [spectralChannels, intensities] = TDM_scilsHdf5GetSpectrum(headerData, x, y, z)
% This function will return a single spectrum from the dataset referenced in headerData.
% Two styles of input are supported
% 1. idx - x == spectrum index, and y == [] & z == [] then the spectrum at the data index x is returned
% 2. coordinates if values for x,y,z are provided then the nearest spectrum to those coordinates are returned. Suggest that these values are chosen from the spotIdx list in headerData to avoid missing spectral coordinates

if isempty(y) && isempty(z) % provide option to just give an index
	%ToDo - link index to data ordering (e.g.row major)
	sIDX = x;
else
	% check if its on the column list
	foo = bsxfun(@minus,headerData.spotIdx,[y x z]);
	[v, sIDX] = min(sum(abs(foo),2));
	if v > 0.1 %some small tolerance (which should not be needed) 
	    spectralChannels = [];
	    intensities = [];
	    return
	end
end
if sIDX > length(headerData.spotIdx) || sIDX < 1 || isnan(sIDX)
    error(['bad sdx ' num2str(sIDX)])
end
spectrum = headerData.spots{sIDX}.getSpectrum(headerData.spectragroup);
samplePositions = spectrum.getSamplePositions;

 spectralChannels = samplePositions.samplePositions;
intensities = spectrum.getIntensities;

% spectralChannels = 1;
% intensities = 1;
