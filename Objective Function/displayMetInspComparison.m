function displayMetInspComparison( CompTable, MethodResults )
%UNTITLED Summary of this function goes here
%   Display a table from 'CompTable' to compare inspection points and how 
%   one of the methods ranked the images

f = figure;
set(f,'Name',['Comparison - Method ''' MethodResults.name '''' ], 'NumberTitle','off')
pos = get(f,'Position');
set(f,'Position',[pos(1) pos(2) 650 200]);

Npoints = size(CompTable,2) - 1;
colWidth = cell(1,Npoints);
colWidth(1:end) = {'auto'};

uitable('Parent',f, 'Data',CompTable(2:end,2:end), 'RowName',CompTable(2:end,1), ...
    'ColumnName',CompTable(1,2:end), 'ColumnWidth',colWidth,...
    'Units','normalized', 'Position',[0 0 1 0.9]);
uicontrol('Parent',f, 'Style','text', 'String',{['Images categorised by points given by human inspection and the method ' MethodResults.name '. Window = ' MethodResults.window]},...
    'Units','normalized', 'Position',[0 0.9 1 0.1]);

end

