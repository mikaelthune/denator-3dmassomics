function Comp = compareRanking( Results, selectedSamp, inspection_points, pointLimits )
%UNTITLED Summary of this function goes here
%   Creates a table showing the fraction of the method values that match
%   the human inspection to the point and  +-1. + display

Results_methods = Results(selectedSamp);
points = pointLimits(1):pointLimits(2);
Npoints = length(points);
Nmethods = size(Results_methods.method,2);
Nmz = length(inspection_points);

Inspect_sets = createSetsFromPoints(inspection_points,pointLimits);

Comp = cell(Nmethods+1, 3*Npoints+3);
for k = 1:Nmethods
    Sets = divideSeries(Results_methods.method(k).ranks(end:-1:1), Inspect_sets);
    totalExtended = 0;
    total = 0;
    for p = 1:Npoints
        if p == 1
            setExtended = [Sets{p}; Sets{p+1}];
        elseif p == Npoints
            setExtended = [Sets{p-1}; Sets{p}];
        else
            setExtended = [Sets{p-1}; Sets{p}; Sets{p+1}];
        end
        
        same = intersect(Inspect_sets{p}, Sets{p});
        Comp{k+1,p*3-1} = length(same)/length(Inspect_sets{p});
        
        sameExtended = intersect(Inspect_sets{p}, setExtended);
        Comp{k+1,p*3} = length(sameExtended)/length(Inspect_sets{p});
        
        totalExtended = totalExtended + length(sameExtended);
        total = total + length(same);
    end
    
    Comp{k+1,1} = Results_methods.method(k).name;
    Comp{k+1,end-1} = total/Nmz;
    Comp{k+1,end} = totalExtended/Nmz;
    
end

for p = 1:Npoints
    Comp{1,p*3-1} = num2str(points(p));
    Comp{1,p*3} = [num2str(points(p)) ' +- 1'];
end
Comp{1,end-1} = 'Total';
Comp{1,end} = 'Total +- 1';


% Display Table
displayMethodHit( Comp, Results, selectedSamp );


end

