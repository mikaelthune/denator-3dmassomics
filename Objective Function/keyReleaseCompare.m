function keyReleaseCompare( src, eventData, Image, mz, Results, mzInd, plotDim, current)
%UNTITLED2 Summary of this function goes here
%   keyRelease( src, eventData, Image, mz, current, eq, varargin)

shownMethods = [1 3 5];

% -------------------- Pressed Right --------------------
if strcmp(eventData.Key, 'rightarrow')
    if current < length(mzInd)
        
        % Normal image
        subaxis(plotDim(1), plotDim(2), 1, 'Spacing',0.01, 'SpacingVert',0.05, ...
            'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
        imagesc( Image(:,:,mzInd(current+1)) )
        axis image
        title({['index: ' num2str( mzInd(current+1) )] ['m/z = ' num2str( mz( mzInd(current+1) ))]});
        set(gca,'XTick',[])
        set(gca,'YTick',[])
        colormap('gray');
        freezeColors
        
        % Equalised image
        subaxis(plotDim(1), plotDim(2), 2, 'Spacing',0.01, 'SpacingVert',0.05, ...
            'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
        imagesc( histEq( Image(:,:,mzInd(current+1)) ) )
        axis image
        title(['Equalised Data']);
        set(gca,'XTick',[])
        set(gca,'YTick',[])
        colormap('gray');
        freezeColors
        
        % Method result
        for m = 1:length(Results.method)-2
            
            subaxis(plotDim(1), plotDim(2), 2+m, 'Spacing',0.01, 'SpacingVert',0.05, ...
                'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
            
            if m == length(Results.method)-2
                h = fspecialIM('gaussian',5,1);
                filtered = conv2(Results.method(shownMethods(m)).ResultImages(:,:,mzInd(current+1)), h, 'same');
                imagesc( histEq( filtered ) )
                title([Results.method(shownMethods(m)).name ' (Filtered) ' Results.method(shownMethods(m)).window]);
            else
                imagesc( histEq( Results.method(shownMethods(m)).ResultImages(:,:,mzInd(current+1)) ) )
                title([Results.method(shownMethods(m)).name ' ' Results.method(shownMethods(m)).window]);
            end
            
            axis image
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            colormap('jet');
            
        end
        set(src,'KeyReleaseFcn',{@keyReleaseCompare,Image,mz,Results,mzInd,plotDim,current+1});
        
    else
        warning('Cannot go further forwards. Already at the last m/z-value')
    end
    
    % -------------------- Pressed Left --------------------
elseif strcmp(eventData.Key, 'leftarrow')
    if current > 1
        
        % Normal image
        subaxis(plotDim(1), plotDim(2), 1, 'Spacing',0.01, 'SpacingVert',0.05, ...
            'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
        imagesc( Image(:,:,mzInd(current-1)) )
        axis image
        title({['index: ' num2str( mzInd(current-1) )] ['m/z = ' num2str( mz( mzInd(current-1) ))]});
        set(gca,'XTick',[])
        set(gca,'YTick',[])
        colormap('gray');
        freezeColors
        
        % Equalised image
        subaxis(plotDim(1), plotDim(2), 2, 'Spacing',0.01, 'SpacingVert',0.05, ...
            'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
        imagesc( histEq( Image(:,:,mzInd(current-1)) ) )
        axis image
        title(['Equalised Data']);
        set(gca,'XTick',[])
        set(gca,'YTick',[])
        colormap('gray');
        freezeColors
        
        % Method result
        for m = 1:length(Results.method)-2
            
            subaxis(plotDim(1), plotDim(2), 2+m, 'Spacing',0.01, 'SpacingVert',0.05, ...
                'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
            
            if m == length(Results.method)-2
                h = fspecialIM('gaussian',5,1);
                filtered = conv2(Results.method(shownMethods(m)).ResultImages(:,:,mzInd(current-1)), h, 'same');
                imagesc( histEq( filtered ) )
                title([Results.method(shownMethods(m)).name ' (Filtered) ' Results.method(shownMethods(m)).window]);
            else
                imagesc( histEq( Results.method(shownMethods(m)).ResultImages(:,:,mzInd(current-1)) ) )
                title([Results.method(shownMethods(m)).name ' ' Results.method(shownMethods(m)).window]);
            end
            
            axis image
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            colormap('jet');
            
        end
        set(src,'KeyReleaseFcn',{@keyReleaseCompare,Image,mz,Results,mzInd,plotDim,current-1});
        
    else
        warning('Cannot go further backwards')
    end
    
end

end

