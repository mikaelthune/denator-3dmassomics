function ResultsFromImcube = evaluateMethodsParallel(ImCube, mzVals, window, varargin)
%   Calculates the image descriptors for all images in ImCube.
%       - mzVals is the mz values for the images in ImCube.
%       - window is the window size that should be used for some of the image
%         descriptors.
%   Usage of variable input can be seen below: (variable input can be
%   combined)
%   ResultsFromImage = evaluateMethodsParallel(ImCube, mzVals, window)
%   ResultsFromImage = evaluateMethodsParallel(ImCube, mzVals, window, params)
%   ResultsFromImage = evaluateMethodsParallel(ImCube, mzVals, window, 'NoDisp')
%   ResultsFromImage = evaluateMethodsParallel(ImCube, imageLabels, window, 'labels')
%
%       - params is paramters for the Spatial Chaos method.
%
%   Output and usage is identical to the function evaluateMethods() but
%   this funciton makes use of the Parallel Processing Toolbox.

%% Check input
if size(ImCube,3) ~= length(mzVals)
    error('Input does not match: Number of images must equal the number of mz values')
end

if size(window,1) ~= 1 || size(window,2) ~= 2
    error('Input ''window'' must be a 1x2 array')
end

% Check that tissue in ImCube has been separated by using NaN:s as background
test = isnan( [ImCube(1,1,1) ImCube(end,1,1) ImCube(1,end,1) ImCube(end,end,1)] );

if sum(test) ~= 4
    disp(' ')
    warning('ImCube appears to be missing NaN separation of background!')
    warndlg({'ImCube appears be missing NaN separation of background!' ...
        'The calculated results for this ImCube will be affected'})
end

% Check if user defined paramters for the Spatial Chaos method should be
% used
tmpInd = cellfun(@isnumeric,varargin);
if nargin > 3 && sum(tmpInd) > 0
    params = varargin{tmpInd == 1};
    SCpar = length(params);
else
    SCpar = 0;
    params = [];
end

%% Initialise
Nmz = size(ImCube,3);
windowString = [num2str(window(1)) 'x' num2str(window(2))];

% STD mean - standard deviation mean
MethodName{1} = 'STDmean';
bestValue{1} = 'High';
win{1} = windowString;

% STD median - standard deviation median
MethodName{2}= 'STDmedian';
bestValue{2} = 'High';
win{2} = windowString;

% SNR mean - signal to noise ratio mean
MethodName{3} = 'SNRmean';
bestValue{3} = 'High';
win{3} = windowString;

% SNR median - signal to noise ratio median
MethodName{4} = 'SNRmedian';
bestValue{4} = 'High';
win{4} = windowString;

% MSE - mean square error
MethodName{5} = 'MSE';
bestValue{5} = 'High';
win{5} = '';

% Spatial Chaos
MethodName{6} = 'SpatialChaos';
bestValue{6} = 'Low';
win{6} = '';

% Mean Intensity
MethodName{7} = 'MeanIntensity';
bestValue{7} = 'High';
win{7} = '';

Nmethods = length(MethodName);

%% Calculate
values1 = zeros(Nmz,1);
values2 = zeros(Nmz,1);
values3 = zeros(Nmz,1);
values4 = zeros(Nmz,1);
values5 = zeros(Nmz,1);
values6 = zeros(Nmz,1);
values7 = zeros(Nmz,1);


parfor mz = 1:Nmz
    values1(mz) = localSTD(ImCube(:,:,mz), 'mean', window);
    values2(mz) = localSTD(ImCube(:,:,mz), 'median', window);
    values3(mz) = localSNR(ImCube(:,:,mz), 'mean', window);
    values4(mz) = localSNR(ImCube(:,:,mz), 'median', window);
    values5(mz) = noiseMSE(ImCube(:,:,mz));
    
    % Decide if default or user defined paramters should be used for
    % Spatial Chaos
    if SCpar == 4
        [values6(mz)] = calcMoC(ImCube(:,:,mz), 'omega',params(1), 'sigma_d',params(2), 'sigma_i',params(3), 'quantVec',params(4)*0.05:0.05:0.95 );
    elseif SCpar == 3
        [values6(mz)] = calcMoC(ImCube(:,:,mz), 'omega',params(1), 'sigma_d',params(2), 'sigma_i',params(3));
    else
        [values6(mz)] = calcMoC(ImCube(:,:,mz));
    end
    
    values7(mz) = imageMIC(ImCube(:,:,mz));

end
values = [values1 values2 values3 values4 values5 values6 values7];

%% Create struct with results and information
for m = 1:Nmethods
    
    % Create array with rankings. ranks(1) will hold the index of the best
    % image and ranks(end) will hold the index of the worst image,
    % according to the respective methods.
    if strcmpi(bestValue{m}, 'High')
        [~, ranks] = sort(values(:,m),'descend');
    elseif strcmpi(bestValue{m}, 'Low')
        [~, ranks] = sort(values(:,m),'ascend');
    end
    
    ResultsFromImcube.method(m).name = MethodName{m};
    ResultsFromImcube.method(m).window = win{m};
    ResultsFromImcube.method(m).bestValue = bestValue{m};
    
    % If specified in input, use user defined labels instead of mz-values
    if nargin > 3 && sum( strcmpi(varargin, 'labels') ) > 0
        ResultsFromImcube.method(m).labels = mzVals;
    else
        ResultsFromImcube.method(m).mz = mzVals;
    end
    
    ResultsFromImcube.method(m).values = values(:,m);
    ResultsFromImcube.method(m).ranks = ranks;
    
end


end
