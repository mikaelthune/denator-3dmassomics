function CompTable = comparePoints( MethodResults, inspection_points, pointLimits, varargin )
%UNTITLED2 Summary of this function goes here
%   Create a table to compare inspection points and how one of the methods ranked the images + display

points = pointLimits(1):pointLimits(end);
Npoints = length(points);

Inspect_sets = createSetsFromPoints(inspection_points, pointLimits);
Method_sets = divideSeries(MethodResults.ranks(end:-1:1), Inspect_sets);

CompTable = cell(1+Npoints);

for mp = 1:Npoints
    for ip = 1:Npoints
        same = length( intersect( Inspect_sets{ip}, Method_sets{mp} ) );
        CompTable{mp+1,ip+1} = same;
        
    end
    CompTable{mp+1,1} = ['Method: ' num2str(points(mp)) 'p'];
    CompTable{1,mp+1} = ['Inspection: ' num2str(points(mp)) 'p'];
end

if nargin == 4 && strcmp(varargin{1}, 'nodisp')
else
    displayMetInspComparison(CompTable, MethodResults);
end

end

