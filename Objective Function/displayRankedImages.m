function displayRankedImages( ResultsFromImage, ranksToDisplay, Images, selectedMethod, varargin)
%   Displays the images that the methods rank as top and bottom

if ~iscell(Images)
    Images = {Images};
end

Nmethods = size(ResultsFromImage(1).method,2);


topMZ = ResultsFromImage.method(selectedMethod).ranks(1:ranksToDisplay(1));
bottomMZ = ResultsFromImage.method(selectedMethod).ranks(end-ranksToDisplay(2)+1:end);

displayImages(Images, [topMZ' bottomMZ'], varargin{:});


set(gcf,'Name',['Method ''' ResultsFromImage.method(selectedMethod).name...
        '''' ', ' ResultsFromImage.method(selectedMethod).window],'NumberTitle','off')


end
