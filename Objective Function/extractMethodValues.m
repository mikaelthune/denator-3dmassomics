function values = extractMethodValues( Results, methodString )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Nsample = length(Results);
methodInd = find(strcmpi(methodString,{Results(1).method.name}));

if isempty(methodInd)
    error(['No method with name ''' methodString ''' could be found'])
end

values = [];
for k = 1:Nsample
    values = [values; Results(k).method(methodInd).values];
end

end

