function varargout = compareOrderedList( MethodResults, humanList )
%UNTITLED Summary of this function goes here
%   [errorLists, methodList] = compareOrderedList( MethodResults, humanList )

Nmethods = length(MethodResults.method);
Nind = length(humanList);

errorLists = cell(Nmethods,1);
errorLists(1:end) = {nan(Nind,1)};
methodList = cell(Nmethods,1);

for m = 1:Nmethods
%     [~,tmpInd,~] = intersect(MethodResults.method(m).ranks,humanList);
%     methodList = MethodResults.method(m).ranks(tmpInd);
%     methodValues = MethodResults.method(m).values(methodList);
    methodValues = MethodResults.method(m).values(humanList);
    if strcmpi(MethodResults.method(m).bestValue, 'high')
        [~,tmpInd] = sort( methodValues,'descend' );
    elseif strcmpi(MethodResults.method(m).bestValue, 'low')
        [~,tmpInd] = sort( methodValues,'ascend' );
    else
        error('MethodResult struct has bad value')
    end
%     methodList = methodList(tmpInd);
    methodList{m} = humanList(tmpInd);
    
    % Negative values means the method ranks that image lower than the
    % human.
    for k = 1:Nind
        tmpInd = find( methodList{m} == humanList(k));
        errorLists{m}(k) = k - tmpInd;
    end
end

varargout{1} = errorLists;

if nargout == 2
    varargout{2} = methodList;
end
    

end

