function compareImFromMethods( Images, mz, Results, selectIm, mzInd )
%UNTITLED Summary of this function goes here
%   Slideshow with where each slide shows an m/z-image and the Results
%   images from the mehtods

if ~iscell(Images)
    Images = {Images};
end

Nim = length(mzInd);
Nmethods = length(Results(selectIm).method)-2;
shownMethods = [1 3 5];
plotDim = calcSubplotDim(Nmethods+2);

f=figure('KeyReleaseFcn',{@keyReleaseCompare,Images{selectIm},mz,Results(selectIm),mzInd,plotDim,1},...
    'Name',['Slideshow - Compare Methods, ' Results(selectIm).method(1).window],'NumberTitle','off');

% hManager = uigetmodemanager(f);
% set(hManager.WindowListenerHandles,'Enable','off');

% Normal image
subaxis(plotDim(1), plotDim(2), 1, 'Spacing',0.01, 'SpacingVert',0.05, ...
                'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
imagesc( Images{selectIm}(:,:,mzInd(1)) )
axis image
title({['index: ' num2str( mzInd(1) )] ['m/z = ' num2str( mz( mzInd(1) ))]});
set(gca,'XTick',[])
set(gca,'YTick',[])
colormap('gray');
freezeColors

% Equalised image
subaxis(plotDim(1), plotDim(2), 2, 'Spacing',0.01, 'SpacingVert',0.05, ...
                'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
imagesc( histEq( Images{selectIm}(:,:,mzInd(1)) ) )
axis image
title(['Equalised Data']);
set(gca,'XTick',[])
set(gca,'YTick',[])
colormap('gray');
freezeColors

% Method result
for m = 1:Nmethods
    
    subaxis(plotDim(1), plotDim(2), 2+m, 'Spacing',0.01, 'SpacingVert',0.05, ...
            'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
    if m == Nmethods
        h = fspecialIM('gaussian',5,1);
        filtered = conv2(Results(selectIm).method(shownMethods(m)).ResultImages(:,:,mzInd(1)), h, 'same');
        imagesc( histEq( filtered ) )
        title([Results(selectIm).method(shownMethods(m)).name ' (Filtered)' Results(selectIm).method(shownMethods(m)).window]);
    else
        imagesc( histEq( Results(selectIm).method(shownMethods(m)).ResultImages(:,:,mzInd(1)) ) )
        title([Results(selectIm).method(shownMethods(m)).name ' ' Results(selectIm).method(shownMethods(m)).window]);
    end
    axis image
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    colormap('jet');

end

end

