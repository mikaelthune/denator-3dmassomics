function displayMethodHit( Comp, Results_methods, selectedSamp )
%UNTITLED Summary of this function goes here
%   Displays a table showing the fraction of the method values that match
%   the human inspection to the point and  +-1.

f = figure;
set(f,'Name',['Comparison - Sample ' num2str(selectedSamp)], 'NumberTitle','off')
pos = get(f,'Position');
set(f,'Position',[pos(1) pos(2) 900 200]);

Npoints = size(Comp,2)/3 - 1;
colWidth = cell(1,3*Npoints+2);
colWidth(1:end) = {'auto'};
colWidth([3:3:end-2]) = {12};

uitable('Parent',f, 'Data',Comp(2:end,2:end), 'RowName',Comp(2:end,1), ...
    'ColumnName',Comp(1,2:end), 'ColumnWidth',colWidth, ...
    'Units','normalized', 'Position',[0 0 1 0.9]);
uicontrol('Parent',f, 'Style','text', 'String',{['Fraction of m/z-values that were ranked same as human inspection - Sample ' num2str(selectedSamp) ', window = ' Results_methods(selectedSamp).method(1).window]},...
    'Units','normalized', 'Position',[0 0.9 1 0.1]);

end

