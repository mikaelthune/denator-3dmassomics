function keyReleaseLowerI( src, eventData, Image, window, maxInt, intensityStep, shownMethods, plotDim, kernel, current )
%UNTITLED2 Summary of this function goes here
%   keyRelease( src, eventData, Image, mz, current, eq, varargin)

% -------------------- Pressed Right --------------------
if strcmp(eventData.Key, 'rightarrow')
    tmp = Image - (current+1)*intensityStep;
    newImage = tmp .* (tmp >= 0);
    
    if max(newImage(:)) > 0
        Results = evaluateMethods({newImage},window,'nodisp');
        displayLowerIntensity(newImage, Results, maxInt, shownMethods, plotDim, kernel, current+1, intensityStep);
        
        set(src,'KeyReleaseFcn',{@keyReleaseLowerI,Image,window,maxInt,intensityStep,shownMethods,plotDim,kernel,current+1});
        
    else
        warning('Cannot lower intensity any further.')
    end
    
    % -------------------- Pressed Left --------------------
elseif strcmp(eventData.Key, 'leftarrow')
    if current > 0
        tmp = Image - (current-1)*intensityStep;
        newImage = tmp .* (tmp >= 0);
        Results = evaluateMethods({newImage},window,'nodisp');
        
        displayLowerIntensity(newImage, Results, maxInt, shownMethods, plotDim, kernel, current-1, intensityStep);
        
        set(src,'KeyReleaseFcn',{@keyReleaseLowerI,Image,window,maxInt,intensityStep,shownMethods,plotDim,kernel,current-1});
        
    else
        warning('Cannot raise intensity any further than original Image')
    end
    
end

end

