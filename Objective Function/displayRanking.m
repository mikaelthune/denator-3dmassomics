function displayRanking( ResultsFromImage, selectedImages, ranksToDisplay )
%UNTITLED Summary of this function goes here
% Display a table showing the top and bottom ranked indices according to the
% different methods


if isempty(selectedImages)
    selected = 1:size(ResultsFromImage,2);
else
    selected = selectedImages;
end

if isempty(ranksToDisplay)
    ranks = [5 5];
else
    ranks = ranksToDisplay;
end

colWidth = cell(1,sum(ranks)+1);
colWidth([1:ranks(1) ranks(1)+2:end]) = {'auto'};
colWidth(ranks(1)+1) = {8};
Nmethods = size(ResultsFromImage(1).method,2);

for im = selected
    f = figure;
    set(f,'Name',['Results - Image ' num2str(im)], 'NumberTitle','off')
    pos = get(f,'Position');
    set(f,'Position',[pos(1) pos(2) 900 200]);
    
    data = zeros(Nmethods,sum(ranks));    
    for k = 1:Nmethods
        data(k,:) = [ResultsFromImage(im).method(k).ranks(1:ranks(1))' ...
            ResultsFromImage(im).method(k).ranks(end-ranks(2)+1:end)'];
        rowNames{k} = ResultsFromImage(im).method(k).name;
        
    end
    
    Nmz = length(ResultsFromImage(im).method(1).ranks);
    
    uitable('Parent',f, 'Data',[data(:,1:ranks(1)) zeros(Nmethods,1) data(:,ranks(1)+1:end)],...
            'RowName',rowNames, 'ColumnName',[1:ranks(1) 0 Nmz-ranks(2)+1:Nmz], ...
            'ColumnWidth',colWidth, 'Units','normalized', 'Position',[0 0 1 0.9]);
    uicontrol('Parent',f, 'Style','text', 'String',{['Top & Bottom rankings of m/z-indices - Image ' num2str(im) ', window = ' ResultsFromImage(1).method(1).window]},...
                'Units','normalized', 'Position',[0 0.9 1 0.1]);
     
end

end



