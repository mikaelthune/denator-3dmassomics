function displayErrorLists( MethodResults, errorLists, varargin )
%UNTITLED Summary of this function goes here
%   Histogram of how the methods performed compared to human evaluation

Nset = length(errorLists{1});
Nmethods = length(MethodResults.method);
if Nmethods ~= length(errorLists)
    error('Mismatching arguments')
end

plotDim = calcSubplotDim(Nmethods);
figure('Name',['Distribution of ranking misses by method'], 'NumberTitle','off');

for m = 1:Nmethods
    subaxis(plotDim(1), plotDim(2), m, 'Spacing',0.07, 'SpacingVert',0.15,...
                'MR',0.02, 'ML',0.06, 'MT',0.05, 'MB',0.1, 'Padding',0);
    hist(errorLists{m})
    xlim([-Nset Nset ]);
    if nargin > 2 && isnumeric(varargin{1})
        ylim([ 0 varargin{1} ])
    end
    xlabel('Number of ranks the method missed')
    ylabel('Count')
    title([MethodResults.method(m).name ' ' MethodResults.method(m).window])
    
end

end