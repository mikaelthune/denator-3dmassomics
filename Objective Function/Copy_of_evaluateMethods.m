function ResultsFromImage = evaluateMethods(ImCube, mzVals, window, varargin)
%EVALUATEMETHODS
%   ResultsFromImage = evaluateMethods(ImCube, mzVals, window, params)
%

%% Initialise
Nmz = size(ImCube,3);
windowString = [num2str(window(1)) 'x' num2str(window(2))];

% STD mean - standard deviation mean
MethodName{1} = 'STDmean';
bestValue{1} = 'High';
win{1} = windowString;

% STD median - standard deviation median
MethodName{2}= 'STDmedian';
bestValue{2} = 'High';
win{2} = windowString;

% SNR mean - signal to noise ratio mean
MethodName{3} = 'SNRmean';
bestValue{3} = 'High';
win{3} = windowString;

% SNR median - signal to noise ratio median
MethodName{4} = 'SNRmedian';
bestValue{4} = 'High';
win{4} = windowString;

% MSE - mean square error
MethodName{5} = 'MSE';
bestValue{5} = 'High';
win{5} = '';

% Spatial Chaos
MethodName{6} = 'SpatialChaos';
bestValue{6} = 'Low';
win{6} = '';

Nmethods = length(MethodName);

%% Calculate

% Nrows = size(ImCube,1);
% Ncols = size(ImCube,2);
% if nargin == 3 || ~( sum(strcmpi('noim', varargin)) )
%     Iresults = cell(Nmethods,1);
%     Iresults(1:4) = {NaN( Nrows - window(1) + 1, Ncols - window(2) + 1, Nmz)};
%     Iresults{5} = NaN(Nrows - 6, Ncols - 6, Nmz);
% end

values = zeros(Nmz,Nmethods);
for mz = 1:Nmz
%     if nargin > 3 && sum(strcmpi('noim',varargin))
        [values(mz,1)] = localSTD(ImCube(:,:,mz), 'mean', window);
        [values(mz,2)] = localSTD(ImCube(:,:,mz), 'median', window);
        [values(mz,3)] = localSNR(ImCube(:,:,mz), 'mean', window);
        [values(mz,4)] = localSNR(ImCube(:,:,mz), 'median', window);
        [values(mz,5)] = noiseMSE(ImCube(:,:,mz));
%     else
%         [values(mz,1), Iresults{1}(:,:,mz)] = localSTD(ImCube(:,:,mz), 'mean', window);
%         [values(mz,2), Iresults{2}(:,:,mz)] = localSTD(ImCube(:,:,mz), 'median', window);
%         [values(mz,3), Iresults{3}(:,:,mz)] = localSNR(ImCube(:,:,mz), 'mean', window);
%         [values(mz,4), Iresults{4}(:,:,mz)] = localSNR(ImCube(:,:,mz), 'median', window);
%         [values(mz,5), Iresults{5}(:,:,mz)] = noiseMSE(ImCube(:,:,mz));
%     end
    
    tmpInd = cellfun(@isnumeric,varargin);
    if nargin > 3 && sum(tmpInd) > 0
        params = varargin{tmpInd == 1};
        [values(mz,6)] = calcMoC(ImCube(:,:,mz), 'omega',params(1), 'sigma_d',params(2), 'sigma_i',params(3) );
    else
        [values(mz,6)] = calcMoC(ImCube(:,:,mz));
    end

    if nargin == 3 || ~( sum(strcmpi('nodisp', varargin)) )
        if mod(mz,round(Nmz/10)) == 0
            disp(['Progress: ' num2str(round(100*mz/Nmz)) '%'])
        elseif mz == Nmz
            disp('Finished ')
        end
    end
end

for m = 1:Nmethods
    if strcmpi(bestValue{m}, 'High')
        [~, ranks] = sort(values(:,m),'descend');
    elseif strcmpi(bestValue{m}, 'Low')
        [~, ranks] = sort(values(:,m),'ascend');
    end
    ResultsFromImage.method(m).name = MethodName{m};
    ResultsFromImage.method(m).window = win{m};
    ResultsFromImage.method(m).bestValue = bestValue{m};
    ResultsFromImage.method(m).mz = mzVals;
    ResultsFromImage.method(m).values = values(:,m);
    ResultsFromImage.method(m).ranks = ranks;
%     if nargin == 3 || ~( sum(strcmpi('noim', varargin)) )
%         ResultsFromImage.method(m).ResultImages = Iresults{m};
%     end
end


end

