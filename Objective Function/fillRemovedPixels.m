function Ifilled = fillRemovedPixels( I, varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation

if ~iscell(I)
    I = {I};
end
Nim = length(I);

Ifilled = I;
for im = 1:Nim
    Nrows = size(I{im},1);
    Ncols = size(I{im},2);
    
    indNaN = find(isnan(I{im}(:,:,1)));
    [r,c] = ind2sub([Nrows Ncols], indNaN);
    r( or(c==1, c==Ncols) ) = [];
    c( or(c==1, c==Ncols) ) = [];
    c( or(r==1, r==Nrows) ) = [];
    r( or(r==1, r==Nrows) ) = [];
    
    for k = 1:length(r)
        Area = I{im}(r(k)-1:r(k)+1, c(k)-1:c(k)+1, 1);
        
        if sum(isnan(Area(:))) == 1
            if nargin > 1 && strcmpi(varargin{1},'zero')
                Ifilled{im}(r(k),c(k),:) = 0;
            else
                tmp = I{im}(r(k)-1:r(k)+1, c(k)-1:c(k)+1, :);
                straight = squeeze( tmp(1,2,:)+tmp(2,1,:)+tmp(2,3,:)+tmp(3,1,:) );
                diag = squeeze( (tmp(1,1,:)+tmp(1,3,:)+tmp(3,1,:)+tmp(3,3,:))./sqrt(2) );
                
                newSpec = mean([straight./4 diag./4],2);
                Ifilled{im}(r(k),c(k),:) = newSpec;
            end
        end
        
    end
    
end


if Nim == 1
    Ifilled = Ifilled{1};
end

end

