function IntensityResults = intensityEffect( Images, window, varargin )
%UNTITLED Summary of this function goes here
%   Calculate how the method values are affected by lowering the intensity
%   in the image.
%   IntensityResults = intensityEffect( Images, window, 'nodisp' )

Nim = size(Images,3);
Npoints = 200;
percent = linspace(0,100,Npoints);

IntensityResults = cell(7,2,Nim);
IntensityResults(:,2,:) = {zeros(Npoints,1)};

for im = 1:Nim
    maxInt = max(max(Images(:,:,im)));
    step = maxInt/Npoints;

    for k = 1:Npoints
        newIm = Images(:,:,im) - (k-1)*step;
        newIm = newIm .* (newIm >= 0);
        Result = evaluateMethods({newIm}, window, 'nodisp','noim');
        % Holes
        IntensityResults{6,2,im}(k) = datasetHoles(newIm);
        % Mean intensity
        IntensityResults{7,2,im}(k) = mean( newIm(~isnan(newIm)) );
        
        for m = 1:length(Result.method)
            IntensityResults{m,2,im}(k) = Result.method(m).values;
        end
        if mod(k,Npoints/2) == 0
            disp(['Image ' num2str(im) ': ' num2str(round(100*k/Npoints)) '%'])
        elseif k == Npoints
            disp(['Image ' num2str(im) ': ' '100%'])
        end
        
    end
    
end

for m = 1:length(Result.method)
    IntensityResults(m,1,:) = {[Result.method(m).name ' ' Result.method(m).window]};
end
IntensityResults(6,1,:) = {'# of holes'};
IntensityResults(7,1,:) = {'Mean intensity'};

if nargin == 2 || ~strcmpi('nodisp',varargin{1})
    displayIntensityEffect( IntensityResults )
end

end

