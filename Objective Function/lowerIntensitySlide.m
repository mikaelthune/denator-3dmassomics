function lowerIntensitySlide( Image, window )
%UNTITLED Summary of this function goes here
%   slideshow that lowers the intensity of an image for each slide

shownMethods = [1 3 5];
NmethodIm = length(shownMethods);
plotDim = calcSubplotDim(NmethodIm+3+2);
kernel = fspecialIM('gaussian',5,1);
ind = find( and(~isnan(Image), Image ~= 0) );
intensityStep = 2000*median(Image(ind)) / (numel(Image)/2);
maxInt = max(Image(:));

f = figure('KeyReleaseFcn',{@keyReleaseLowerI,Image,window,maxInt,intensityStep,shownMethods,plotDim,kernel,0},...
    'Name',['Slideshow - Lower Intensity, ' num2str(window(1)) 'x' num2str(window(2))],'NumberTitle','off');

Results = evaluateMethods({Image},window,'nodisp');
displayLowerIntensity( Image, Results, maxInt, shownMethods, plotDim, kernel, 0, intensityStep );

end

