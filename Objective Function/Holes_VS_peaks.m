
I = ImagesPancreas{1};
Nmz = size(I,3);

% m1=mean(I,1);
% m2=mean(m1,2);
% meanSpec = squeeze(m2);
% [sortedSpec,ind] =  sort(meanSpec);

holes = datasetHoles(I);
[sortedHoles, ind] = sort(holes);

pixelSpecs = zeros( numel(I(:,:,1)), Nmz );
count = 1;
for r = 1:size(I,1)
    for c = 1:size(I,2)
        pixelSpecs(count,:) = I(r,c,:);
        count = count + 1;
    end
end

figure
hold on
for m = 1:Nmz
    plot(sortedHoles, pixelSpecs(:,ind(m)),'+')
end