function Sets = createSetsFromPoints( ImPoints, pointLimits )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

points = pointLimits(1):pointLimits(end);
Npoints = length(points);

Sets = cell(Npoints,1);
for p = 1:Npoints
    ind = find(ImPoints == points(p));
    Sets{p,1} = ind; 
end

end

