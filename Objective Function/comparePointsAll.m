function CompTables = comparePointsAll( MethodResults, inspection_points, pointLimits, varargin )
%UNTITLED Summary of this function goes here
%   Create a table for each method
Nmethods = length(MethodResults.method);
CompTables = cell(Nmethods,2);
for k = 1:Nmethods
    if nargin == 4
        CompTables{k,2} = comparePoints(MethodResults.method(k), inspection_points, pointLimits, varargin);
    else
        CompTables{k,2} = comparePoints(MethodResults.method(k), inspection_points, pointLimits);
    end
    
    CompTables{k,1} = MethodResults.method(k).name;
    
end

