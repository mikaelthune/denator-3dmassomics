function displayIntensityEffect( IntensityResults, varargin )
%UNTITLED2 Summary of this function goes here
%   displayIntensityEffect( IntensityResults, 'norm' )

Nim = size(IntensityResults,3);
plotDim = calcSubplotDim(size(IntensityResults,1)-2);

if nargin == 2 && strcmpi(varargin{1},'norm')
    figure
    set(gcf, 'Name',['Lowered Intensity'], 'NumberTitle','off');
    meanHoles = mean([IntensityResults{6,2,:}],2);
    meanHoles = meanHoles ./ max(meanHoles);
    meanInt = mean([IntensityResults{7,2,:}],2);
    meanInt = meanInt ./ max(meanInt);
    
    for m = 1 : (size(IntensityResults,1)-2)
        percInt = [];
        for im = 1:Nim
            percInt = [percInt IntensityResults{m,2,im}./max(IntensityResults{m,2,im})];
        end
        
        x = repmat(linspace(0,100,length(IntensityResults{1,2,1}))',1,Nim);
        
        subaxis(plotDim(1), plotDim(2), m, 'Spacing',0.05, 'SpacingVert',0.1,...
                'MR',0.02, 'ML',0.04, 'MT',0.05, 'MB',0.05, 'Padding',0);
        plot(x, percInt,'b')
        hold on
        plot( x(:,1), mean(percInt,2), 'g','LineWidth',2 )
        plot( x(:,1), meanHoles, '--k', 'LineWidth',2 )
        plot( x(:,1), meanInt, '--r', 'LineWidth',2 )
        xlabel('Amount of maximum intensity reduced [%]')
        ylabel('Amount of maximum value')
        title(IntensityResults{m,1})
    end
    
else
    warning('This part of code not finished')
    figure
    set(gcf, 'Name',['Lowered Intensity'], 'NumberTitle','off');
    for m = 1 : (size(IntensityResults,1)-1)
        y = [];
        for im = 1:Nim
            y = [y IntensityResults{m,2,im}];
        end
        x = repmat(linspace(0,100,length(IntensityResults{1,2,1}))',1,Nim);
        
        subaxis(plotDim(1), plotDim(2), m, 'Spacing',0.05, 'SpacingVert',0.1,...
                'MR',0.02, 'ML',0.04, 'MT',0.05, 'MB',0.05, 'Padding',0);
        plot(x,y)
        xlabel('Amount of intensity reduced [%]')
        ylabel('Method Value')
        title(IntensityResults{m,1})
    end
end

end

