function Icut = cutBackground( ImCube )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Isum = sum(ImCube,3);

[r,c] = find(Isum ~= 0);

xMax = max(c);
xMin = min(c);
yMax = max(r);
yMin = min(r);

Icut = ImCube(yMin:yMax, xMin:xMax, :);

end

