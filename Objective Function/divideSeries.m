function Sets = divideSeries( series, divInfo )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% Divide into 'divInfo' as equaly large sets as possible
if length(divInfo) == 1
    
    setLength = round(length(series)/divInfo);
    
    Sets = cell(divInfo,1);
    for k = 1:divInfo
        if k == divInfo
            Sets{k,1} = series( (k*setLength-setLength+1) : end );
        else
            Sets{k,1} = series( (k*setLength-setLength+1) : (k*setLength) );
        end
        
    end
    
    % Divide into sets with same sizes as 'divInfo'
else
    Nsets = length(divInfo);
    Sets = cell(Nsets,1);

    count = 1;
    for k = 1:Nsets
        l = length(divInfo{k});
        Sets{k,1} = series( count : count + l - 1 );
        count = count + l;
    end
    
end

end

