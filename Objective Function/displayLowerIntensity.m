function displayLowerIntensity( Image, Results, maxInt, shownMethods, plotDim, kernel, current, intensityStep )
%UNTITLED Summary of this function goes here
%   display a slide in the slideshow LowerIntensity

NmethodIm = length(shownMethods);

% Normal image
subaxis(plotDim(1), plotDim(2), 1, 'Spacing',0.01, 'SpacingVert',0.05, ...
                'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
imagesc( Image )
axis image
title('Original');
set(gca,'XTick',[])
set(gca,'YTick',[])
colormap('gray');
freezeColors

% Equalised image
subaxis(plotDim(1), plotDim(2), 2, 'Spacing',0.01, 'SpacingVert',0.05, ...
                'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
imagesc( histEq( Image ) )
axis image
title('Equalised');
set(gca,'XTick',[])
set(gca,'YTick',[])
freezeColors

% Show zero
subaxis(plotDim(1), plotDim(2), 3, 'Spacing',0.01, 'SpacingVert',0.05, ...
                'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
imagesc( double(Image == 0));
axis image
title('Zero valued pixels');
set(gca,'XTick',[])
set(gca,'YTick',[])
freezeColors

% Methods
for m = 1:NmethodIm
    subaxis(plotDim(1), plotDim(2), 4+m, 'Spacing',0.01, 'SpacingVert',0.05, ...
            'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
        
    if m == NmethodIm
        filtered = conv2( Results.method(shownMethods(m)).ResultImages, kernel, 'same');
        imagesc( histEq( filtered ) )
        title([Results.method(shownMethods(m)).name ' (Filtered)' Results.method(shownMethods(m)).window]);
    else
        imagesc( histEq( Results.method(shownMethods(m)).ResultImages ))
        title([Results.method(shownMethods(m)).name ' ' Results.method(shownMethods(m)).window]);
    end
    
    axis image
    set(gca,'XTick',[])
    set(gca,'YTick',[])

end
colormap('jet')

% Display Method values
Nmet = length(Results.method);
l = [];
for k = 1:Nmet
    l = [l length(Results.method(k).name)];
end
maxStr = max(l);

for k = 1:Nmet
    vals{k} = [ Results.method(k).name ':   ' repmat(' ', 1, maxStr-l(k)) num2str(Results.method(k).values) ];
end
vals{length(Results.method)+1} = '';
vals{length(Results.method)+2} = ['Intensity = -' num2str(current) ' * ' num2str(intensityStep)];
vals{length(Results.method)+3} = ['Reduced by: ' num2str( 100*current*intensityStep/maxInt ) ' %' ];

a = subaxis(1, plotDim(2), plotDim(2), 'Spacing',0.01, 'SpacingVert',0.05, ...
    'MR',0.01, 'ML',0.01, 'MT',0.08, 'MB',0.01, 'Padding',0);
pos = get(a,'position');
uicontrol('Parent',gcf, 'Style','text', 'String',vals, 'HorizontalAlignment','Left',...
    'Units','normalized', 'Position',pos);

set(gca,'XTick',[])
set(gca,'YTick',[])

end

