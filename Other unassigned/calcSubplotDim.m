function dimensions = calcSubplotDim( Nplots, varargin )
%UNTITLED Summary of this function goes here
%   Calculate optimal subplot dimensions
%   calcSubplotDim( Nplots, [ImRows ImCols] )

% screen ratio
horiz = 16;
vert = 9;
% horiz = 8;
% vert = 5;


if nargin == 2
    displayRatio = 1.4*(horiz/varargin{1}(2)) / (vert/varargin{1}(1));
else
    displayRatio = horiz / vert;
end

maxDim = 20;
maxPlots = maxDim*round(maxDim/displayRatio);

if Nplots > maxPlots
    error('Too many plots to display')
end

combinations = combnk(1:maxDim,2);
ind = (prod(combinations,2) <= maxPlots) & (prod(combinations,2) >= Nplots);
goodComb = combinations(ind,:);

div = goodComb(:,2)./goodComb(:,1);
area = zeros(length(div),1);
for k = 1:length(div);
    if div(k) > displayRatio
        area(k) = Nplots*horiz^2/(goodComb(k,2)^2);
    else
        area(k) = Nplots*vert^2/(goodComb(k,1)^2);
    end
end
[~,ind] = max(area);

dimensions = goodComb(ind,:);

end

