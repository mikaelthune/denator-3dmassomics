function displayImages( Images, varargin )
%UNTITLED2 Summary of this function goes here
%   displayImages( Images, mzValuesToShow, 'eq','gray' )
% displays the images in 'Images'

if ~iscell(Images)
    Images = {Images};
end

if nargin == 1
    Nmz = size(Images{1},3);
    for im = 1:size(Images,1)
        subplotDim = calcSubplotDim(Nmz, size(Images{1}));
        figure('Name',['Image ' num2str(im)],'NumberTitle','off');
        
        for k = 1:Nmz
            subaxis(subplotDim(1),subplotDim(2),k,'Spacing',0.01,...
                    'SpacingVert',0.05,'MR',0.01,'ML',0.01,'MT',0.05,'MB',0.01,...
                    'Padding',0);
            h1 = imagesc( Images{im}(:,:,k) ); 
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            axis image
            set(gca,'xcolor','w', 'ycolor','w', 'box','off', 'visible','off')
            set(h1, 'alphadata', ~isnan(Images{im}(:,:,k)));
            title(['ind: ' num2str(k)])
        end
        
    end
    
elseif nargin > 1
    ind = find( cellfun(@isnumeric,varargin) );
    if isempty(ind)
        mzToShow = 1:size(Images{1},3);
    else
        mzToShow = varargin{ind};
    end
    
    subplotDim = calcSubplotDim(length(mzToShow), size(Images{1}));
    for im = 1:size(Images,1)
        f = figure;
        
        if sum(strcmpi(varargin, 'eq')) >= 1
            set(f,'Name',['Image ' num2str(im) '- Equalised'],'NumberTitle','off')
        else
            set(f,'Name',['Image ' num2str(im)],'NumberTitle','off')
        end
        
        tmp = 0;
        for m = 1:length(mzToShow)
            tmp = tmp + 1;
            subaxis(subplotDim(1),subplotDim(2),tmp,'Spacing',0.01,...
                    'SpacingVert',0.05,'MR',0.01,'ML',0.01,'MT',0.05,'MB',0.01,...
                    'Padding',0);
                
            if sum( strcmpi(varargin,'eq') ) >= 1
                imagesc(histEq(Images{im}(:,:,mzToShow(m))));
                if mod(m,round(length(mzToShow)/4)) == 0
                    disp([ num2str(round(100*m/length(mzToShow))) ' %'])
                elseif m == length(mzToShow)
                    disp('100 %')
                end
            else
                imagesc(Images{im}(:,:,mzToShow(m))); 
            end
            
            if sum( strcmpi(varargin,'gray') ) >= 1
                colormap('gray')
            end
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            axis image
            title(['ind: ' num2str(mzToShow(m))])
            
        end
    end
end

end

