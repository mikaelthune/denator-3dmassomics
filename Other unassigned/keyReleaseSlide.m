function keyReleaseSlide( src, eventData, Image, mz, current, args)
%UNTITLED2 Summary of this function goes here
%   keyRelease( src, eventData, Image, mz, current, eq, varargin)

    % -------------------- Pressed Right --------------------
if strcmp(eventData.Key, 'rightarrow')
    if current < size(Image,3)
            % -------------- Equalise --------------
        if strcmp(args{1}, 'eq')
            imagesc(histEq(Image(:,:,current+1)))
            axis image
            title({['index: ' num2str(current+1)] ['m/z = ' num2str(mz(current+1))]});
                if length(args) == 2
                    colormap(args{2});
                end
            
            % ---------- Equalise & Normal ----------
        elseif strcmp(args{1}, 'both')
            subaxis(1,2,1,'Spacing',0.01,'SpacingVert',0.05,'MR',0.01,'ML',0.01,'MT',0.08,'MB',0.01,'Padding',0);
            imagesc(Image(:,:,current+1))
            axis image
            title({['index: ' num2str(current+1)] ['m/z = ' num2str(mz(current+1))]});
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            if length(args) == 2 
                colormap(args{2});
            end
            
            subaxis(1,2,2,'Spacing',0.01,'SpacingVert',0.05,'MR',0.01,'ML',0.01,'MT',0.08,'MB',0.01,'Padding',0);
            imagesc(histEq(Image(:,:,current+1)))
            axis image
            title({['Equalised'] ['index: ' num2str(current+1)] ['m/z = ' num2str(mz(current+1))]});
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            if length(args) == 2 
                colormap(args{2});
            end
            
            % -------------- Normal --------------
        else
            imagesc(Image(:,:,current+1))
            axis image
            title({['index: ' num2str(current+1)] ['m/z = ' num2str(mz(current+1))]});
            
        end
        
        set(src,'KeyReleaseFcn',{@keyReleaseSlide,Image,mz,current+1,args});
        
    else
        warning('Cannot go further forwards. Already at the last m/z-value')
    end
    
    % -------------------- Pressed Left --------------------
elseif strcmp(eventData.Key, 'leftarrow')
    if current > 1
            % -------------- Equalise --------------
        if strcmp(args{1}, 'eq')
            imagesc(histEq(Image(:,:,current-1)))
            axis image
            title({['index: ' num2str(current-1)] ['m/z = ' num2str(mz(current-1))]});
            if length(args) == 2
                colormap(args{2});
            end
            
            % ---------- Equalise & Normal ----------
        elseif strcmp(args{1}, 'both')
            subaxis(1,2,1,'Spacing',0.01,'SpacingVert',0.05,'MR',0.01,'ML',0.01,'MT',0.08,'MB',0.01,'Padding',0);
            imagesc(Image(:,:,current-1))
            axis image
            title({['index: ' num2str(current-1)] ['m/z = ' num2str(mz(current-1))]});
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            if length(args) == 2
                colormap(args{2});
            end
            
            subaxis(1,2,2,'Spacing',0.01,'SpacingVert',0.05,'MR',0.01,'ML',0.01,'MT',0.08,'MB',0.01,'Padding',0);
            imagesc(histEq(Image(:,:,current-1)))
            axis image
            title({['Equalised'] ['index: ' num2str(current-1)] ['m/z = ' num2str(mz(current-1))]});
            set(gca,'XTick',[])
            set(gca,'YTick',[])
            if length(args) == 2
                colormap(args{2});
            end
            
            % -------------- Normal --------------
        else
            imagesc(Image(:,:,current-1))
            axis image
            title({['index: ' num2str(current-1)] ['m/z = ' num2str(mz(current-1))]});
            
        end
        
        set(src,'KeyReleaseFcn',{@keyReleaseSlide,Image,mz,current-1,args});
    else
        warning('Cannot go further backwards')
    end
    
end

end

