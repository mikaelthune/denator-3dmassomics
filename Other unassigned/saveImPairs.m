function saveImPairs( ImCube, ImPairs, dir, varargin )
%UNTITLED Summary of this function goes here
%   saveImPairs( ImCube, ImPairs, dir, mask )


Npairs = size(ImPairs,1);
ydim = size(ImCube,1);
xdim = size(ImCube,2);
p = 100;
sc = 1.6;

h = figure('Position', [p, p, p + round(sc*(2*xdim + 100)), p + round(sc*ydim)]);

for k = 1:Npairs
    subaxis(1,2,1,'Spacing',0.01,...
        'SpacingVert',0,'MR',0.01,'ML',0.002,'MT',0,'MB',0,...
        'Padding',0);
    
    h1 = imagesc( ImCube(:,:,ImPairs(k,1)) );
    axis image
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    set(gca,'xcolor','w', 'ycolor','w', 'box','off', 'visible','off')
    if nargin > 3 && islogical(varargin{1})
        set(h1, 'alphadata', varargin{1});
    end
    
    subaxis(1,2,2,'Spacing',0.01,...
        'SpacingVert',0,'MR',0.002,'ML',0.01,'MT',0,'MB',0,...
        'Padding',0);
    
    h2 = imagesc( ImCube(:,:,ImPairs(k,2)) );
    axis image
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    set(gca,'xcolor','w', 'ycolor','w', 'box','off', 'visible','off')
    if nargin > 3 && islogical(varargin{1})
        set(h2, 'alphadata', varargin{1});
    end
    
    set(gcf,'units','centimeters')
    pos = get(gcf,'Position');
    
    set(gcf, 'PaperUnits','centimeters');
    set(gcf, 'PaperSize', [pos(3) pos(4)]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition',[0 0 pos(3) pos(4)]);
    
    set(gcf,'Color','w')

    saveas(h,fullfile( dir, [num2str(k) '.png'] ))
    
    if mod(k,Npairs/10) == 0
        disp([num2str(100*k/Npairs) '%'])
    end
end

end

