function ImPairs = randPairsAllComb( Nim )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

AllPairs = nchoosek(1:Nim,2);
Npairs = size(AllPairs,1);

rng('shuffle');
LRrand = randi(2,1,Npairs);

for k = 1:Npairs
    if LRrand(k) == 1
        AllPairs(k,:) = AllPairs(k,[2 1]);
    end
end

ImPairs = AllPairs(randperm(Npairs),:);


end