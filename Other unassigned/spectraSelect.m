function spectrum = spectraSelect( Image, mz, varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin == 3
    Idisp = Image(:,:,varargin{1});
else
    Idisp = mean(Image,3);
end

h = figure;
imagesc(histEq(Idisp));
axis image
title({'select pixel'},'Interpreter','none')
colormap('Gray')

spectrum = [];
run = 1;
k = 1;
while run
    figure(h)
    [x y button] = ginput(1);

    x = round(x);
    y = round(y);
    
    % Pressed Left mouse = Select Point
    if(button == 1)
        spectrum = squeeze(Image(y,x,:));

        figure; bar(mz,spectrum);
        axis tight
        title(['Spectrum for coordinate: x = ' num2str(x) ', y = ' num2str(y)])
        xlabel('m/z')
        ylabel('Normalised Intensity')
        
    % pressed Return = Finished
    elseif isempty(button)
        run = 0;
    end;
    k = k + 1;
end;

end

