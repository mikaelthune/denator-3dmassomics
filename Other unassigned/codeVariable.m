function coded = codeVariable(natural)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

maxVal = max(natural);
minVal = min(natural);

coded = (natural - (minVal+maxVal)/2) ./ ((maxVal-minVal)/2); 


end

