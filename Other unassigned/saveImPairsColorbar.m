function saveImPairsColorbar( ImCube, ImPairs, dir, tissuemask, varargin )
%UNTITLED Summary of this function goes here
%   saveImPairs( ImCube, ImPairs, dir, tissuemask, namePost, nameAppend )


Npairs = size(ImPairs,1);
ydim = size(ImCube,1);
xdim = size(ImCube,2);
p = 100;
sc = 1.5;

left = p;
bottom = p;
width = p + round(sc*(2*xdim + 190));
height = p + round(sc*ydim*1.2);
pos = [left, bottom, width, height];
borderPos = [2 2 width-2 height-2];

for k = 1:Npairs
    f = figure('Position', pos);
    axes('units','pixels','Position',borderPos,'xtick',[],'ytick',[],'box','on','handlevisibility','off')

    %% Left Image --------------------------------------------------------
    a1 = subaxis(1,2,1,'Spacing',0.01, 'SpacingVert',0, 'MR',0.01,...
        'ML',0, 'MT',0, 'MB',0, 'Padding',0);
    h1 = imagesc( ImCube(:,:,ImPairs(k,1)) );
    axis image
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    
    a1Pos = get(a1,'Position');
    newPos1 = [0.03 0 (a1Pos(3)-0.03) a1Pos(4)];
    set(a1,'Position',newPos1);
    
    colorbar('XTickLabel',{}, 'YTickLabel',{}, 'YTick',[],...
        'Position',[0.005 0.15 0.015 0.7])
    set(gca,'xcolor','w', 'ycolor','w', 'box','off', 'visible','off')
    if size(tissuemask,3) > 1;
        set(h1, 'alphadata', tissuemask(:,:,ImPairs(k,1)));
    else
        set(h1, 'alphadata', tissuemask);
    end


    %% Right Image -------------------------------------------------------
    a2 = subaxis(1,2,2,'Spacing',0.01, 'SpacingVert',0, 'MR',0,...
        'ML',0.01, 'MT',0, 'MB',0, 'Padding',0);
    
    h2 = imagesc( ImCube(:,:,ImPairs(k,2)) );
    axis image
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    
    a2Pos = get(a2,'Position');
    newPos2 = [a2Pos(1) 0 a2Pos(3)-0.03 a2Pos(4)];
    set(a2,'Position',newPos2);
    
    colorbar('XTickLabel',{}, 'YTickLabel',{}, 'YTick',[],...
        'Position',[(newPos2(1)+newPos2(3)+0.01) 0.15 0.015 0.7])
    set(gca,'xcolor','w', 'ycolor','w', 'box','off', 'visible','off')
    if size(tissuemask,3) > 1;
        set(h2, 'alphadata', tissuemask(:,:,ImPairs(k,2)));
    else
        set(h2, 'alphadata', tissuemask);
    end
    
    %% Save --------------------------------------------------------------
    set(f,'units','centimeters')
    pos2 = get(f,'Position');
    
    set(f, 'PaperUnits','centimeters');
    set(f, 'PaperSize', [pos2(3) pos2(4)]);
    set(f, 'PaperPositionMode', 'manual');
    set(f, 'PaperPosition',[0 0 pos2(3) pos2(4)]);
    
    set(f,'Color','w')
    
    if nargin > 4
        saveas(f,fullfile( dir, [varargin{1} num2str(k) varargin{2} '.png'] ))
    else
        saveas(f,fullfile( dir, [num2str(k) '.png'] ))
    end
    
    close(f)
    
    if mod(k,Npairs/10) == 0
        disp([num2str(100*k/Npairs) '%'])
    end
end

end

