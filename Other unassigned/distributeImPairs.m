function SurveyPairList = distributeImPairs( ImPairs, NSurveys )
%Distribute ImPairs amongst N Surveys so that each pair exist exactly 3
%times in total

SurveyPairList = cell(NSurveys,1);
NpairsLeft = size(ImPairs,1);
timesSelected = zeros(NpairsLeft,1);
notFinished = true;
rng('shuffle')
while notFinished
    
    for s = 1:NSurveys
        notUnique = true;
        
        while notUnique
            num = randi(NpairsLeft,1,1);
            
            if size(SurveyPairList{s},1) > 0
                comp = repmat(ImPairs(num,:),size(SurveyPairList{s},1),1);
                notUnique = ~isempty(find(sum(SurveyPairList{s}==comp, 2 )==2, 1 ));
                
            else
                notUnique = false;
            end
        end
        
        SurveyPairList{s} = [SurveyPairList{s}; ImPairs(num,:)];
        timesSelected(num) = timesSelected(num) + 1;
        
        if timesSelected(num) == 3
            ImPairs(num,:) = [];
            timesSelected(num) = [];
            NpairsLeft = NpairsLeft - 1;
        end
        
        if NpairsLeft == 0
            notFinished = false;
            break;
        end
        
    end
end

end

