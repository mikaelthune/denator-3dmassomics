function [newPeaks, ind] = choosePeaks( spec )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[xmax,imax,xmin,imin] = extrema(spec);

notAnswered = 1;
newPeaks = [];
ind = [];
for k = 1:length(xmax)
    bar(spec);
    hold on
    plot(imax(k), xmax(k), 'r*')
    hold off
    
    result = input('1 = Save, 0 = Discard, -1 = Quit: ');
    
    notAnswered = 1;
    while notAnswered
        if result == 1
            newPeaks = [newPeaks xmax(k)];
            ind = [ind imax(k)];
            notAnswered = 0;
            disp(['Saving Peak ' num2str(imax(k))])
        elseif result == 0
            disp(['Discarding Peak'])
            notAnswered = 0;
        elseif result == -1
            disp(['Quiting'])
            notAnswered = 0;
            return
        else
            disp('Incorrect input')
        end
    end
    
end


end

