function holes = datasetHoles( I, varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~iscell(I)
    I = {I};
end

Nim = length(I);
holes = cell(Nim,1);
for im = 1:Nim
    
    
    if sum(isnan(I{1}(:))) == 0
        % Background is zero
        Sample = sum(I{im},3) ~= 0;
    else
        % Background is NaN
        Sample = ~isnan(I{im}(:,:,1));
    end
    
    holes{im} = zeros(size(I{im},3),1);
    
    for m = 1:size(I{im},3)
        inside = and( I{im}(:,:,m) == 0, Sample);
        holes{im}(m) = sum(inside(:));
    end
    
    if nargin == 2 && strcmp(varargin{1},'%')
        holes{im} = holes{im}./sum(Sample(:));
    end
    
end

if Nim == 1
    holes = holes{1};
end

end

