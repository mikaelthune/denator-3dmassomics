function ind = extractIndFromPeaks( mzPeaks, mzAll )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Npeaks = length(mzPeaks);

ind = zeros(length(mzPeaks),1);
for k = 1:Npeaks
    ind(k) = find(mzAll == mzPeaks(k));
end

end

