function startSlideshow( Image, mz, varargin )
%UNTITLED Summary of this function goes here
%   startSlideshow( Images, mz, 'eq', color )

Nmz = size(Image,3);
if Nmz ~= length(mz)
    error('Incompatible Image and mz-values')
end

if nargin >= 3 && sum(strcmpi(varargin, 'eq')) == 1
    figure('KeyReleaseFcn',{@keyReleaseSlide,Image,mz,1,varargin}, 'Name','Slideshow','NumberTitle','off');
    imagesc(histEq(Image(:,:,1)))
    axis image
    title({['index: ' num2str(1)] ['m/z = ' num2str(mz(1))]});
    if nargin == 4
        colormap(varargin{2});
    end
    
elseif nargin >= 3 && sum(strcmpi(varargin, 'both')) == 1
    figure('KeyReleaseFcn',{@keyReleaseSlide,Image,mz,1,varargin}, 'Name','Slideshow','NumberTitle','off');
    subaxis(1,2,1,'Spacing',0.01,'SpacingVert',0.05,'MR',0.01,'ML',0.01,'MT',0.08,'MB',0.01,'Padding',0);
    imagesc(Image(:,:,1))
    axis image
    title({['index: ' num2str(1)] ['m/z = ' num2str(mz(1))]});
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    if nargin == 4
        colormap(varargin{2});
    end
    
    subaxis(1,2,2,'Spacing',0.01,'SpacingVert',0.05,'MR',0.01,'ML',0.01,'MT',0.08,'MB',0.01,'Padding',0);
    imagesc(histEq(Image(:,:,1)))
    axis image
    title({['Equalised'] ['index: ' num2str(1)] ['m/z = ' num2str(mz(1))]});
    set(gca,'XTick',[])
    set(gca,'YTick',[])
    if nargin == 4
        colormap(varargin{2});
    end
else
    figure('KeyReleaseFcn',{@keyReleaseSlide,Image,mz,1,{''}}, 'Name','Slideshow','NumberTitle','off');
    imagesc(Image(:,:,1))
    axis image
    title({['index: ' num2str(1)] ['m/z = ' num2str(mz(1))]});
    
    if nargin >= 3
        colormap(varargin{1});
    end
    
end

end

