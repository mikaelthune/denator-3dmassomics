function Ieq = histEq( I, varargin )
%UNTITLED Summary of this function goes here
%   Contrast enhancment. To use histogram equalisation specify 'histeq'
%   Ieq = histEq( I, 'histeq' )
%   Ieq = histEq( I, quantileVal )

if nargin == 2 && strcmpi(varargin{1},'histeq')
    Npixels = numel(I);
    values = unique(I);
    Non_NaN = ~isnan(values);
    values = values(Non_NaN);
    
    Sample = ~isnan(I);
    
    maxVal = values(end);
    Nvals = length(values);
    
    p = histc(I(Sample),values)./Npixels;
    
    Ieq = NaN(size(I));
    for k = 1:Nvals
        ind = I == values(k);
        Ieq(ind) = maxVal*sum(p(1:k));
    end
    
elseif nargin == 2 && isnumeric(varargin{1})
    Ieq = I;
    qval = quantile(I(:), varargin{1});
    Ieq(I > qval) = qval;
else
    Ieq = I;
    qval = quantile(I(:),0.99);
    Ieq(I > qval) = qval;
end


end

