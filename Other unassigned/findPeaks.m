function peaksInd = findPeaks( mz, spectra )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

warning('currently only works for Pancreas dataset')

mzDiff = diff(mz);
borders = find(mzDiff > 0.06);

Npeaks = length(borders)+1;
peaksInd = zeros(Npeaks,1);

[~,peaksInd(1)] = max( spectra( 1:borders(1) ) );
[~,tempInd] = max( spectra( borders(end):end ) );
peaksInd(end) = tempInd + borders(end) - 1;

for p = 2:Npeaks-1
    [~,tempInd] = max( spectra( borders(p-1)+1 : borders(p) ) );
    peaksInd(p) = borders(p-1) + tempInd;
end

end

