function saveFiguresAsPng( figureHandles, directory )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

for k = 1:length(figureHandles)
    nameStr = get(figureHandles(k), 'Name');
    saveas(figureHandles(k), [fullfile(directory,nameStr) '.png'], 'png');
end

end

