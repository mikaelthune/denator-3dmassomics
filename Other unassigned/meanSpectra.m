function meanSpec = meanSpectra( I )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if ~iscell(I)
    I = {I};
end

Nim = length(I);
Nmz = size(I{1},3);

meanSpec = cell(Nim,1);
for im = 1:Nim
    
    if sum(isnan(I{1}(:))) == 0
        % Background is zero
        Sample = sum(I{im},3) ~= 0;
    else
        % Background is NaN
        Sample = ~isnan(I{im}(:,:,1));
    end
    
    meanSpec{im} = zeros(Nmz,1);
    for m = 1:Nmz
        Itmp = I{im}(:,:,m);
        meanSpec{im}(m) = mean(Itmp(Sample));
    end
    
end

if Nim == 1
    meanSpec = meanSpec{1};
end

end
