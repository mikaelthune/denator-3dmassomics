function boxplotForDatasetDescriptors( DatasetDescriptorCell )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

NMethods = length(DatasetDescriptorCell{1}.imMethod);
NDescriptors = length(DatasetDescriptorCell{1}.imMethod(1).descriptor);

warndlg('Not finished!')

% Find quantile descriptor
for q = 1:NDescriptors
    if strcmpi(DatasetDescriptorCell{1}.imMethod(1).descriptor(q).name, 'Quantiles')
        NQ = length(DatasetDescriptorCell{1}.imMethod(1).descriptor(q).value);
        Qind = q;
    end
end
PlotMatrix = zeros(NMethods,NDescriptors+NQ-1,36);
for m = 1:NMethods
    
    for k = 1:36
        count = 0;
        for p = 1:NDescriptors
            if p == Qind
                for q = 1:NQ
                    count = count + 1;
                    PlotMatrix(m,count,k) = DatasetDescriptorCell{k}.imMethod(m).descriptor(p).value(q);
                end
            else
                count = count + 1;
                PlotMatrix(m,count,k) = DatasetDescriptorCell{k}.imMethod(m).descriptor(p).value;
            end
                    
        end
    end
    
end

%% Plotting
for m = 1:NMethods
    for k = 1:NDescriptors+NQ-1
        figure;
        boxplot(PlotMatrix(:,k));
        title({ ['Image Method: ' DatasetDescriptorCell{k}.imMethod(m).name] ...
            [] })
    end
end


end

