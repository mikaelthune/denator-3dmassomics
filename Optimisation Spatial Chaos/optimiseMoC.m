function [params, fval,exitflag,output,population,scores] = optimiseMoC( ImCube, ImPairs, chosenLR, NImcubes )
%UNTITLED Summary of this function goes here
%   NImcubes = the number of different image cubes in the cell arrays

lowBound = [2 0.05 0.05 1];
uppBound = [30 15 15 18];
integerParams = [1 4];

opts = gaoptimset(@ga);
opts.Display = 'diagnose';
opts.PlotFcns = @gaplotbestf;
opts.Generations = 200;
opts.TimeLimit = 60*60*20;
opts.PopInitRange = [lowBound; uppBound];
opts.PopulationSize = 60;
disp(['Using PopulationSize =  ' num2str(opts.PopulationSize)])

if NImcubes == 1
    isLeftBest_human = descisionListFromHumanComp(chosenLR);
    
    [mzInd, ~, ~] = unique(ImPairs(:));
    newImCube = ImCube(:,:, mzInd );
    
    newImPairs = ImPairs;
    for k = 1:length(mzInd)
        newImPairs( newImPairs == mzInd(k) ) = k;
    end
    
    f = @(x)parameterAddMoC_4param(x, newImCube, newImPairs, isLeftBest_human);
    
else
    for im = 1:NImcubes
        isLeftBest_human{im} = descisionListFromHumanComp(chosenLR{im});
        
        [mzInd, ~, ~] = unique(ImPairs{im}(:));
        newImCube{im} = ImCube{im}(:,:, mzInd );
        
        newImPairs{im} = ImPairs{im};
        for k = 1:length(mzInd)
            newImPairs{im}( newImPairs{im} == mzInd(k) ) = k;
        end
    end
    % 4 parameters
    f = @(x)parameterAddMoC_3ims_4param(x, newImCube, newImPairs, isLeftBest_human);
    
end

% 4 parameters
[params, fval, exitflag, output, population, scores] = ...
    ga(f, 4, [], [], [],[], lowBound, uppBound, [], integerParams, opts);

end



%     % 3 parameters
%     f = @(x)parameterAddMoC_3ims(x, newImCube, newImPairs, isLeftBest_human);

%     % 5 parameters
%     f = @(x)parameterAddMoC_3ims_5param(x, newImCube, newImPairs, isLeftBest_human);

% % 3 parameters
% [params, fval] = ga(f, 3, [],[],[],[], [2 0.1 0.1], [22 10 10], [], 1, opts);

% % 5 parameters
% [params, fval] = ga(f, 5, [0 0 0 1 -1], [-1], [],[],...
%         [2, 0.05, 0.05, 1, 2], [30, 15, 15, 18, 19], [], [1 4 5], opts);