function accuracy = parameterAddMoC_4param( params, ImCube, ImPairs, isLeftBest_human)
%UNTITLED Summary of this function goes here
%   isLeftBest_human = descisionListFromHumanComp(chosenLR)
%   4 parameters

% Create quantile vector
quantVec = params(4)*0.05:0.05:0.95;


NPairs = size(ImPairs,1);
[mzInd, ~, ic] = unique(ImPairs(:));
Nind = length(mzInd);

methodVals = zeros(Nind,1);
parfor k = 1:Nind
    methodVals(k) = calcMoC(ImCube(:,:,mzInd(k)), 'omega',params(1), 'sigma_d',params(2), 'sigma_i',params(3), 'quantVec',quantVec );
end

ValuePairs = reshape(methodVals(ic),NPairs,2);
IsLeftBest_method = ValuePairs(:,1) < ValuePairs(:,2);

accuracy = -sum( isLeftBest_human == IsLeftBest_method )./NPairs;

end

