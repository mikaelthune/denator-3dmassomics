function accuracy = parameterAddMoC_3ims( params, ImCube, ImPairs, isLeftBest_human )
%UNTITLED Summary of this function goes here
%   isLeftBest_human = descisionListFromHumanComp(chosenLR)
%   3 parameters

%% Image 1
NPairs = size(ImPairs{1},1);
[mzInd, ~, ic] = unique(ImPairs{1}(:));
Nind = length(mzInd);

methodVals = zeros(Nind,1);
parfor k = 1:Nind
    methodVals(k) = calcMoC(ImCube{1}(:,:,mzInd(k)), 'omega',params(1), 'sigma_d',params(2), 'sigma_i',params(3));
end

ValuePairs = reshape(methodVals(ic),NPairs,2);
IsLeftBest_method = ValuePairs(:,1) < ValuePairs(:,2);

accuracy1 = sum( isLeftBest_human{1} == IsLeftBest_method )./NPairs;

%% Image 2
NPairs = size(ImPairs{2},1);
[mzInd, ~, ic] = unique(ImPairs{2}(:));
Nind = length(mzInd);

methodVals = zeros(Nind,1);
parfor k = 1:Nind
    methodVals(k) = calcMoC(ImCube{2}(:,:,mzInd(k)), 'omega',params(1), 'sigma_d',params(2), 'sigma_i',params(3));
end

ValuePairs = reshape(methodVals(ic),NPairs,2);
IsLeftBest_method = ValuePairs(:,1) < ValuePairs(:,2);

accuracy2 = sum( isLeftBest_human{2} == IsLeftBest_method )./NPairs;

%% Image 3
NPairs = size(ImPairs{3},1);
[mzInd, ~, ic] = unique(ImPairs{3}(:));
Nind = length(mzInd);

methodVals = zeros(Nind,1);
parfor k = 1:Nind
    methodVals(k) = calcMoC(ImCube{3}(:,:,mzInd(k)), 'omega',params(1), 'sigma_d',params(2), 'sigma_i',params(3));
end

ValuePairs = reshape(methodVals(ic),NPairs,2);
IsLeftBest_method = ValuePairs(:,1) < ValuePairs(:,2);

accuracy3 = sum( isLeftBest_human{3} == IsLeftBest_method )./NPairs;

%% Total Accuracy
accuracy = -1*(exp(accuracy1) + exp(accuracy2) + exp(accuracy3));


end

