%--------------------------------------------------------------------------
%Created by Mikael Thun�
%
% As part of his thesis work:
%       Eigen-birds: Exploring avian morphospace with image analytic tools
%
% Centre for Image Analysis, Uppsala Univerity
% 2011-08-14
%--------------------------------------------------------------------------

function Icell = read_images(directory,imtype)
%READ_IMAGES Read images from directory
%
%   ICELL = READ_IMAGES(DIRECTORY,IMTYPE) Reads all the images of type
%   IMTYPE that are located in DIRECTORY and returns them. ICELL is a
%   cell-array of size 2xN, where N is the number of images. The first
%   row contains the images and the second row contains the corresponding
%   names of the images.

% Remove eventual '.'
imtype = strrep(imtype,'.','');

% Get information of all files of type 'imtype' in directory
files = dir(fullfile(directory,['*.' imtype]));
N = size(files,1);
Icell = cell(2,N);

disp('Reading images...')

for k = 1:N
    name = files(k,1).name;
    % Remove file ending
    Icell{2,k} = strrep(name,['.' imtype],'');
    Icell{1,k} = imread(fullfile(directory,name));
end

disp('Finished Reading')