function [Intensity, Coord, mz] = importMSData(filePath)
%IMPORTMSDATA Summary of this function goes here
%   Detailed explanation goes here

%%
delimiter = {',',' '};

fid = fopen(filePath);
[~] = textscan(fid,'%[m/z]');
mz = textscan(fid,',%f');

mz = mz{1,:};
Nmz = length(mz);

ScanOutput = textscan(fid, ['%s' repmat('%f',1,Nmz)], 'Delimiter', delimiter, 'MultipleDelimsAsOne', true);
fclose(fid);

posX = strfind(ScanOutput{1}{1},'X');
posY = strfind(ScanOutput{1}{1},'Y');
Ndig = posY - posX - 1;

Ncoord = length(ScanOutput{1});
Coord = zeros(Ncoord,2);
for k = 1:Ncoord
    Coord(k,1) = str2double(ScanOutput{1}{k}((posX+1):(posY-1)));
    Coord(k,2) = str2double(ScanOutput{1}{k}((posY+1):(posY+Ndig)));
end

Intensity = cell2mat(ScanOutput(2:end));    

end

