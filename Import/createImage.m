function I = createImage( Intensity, Coord, mz)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

xMax = max(Coord(:,1));
yMax = max(Coord(:,2));
Ncoord = size(Coord,1);
Nmz = length(mz);

indices = sub2ind([yMax xMax Nmz], repmat(Coord(:,2),Nmz,1), repmat(Coord(:,1),Nmz,1),...
                                    reshape(repmat((1:Nmz)',1,Ncoord)', Nmz*Ncoord, 1));
I = zeros(yMax,xMax,Nmz);
I(indices) = Intensity(:);

I = cutBackground(I);
NaNvalues = repmat(sum(I,3) == 0,[1 1 Nmz]);
I(NaNvalues) = NaN;

end

