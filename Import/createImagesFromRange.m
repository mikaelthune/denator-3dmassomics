function Images = createImagesFromRange( Intensity, Coord, mz, ranges )
%Creates multiple images from a single dataset, according to information in
%ranges.


Nim = size(ranges,1);

Images = cell(Nim,1);
for k = 1:Nim
    Images{k} = createImage(Intensity(ranges(k,1):ranges(k,2), :), ...
                            Coord(ranges(k,1):ranges(k,2), :), mz);
end

end

