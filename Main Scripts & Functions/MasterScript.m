% Master script for going through all processes described in the
% instructions document.
% This script should only be used for the 3DMASSOMICS project - WP3
add3DMpaths()
addpath('/home/palmer/Documents/MASSOMICS/codebase/thirdParty/ojwoodford-export_fig-5735e6d/')
%% Check Consistency
% this is superceded with GA for optimised rater agreement
% CheckMultipleRunDirsForConsistency;

%% Import Survey Responses
% First you have to load the mat-file located in the FTP path:
% /data/Denator/Important Data/Data for Matlab/Full Survey related Data/Pair Information
% load('/home/palmer/Documents/MASSOMICS/Data/Important Data/Data for Matlab/Full Survey related Data/Pair Information/ImagePairs_FullSurvey.mat')
load('/media/palmer/Backup/dataStore/MASSOMICS/Data/Important Data/Data for Matlab/Full Survey related Data/Pair Information/ImagePairs_FullSurvey.mat')

disp('-------------------- Import Survey Responses --------------------')
disp(' ')
if ~exist('ImPairs_rand') || ~exist('SurveyPairList')
    disp('First you have to load the mat-file with pair information located in the FTP path:')
    disp('/data/Denator/Important Data/Data for Matlab/Full Survey related Data/Pair Information')
    error('Error: See above ')
end

% Enter the directory that contains all csv files you want to use
% importDir = '/media/palmer/Backup/dataStore/MASSOMICS/Data/Full Survey Responses/ConsistencyCheck/all_response';
importDir ='/media/palmer/Backup/dataStore/MASSOMICS/Data/Full Survey Responses/ConsistencyCheck/all_OK';
% importDir = '/media/palmer/Backup/dataStore/MASSOMICS/Data/Full Survey Responses/ConsistencyCheck/ga_optimised'
if isempty(importDir)
    disp('Make sure you have all the csv-files you want to import in one single directory.')
    disp('Press any key to continue')
    disp(' ')
    pause
    importDir = input('Enter import directory: ', 's');
    if strcmpi(importDir(1), '''')
        error('Enter directory without apostrophes')
    end
end
FullSurveyStruct = createFullSurveyStruct(importDir, SurveyPairList, ImPairs_rand); % note: scales slider values
disp('Finished')
%% Optimise k_alpha
categoryThold = 0.1;% rounding precision, 1/multiple of 10
categoryFunction = @(x)(round(x/categoryThold)*categoryThold);
[ratings, timings, rawdata] = generateDataTables(FullSurveyStruct,categoryFunction);
[k_alpha, agreement_table_out] = calcualteKrippendorfAlpha(ratings);
% Remove one question at a time and re-check score
k_alpha_im = zeros(size(ratings,1),1);
for n=1:size(ratings,1)
    [k_alpha_im(n), agreement_table_out] = calcualteKrippendorfAlpha(ratings(1:size(ratings,1)~=n,:));
end
% Show results
figure, 
subplot(1,2,1)
plot(100*(k_alpha_im-k_alpha)/k_alpha)
title('alpha_k with each pair removed')
xlabel('pair removed idx')
ylabel('percentage change to alpha_k')
set(gca,'XLim',[1 length(k_alpha_im)])
% saveas(gcf,'ka_alpha_eachPair_removed.png')
% removing members -> increased k_alpha
% so want to exclude k_alpha_im with large k_alpha
% sort k_alpha_im
[~,k_alpha_im_idx] = sort(k_alpha_im,'ascend');
q_vals = (1:10:length(k_alpha_im))/length(k_alpha_im);
% q_vals = 0.01:0.05:1;
ka_q = zeros(length(q_vals),1);
for n=1:length(q_vals) 
%     ka_q(n) = calcualteKrippendorfAlpha(ratings(k_alpha_im_idx(1:ceil(length(q_vals)*q_vals(n))),:)); 
   ka_q(n) = calcualteKrippendorfAlpha(ratings(k_alpha_im<quantile(k_alpha_im,q_vals(n)),:)); 

end
subplot(1,2,2),
plot(q_vals, ka_q)
set(gca,'YLim',[0 1])
title('alpha_k with images removed - remmoving poorly performing individuals (below quintile)')
xlabel('quintile')
ylabel('alpha_k')

% - choose quintile
flag_auto = 1;
if flag_auto
    [~,q_val_idx] = max(ka_q);
    q_val = q_vals(q_val_idx);
else
    q_val = input('Enter quintile  ', 's');
end
pairs_agreement = k_alpha_im<quantile(k_alpha_im,q_val);
% disp(sum(pairs_agreement))

%% Plot time against slider std

tmp_timings=timings'; 
tmp_timings=reshape(tmp_timings(~isnan(tmp_timings)),3,[]); % time per pair (loose user information)
tmp_ratings=ratings'; 
tmp_ratings=reshape(tmp_ratings(~isnan(tmp_ratings)),3,[]); % time per pair (loose user information)

figure('color','w')
subplot(1,2,1)
plot(mean(tmp_timings), mean(tmp_ratings),'.')
x=1:100;
y=-3:0.1:3;
N = hist3([mean(tmp_timings);mean(tmp_ratings)]',[{x},{y}]);
hold all
contour(x,y,medfilt2(N',[5 5]),5,'LineWidth',3)% show smoothed density contours
colormap('Copper');
xlim([0 quantile(mean(tmp_timings),0.995)])
title('mean time vs mean  slider value','FontSize',18)
xlabel('time (s)','FontSize',18)
ylabel('mean slider value','FontSize',18)
set(gca,'FontSize',18)
subplot(1,2,2)
plot(mean(tmp_timings), std(tmp_ratings),'.')
N = hist3([mean(tmp_timings);std(tmp_ratings)]',[{x},{y}]);
hold all
contour(x,y,medfilt2(N',[5 5]),5,'LineWidth',3)% show smoothed density contours
set(gca,'FontSize',18)
title('mean time vs standard deviation of slider value','FontSize',18)
xlabel('time (s)','FontSize',18)
ylabel('standard deviation of slider value','FontSize',18)
xlim([0 quantile(mean(tmp_timings),0.995)])

flag_save_im = 1;
if flag_save_im
%     saveas(gcf','time_vs_slider.png')
    export_fig('time_vs_slider.png')
end
%% Determine 'gold standard' image pairs
n_ratings_target = 3;
gs_consist_thresh = 0.25;
[pairRatingSummary, summaryHeaders] = summarisePairRatings(FullSurveyStruct,categoryThold);
n_ratings = checkNumberRatings(pairRatingSummary,find(~cellfun(@isempty,strfind(summaryHeaders,'slider value'))));

gs_def=@(x,y,z) x < gs_consist_thresh &y==n_ratings_target & z;
goldStandardPairs = gs_def(pairRatingSummary(:,7),n_ratings,pairs_agreement);

% gs_def=@(x,y) x & y==n_ratings_target;
% goldStandardPairs = gs_def(pairs_agreement,n_ratings);
disp('Gold Standard Calculated')
sum(goldStandardPairs)

print_gs = 0;
if print_gs
    for n=1:length(goldStandardPairs);
        if goldStandardPairs(n);
            subplot(1,2,1);
            imagesc(ImCube_rand_filled(:,:,pairRatingSummary(n,2)));
            title(['Pair index:' num2str(n) '. Average rater value: '  num2str(mean(pairRatingSummary(n,4:6)))])
            
            axis image; axis off
            subplot(1,2,2);
            imagesc(ImCube_rand_filled(:,:,pairRatingSummary(n,3)));
            axis image; axis off
            title( ['rater std ' num2str(pairRatingSummary(n,7))])
            saveas(gcf,['pair_' num2str(n) '.png']);
        end
    end
end
%% Calculate Image Descriptor for images in Survey
disp(' ')
disp('---------------- Calculate Image Descriptors Survey ----------------')
disp(' ')
if ~exist('ImCube_rand_filled','var') || ~exist('mz_rand','var')
    disp('First Load ImCube used in survey:')
    disp('Can be found in FTP path:')
    disp('/data/Denator/Important Data/Data for Matlab/Full Survey related Data/Images in Full Survey')
    error('Error: See above')
end

window = input('Enter 1x2 window size array for image descriptors (use [ ]): ');
if ~isnumeric(window)
    error('Must be numeric')
elseif length(window) ~= 2
    error('Size must be 1x2')
end

MethodResultsForSurveyIm = calculateImageDescriptors(ImCube_rand_filled, mz_rand, window );
disp('Finished')

%% Comparing human judgment Vs Image descriptors
disp(' ')
disp('------------- Comparing human judgment Vs Image descriptors -------------')
disp(' ')

[ComparisonTable, correlation] = createHumanAlgorithmComparisonTable( FullSurveyStruct, MethodResultsForSurveyIm,'goldStandardPairs',goldStandardPairs);
sign_match = sum(bsxfun(@eq,sign(ComparisonTable(:,4:end)),sign(ComparisonTable(:,3))))/length(ComparisonTable);
disp(correlation)
disp(sign_match)

disp('Finished')
disp('Metrics included')
% Create a group algorithm metric
%       [idx_1 idx_2 slider 'COVmean' 'COVmedian' 'SNRmean' 'SNRmedian' 'MSE' 'SpatialChaos' 'STDmean' 'STDmedian']
alg_idx = [1     1     1       1          1            1         1        0         1           1           1]; 
alg_idx = logical(alg_idx);
[x_keep_lin, fval_keep_lin,test_corr] = createGroupAlgorithmMeasure(ComparisonTable(:,alg_idx));
[~,idx] = max(fval_keep_lin); 
% x_keep_lin(:,idx)
% fval_keep_lin(idx)
% test_corr(idx)
alg_idx=alg_idx(4:end); % just record algorithms used
disp(alg_idx)

disp(median(test_corr(fval_keep_lin>max(correlation(alg_idx)))))
[v,max_fval_idx] = max(test_corr);
disp(v)
disp(x_keep_lin(:,max_fval_idx)')
disp(fval_keep_lin(max_fval_idx))
% save(['optimised_algorithm' datestr(now,'yymmdd_HHMMSS') '.mat'],'x_keep_lin', 'fval_keep_lin','test_corr','alg_idx','goldStandardPairs','gs_def','max_fval_idx')

% take statistical average of high scoring coefficients and check it against data.
group_coeffs = mean(x_keep_lin(:,test_corr>max(correlation(alg_idx))),2);
group_coeffs = group_coeffs/(10^mean(real(log10(group_coeffs)))); %scale out crazy high powers of 10
disp('Group Coefficients')
disp(group_coeffs')

tmp_ComparisonTable = ComparisonTable(:,[true true true alg_idx]);
disp(corr(ComparisonTable(:,3),linearAlgorithmCombination(tmp_ComparisonTable(:,4:end),group_coeffs')))


%% Calculating Image Descriptors for all datasets in reproducibillity analysis
disp(' ')
disp('---------------- Calculate Image Descriptors for all datasets ----------------')
disp(' ')

imCubeDir = input('Enter directory of all the image cubes: ', 's');
if strcmpi(imCubeDir(1), '''')
    error('Enter directory without apostrophes')
end
disp(' ')

ImDescSaveDir = input('Enter directory where you want to save the Image Descriptors ', 's');
if strcmpi(ImDescSaveDir(1), '''')
    error('Enter directory without apostrophes')
end
disp(' ')

window = input('Enter 1x2 window size array for image descriptors: ');
if ~isnumeric(window)
    error('Must be numeric')
elseif length(window) ~= 2
    error('Size must be 1x2')
end

ResultCell = calculateImageDescriptorsForAllInDir( imCubeDir, window, ImDescSaveDir);
disp('Finished')
% ResultCell = calculateGroupAlgorithmMeasure(ResultCell_5x5_default,x_keep_lin);
%% Calculating Dataset Descriptors
disp(' ')
disp('------------- Calculate Dataset Descriptors for all datasets -------------')
disp(' ')
MethodResultCell = ResultCell;
CalculateDatasetDescriptorsForAll;
disp('Finished')

%% Plot Main Effects
disp(' ')
disp('---------------- Plot Main Effects ----------------')
disp(' ')

disp('Will now plot main effects for ALL combinations!')
disp('Press any key to proceed')
disp(' ')
pause

plotMainEffectsForDatasets( DatasetDescriptorCell, 'all' )

