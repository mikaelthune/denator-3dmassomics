% Script for checking responses in multiple directories for consistency. It
% will then tell you which files should be copied where in order to get the
% correct folder structure. This wil be done step by step.
%
% The script will check all folders with names containing 'Run' in the
% parent directory. These folders should contain the exported response
% csv-files from LimeSurvey.
%
% The script can be used for rearranging files to their new correct place
% when the consistency cut off or slider scaling has been changed.
%
% The script is based on the function checkDirForInconsitency()

%% Change if required
% NInfoString is a survey parameter that says how many initial fields the 
% csv-files have before the custom fields start appearing.
NInfoStrings = 6; % 6 for 3DMassomics survey

% endString is a survey specific paramter that says what the field name is 
% in the csv-files.
endString = 'QEval_CommentTime'; % 'QEval_CommentTime' for 3DMassomics survey

%% User input
disp(' ')
disp('FIRST make sure that the variables NInfoStrings and endString,')
disp('located at the top of this script, are correct.')
disp(['Their current values are: ' num2str(NInfoStrings) ' and ''' endString ''''])

% Enter the parent directory that contains the 'Run' folders.
disp(' ')
parentDirRunFolders = input('Enter parent directory for consistency check: ', 's');
if strcmpi(parentDirRunFolders(1), '''')
    error('Enter directory without apostrophes')
end

% Verify folder structure
verifySurveyExportFolderStructure(parentDirRunFolders);

% Enter the standard deviation value that should be used as a cut off for
% consistency.
stdVal_cutoff = input('Enter standard deviation to use for consistency cut off value: ');

%% Calculate
% Find the 'Run' folders
runFolderNames = findFoldersWithName(parentDirRunFolders, 'Run', 'sortByEndNumber');
NFolders = length(runFolderNames);

inconsistentFiles = cell(NFolders,1);
consistentFiles = cell(NFolders,1);

% Loop through 'Run' folders
for f = 1:length(runFolderNames)
    % Check csv-files in current folder for consistency
    [inconsistentFiles{f}, consistentFiles{f}] = checkDirForInconsitency( ...
        fullfile(parentDirRunFolders,runFolderNames{f}), NInfoStrings, endString, 'std', stdVal_cutoff, 'NoDisp');
    
    disp(['-------------------- Folder: ' runFolderNames{f} ' --------------------'])
    
    % Display the files/responses that are consistent and should be copied
    % to the OK folder within current folder
    disp(['Copy the following ' num2str(length(consistentFiles{f})) ' files to OK-folder:'])
    disp(consistentFiles{f})
    disp(' ')
    
    % Display the files/responses that are INconsistent and should be copied
    % to the REMOVED folder within current folder
    disp(['Copy the following ' num2str(length(inconsistentFiles{f})) ' files to REMOVED-folder:'])
    disp(inconsistentFiles{f})
    
    disp(['--------------------- END ' runFolderNames{f} ' ---------------------'])
    disp('Press any key to check next folder')
    pause
    
end

% Clear clutter
clear NFolders f

disp('FINISHED!')