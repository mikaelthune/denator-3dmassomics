function printNewHTTPList( mode, parentDir, ID_partial, NTotalSurveys, URLStart, URLEnd, std_thresh, varargin)
%	Prints out a list of URLs to the surveys that still need new responses.
%
%   NOTE these limitations:
%   -   Assumes that parentDir has a defined folder structure with folders
%       named 'Run', 'OK', 'REMOVED'.
%   -   Assumes that csv-files already have been sorted into correct
%       folders
%   -   Only works when the survey ID is in the range 10001 - 10099
%
%   Uses:
%       printNewHTTPList( mode, parentDir, ID_partial, NTotalSurveys, URLStart, URLEnd, std_thresh)
%       printNewHTTPList( mode, parentDir, ID_partial, NTotalSurveys, URLStart, URLEnd, std_thresh, NInfoString, endString)
%
%   Input:
%   -   mode is a string that defines if the printed list should include
%       completed inconsistent responses or not.
%       *   mode = 'sortByConsistency' will re-add all the completed
%           inconsistent responses in order determined by their level of
%           consistency. This is the recommended mode.
%       *   mode = 'useInconsistentResponses' will only re-add surveys that 
%           have no completed response at all.
%       *   mode = 'owerwriteInconsistentResponses' will re-add all the 
%           completed inconsistent responses to the new HTTP-list.
%   -   parentDir is the directory of the Run folders.
%   -   ID_partial is vector with full survey ID:s (e.g. 10037) of eventual 
%       partial responses that haven't been exported yet and are awaitng 
%       completion.
%           NOTE: Can be entered as empty vector to represent that there 
%           are no partial responses that should be taken into account.
%   -   NTotalSurveys is the total number of individual surveys included 
%       in the full survey.
%   -   URLStart is the first part of the URLs to the individual surveys.
%       This should be all of the URL up to the Survey ID
%   -   URLEnd is the last part of the URLs to the individual surveys.
%       This should be everything after the survey ID.
%   -   std_thresh is the standard deviation threshold used to determine
%       which survey responses in parentDir are consistent. If a response
%       has a standard deviation <= std_thresh then it is consistent.
%
%   -   NInfoString is a survey parameter that says how many initial 
%       fields the csv-files have before the custom fields start appearing.
%       DEFAULT value is set to 6 (3DMassomics survey)
%   -   endString is a survey specific paramter that says what the field
%       name is in the csv-files.
%       DEFAULT value is set to 'QEval_CommentTime' (3DMassomics survey)

%% Input check
if nargin == 7
    NInfoStrings = 6;
    endString = 'QEval_CommentTime';
elseif nargin == 8
    error('Must be either 7 or 9 input paramters')
else
    if isnumeric(varargin{1}) && ischar(varargin{2})
        NInfoStrings = varargin{1};
        endString = varargin{2};
    else
        error('Input ''NInfoStrings'' must be numeric and ''endString'' must be a string')
    end
end

if std_thresh < 0
    error('Input STD threshold must be >= 0')
end

if ~strcmp(URLStart(end), '/')
    error('Input ''URLStart'' must end with the character ''/''')
end
if ~strcmp(URLEnd(1), '/')
    error('Input ''URLEnd'' must start with the character ''/''')
end
if ~strcmpi(URLStart(1:4), 'http')
    warning('Input ''URLStart'' does not start with the characters ''http'' ')
end

%% Calculate
subDirCell = findFoldersWithName( parentDir, 'Run', 'sortByEndNumber');

NDir = length(subDirCell);
IDsToExclude = ID_partial;
allIDs = (10001:(10000+NTotalSurveys))';

if strcmpi(mode, 'sortByConsistency')
    inconsistentIDsorted = getSortedInconsistentInParentDir( parentDir, std_thresh, NInfoStrings, endString);
    consistentIDs = [];
    for d = 1:NDir
        FileNames = findAllCsvFiles( fullfile(parentDir, subDirCell{d}, 'OK') );
        consistentIDs = [consistentIDs; extractSurveyIDFromFilenames( FileNames )];
    end
    
    nonExistentID = setdiff(allIDs, [consistentIDs; inconsistentIDsorted; IDsToExclude]);
    IDsInNewList = [nonExistentID; inconsistentIDsorted];
    
else
    % Check that the folder structure is ok
    verifySurveyExportFolderStructure( parentDir );
    
    for d = 1:NDir
        if strcmpi(mode, 'owerwriteInconsistentResponses')
            FileNames = findAllCsvFiles( fullfile(parentDir, subDirCell{d}, 'OK') );
        elseif strcmpi(mode, 'useInconsistentResponses')
            FileNames = findAllCsvFiles( fullfile(parentDir, subDirCell{d}) );
        else
            error('Undefined value for input ''mode''')
        end
        
        SurveyIDs = extractSurveyIDFromFilenames( FileNames );
        IDsToExclude = [IDsToExclude; SurveyIDs];
        
    end
    
    IDsInNewList = setdiff(allIDs, IDsToExclude);
end

%% Display
disp(['Surveys In List = ' num2str(length(IDsInNewList))])
disp('---------------------------------------------------------------------')
disp(' ')
for k = 1:length(IDsInNewList)
    disp([URLStart num2str(IDsInNewList(k)) URLEnd])
end
disp(' ')
disp('---------------------------------------------------------------------')

end

