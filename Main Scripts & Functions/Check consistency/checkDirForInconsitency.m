function varargout = checkDirForInconsitency( directory, NInfoStrings, endString, checkMethod, varargin )
%
%   Each csv-file in directory must contain only 1 response
%
%   checkDirForInconsitency( directory, NInfoStrings, endString, checkMethod )
%   FilenamesOfInconsistent = checkDirForInconsitency( directory, NInfoStrings, endString, checkMethod )
%   [FilenamesOfInconsistent, FilenamesOfConsistent] = checkDirForInconsitency(...)
%
%   checkDirForInconsitency( directory, NInfoStrings, endString, 'std', stdValueForCheck )
%
%   Adding 'NoDisp' as input paramter will diable printouts in command
%   window.
%
%   checkMethod: 'equalSign', 'std'
%
%   Input:
%   -   NInfoString is a survey parameter that says how many initial 
%       fields the csv-files have before the custom fields start appearing.
%       DEFAULT value is set to 6 (3DMassomics survey)
%   -   endString is a survey specific paramter that says what the field
%       name is in the csv-files.
%       DEFAULT value is set to 'QEval_CommentTime' (3DMassomics survey)

[SurveyStructCell, FileNames] = importCsvFiles( directory, NInfoStrings, endString );
NSurveys = length(SurveyStructCell);

NInconsistent = 0;
inconsistentFiles = {};

NConsistent = 0;
consistentFiles = {};

if sum(strcmpi(varargin,'NoDisp')) < 1
    disp(' ')
end
for k = 1:NSurveys
    if SurveyStructCell{k}.numberOfResponses ~= 1
        error('SurveyStructs must only contain 1 response each!')
    end
    
    indNumeric = find(cellfun(@isnumeric, varargin));
    if strcmpi(checkMethod, 'std') && nargin > 4 && length(indNumeric) >= 1
        isConsistent = userIsConsistent(SurveyStructCell{k}.User, checkMethod, 'NoDisp', varargin{indNumeric});
    else
        isConsistent = userIsConsistent(SurveyStructCell{k}.User, checkMethod, 'NoDisp');
    end
    
    if isConsistent
        NConsistent = NConsistent + 1;
        consistentFiles{NConsistent,1} = FileNames{k};
        
        if sum(strcmpi(varargin,'NoDisp')) < 1
            disp([FileNames{k} '  [OK]'])
        end
        
    else
        NInconsistent = NInconsistent + 1;
        inconsistentFiles{NInconsistent,1} = FileNames{k};
        
        if sum(strcmpi(varargin,'NoDisp')) < 1
            disp([FileNames{k} '  [INCONSISTENT] <------'])
        end
        
    end
end


if nargout == 1
    varargout{1} = inconsistentFiles;
    
elseif nargout == 2
    varargout{1} = inconsistentFiles;
    varargout{2} = consistentFiles;
end

end

