% Script for converting all MALDI Imaging data in a directory from SCiLS
% format to MATLAB image cubes.
% 
% NOTE: Script works only for the reproducibillity data in the 3DMASSOMICS
% projekt.

warning('Script works only for the reproducibillity data in the 3DMASSOMICS projekt.')

%% Change if required
dataDir = 'E:\Dokument\Dropbox\Denator\Data\Reproducibility datasets\SCiLS data\';
saveDir = 'E:\Dokument\Dropbox\Denator\Data\Reproducibility datasets\';

%%

blockDir = {'Block1','Block2','Block3'};
for b = 1:3
    name = cell(12,1);
    ImCubeCell = cell(12,1);
    mzCell = cell(12,1);
    for s = 1:12
        if s < 10
            name{s} = ['block' num2str(b) '_section0' num2str(s)];
        else
            name{s} = ['block' num2str(b) '_section' num2str(s)];
        end
        sectionDir = fullfile(dataDir, blockDir{b}, name{s}, [name{s} '.h5']);
        
        headerData = TDM_scilsHdf5ParseHeaderData(sectionDir);
        [ImCubeCell{s}, mzCell{s}] = TDM_generateDatacube(headerData);
        ImCubeCell{s} = ImCubeCell{s} + ~isnan(ImCubeCell{s}) .* (randn(size(ImCubeCell{s}))*0.0001 + 0.000001);
        
        disp([name{s} ' DONE!'])
        
    end
    
    for k = 1:12
        eval( ['ImCube_Noise_' name{k} '= ImCubeCell{k};'] );
        eval( ['mz_' name{k} '= mzCell{k};'] );
        save(fullfile(saveDir, [name{k} '_Noise.mat']), ['ImCube_Noise_' name{k}], ['mz_' name{k}]);
        eval( [' clear ImCube_Noise_' name{k} ' mz_' name{k}] );
    end
    clear ImCubeCell mzCell
    disp(['--------------Block ' num2str(b) ' Done!--------------'])
end
end