function [mzs, scorespec] = scoreDataset(x_coeffs,ResultFromImCube)
% assumes that order in ResultFromImCube is
% [STDmean,STDmedian,SNRmean,SNRmedian,MSE,SpatialChaos]
if length(x_coeffs) ~= length(ResultFromImCube.method)+1
    error('coeffs should be number of algorithms plus a constant')
end
mzs = ResultFromImCube.method(1).mz; 
scorespec = x_coeffs(end) + zeros(size(mzs)); % add constant term
for n=1:length(ResultFromImCube)
    scorespec = scorespec + ResultFromImCube.method(n).values*x_coeffs(n);
    
end
% scorespec = scorespec+x_coeffs(end);