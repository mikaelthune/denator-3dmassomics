%% Load
load('C:\Users\mikael.thune\Dropbox\Denator\Data\Reproducibility datasets\Dataset Descriptors\DatasetDescriptorCells.mat');

factorLevels = extractFactorLevelsFromDatasetDescriptors( DatasetDescriptorCell_5x5_default );
factorNames = extractFactorNamesFromDatasetDescriptors( DatasetDescriptorCell_5x5_default );
NFactors = size(factorLevels,2);

NImMethods = 6;
NDescriptors = 11;

%% calculate
p = nan(NFactors, NImMethods*NDescriptors);
table = cell(1,NImMethods*NDescriptors);
stats = cell(1,NImMethods*NDescriptors);
combString = cell(NImMethods*NDescriptors,1);

count = 0;
for im = 1:NImMethods
    for d = 1:NDescriptors
        count = count + 1;
        response = extractResponseFromDatasetDescriptors(DatasetDescriptorCell_5x5_default, im, d);
        [p(:,count), table{count}, stats{count}] = anovan(response, factorLevels, 'varnames', factorNames, 'display', 'off');
        combString{count} = ['im = ' num2str(im) ', desc = ' num2str(d)];
    end
end
clear im d count response

%% plot
figure
plot(p, '*')
axis([0 5 -0.1 1.1])
set(gca, 'XTick', [1 2 3 4], 'XTickLabel', factorNames)
ylabel('p-value')

figure
boxplot(p', 'labels', factorNames)
ylabel('p-value')



